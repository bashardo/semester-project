function [ shoulder ] = shoulderFromExcel(patient, rawExcel)
%SHOULDERFROMEXCEL build the shoulder structure array from Excel and from
%the patient structure
%   The output is a shoulder strucure array that has the same form as the
%   shoulder table of the mySQL database

fprintf('\nGet shoulder from Excel... ');

% define houlder structure array
shoulder = struct(...
    'shoulder_id', [], ...
    'patient_id',  [], ...
    'side',        [], ...
    'comment',        [] ...
    );

% get column of variables
header = rawExcel(1,:);
sCase_id_col   = find(strcmp([header],'sCase.id'));
IPP_col        = find(strcmp([header],'anonymity.IPP'));
side_col       = find(strcmp([header],'shoulder.side'));
comment_col        = find(strcmp([header],'shoulder.comment'));

% loop over rows of Excel table
[rowN, ~] = size(rawExcel);
shoulder_id = 0;
for row_idx = 2:rowN
    row = rawExcel(row_idx, :); % get the entire row
    sCaseE = row{sCase_id_col}; % sCase id from Excel
    side  = row{side_col};
    % get shoulder comment
    comment = row{comment_col};
    if isnan(comment)
        comment = '';
    end
    if ~isnan(sCaseE) & ~isnan(side) % check that sCase & side are defined
        IPP = row{IPP_col};
        patient_idx = find([patient.IPP] == IPP);
        % get patient index of this sCase
        if ~isempty(patient_idx)
            % there is a patient with same IPP
            % check that this shoulder patient_id & side is already in the shoulder array
            patient_id = patient(patient_idx).patient_id; % patient_id of existing patient
            % find this patient_id in shoulder array
            shoulder_idx = find([shoulder.patient_id] == patient_id);
            % might be 0, 1, or 2 (R/L)
            shoulder_idxN = length(shoulder_idx) == 1; % number of matching patient_id in shoulder
            addShoulder = 0; % 1 to add the shoulder in the shoulder array
            switch shoulder_idxN
                case 0
                    % this sCase patient is not in the shoulder array
                    % so the shoulder of this sCase is also not, so add it
                    addShoulder = 1;
                case 1
                    % this sCase patient is in the shoulder array
                    % so we have to check the shoulder side
                    if side ~= shoulder(shoulder_idx).side
                        % this shoulder side in not yet in the shoulder array
                        % so add it
                        addShoulder = 1;
                    end    
                    % fprintf('\nSecond shoulder in %s', sCase);
                case 2
                    % both shoulder sides of this patient are in the
                    % shoulder array, meaning that this sCase is either
                    % a duplicate, or a multiple case of the same shoulder
                    % Nothing to add, but report for control
                    fprintf('\nDuplicate or multiple case in %s', sCaseE);                    
                otherwise
                    % should not happend, so report in this case
                    fprintf('\nMore than 2 shoulders in %s', sCaseE);
            end
            if addShoulder
                shoulder_id = shoulder_id + 1;
                shoulder_idx = shoulder_id;
                shoulder(shoulder_idx).shoulder_id = shoulder_id;
                shoulder(shoulder_idx).patient_id  = patient_id;
                shoulder(shoulder_idx).side        = side;
                shoulder(shoulder_idx).comment        = comment;
            end                    
        end
    end
end

fprintf('Done\n');

end

