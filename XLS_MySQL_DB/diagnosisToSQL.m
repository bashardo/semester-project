function diagnosisToSQL(diagnosis, conn)
%diagnosisTOSQL Summary of this function goes here
%   Detailed explanation goes here

fprintf('\ndiagnosis to mySQL...');

% Empty diagnosis table and reset AUTO_INCREMENT
sqlquery = 'TRUNCATE diagnosis';
curs = exec(conn,sqlquery);
if ~isempty(curs.Message)
    fprintf('\nMessage: %s\n', curs.Message);
end

colnames = {'diagnosis_id','sCase_id','diagnosisList_id','date','comment'};

% Loop on pCase structure array
for i=1:numel(diagnosis)
    diagnosis_id      = diagnosis(i).diagnosis_id;
    sCase_id          = diagnosis(i).sCase_id;
    diagnosisList_id  = diagnosis(i).diagnosisList_id;
    date              = diagnosis(i).date;
    comment           = diagnosis(i).comment;
    
    exdata   = {diagnosis_id,sCase_id,diagnosisList_id,date,comment};
    % insert data in SQL
    datainsert(conn,'diagnosis',colnames,exdata);
end

fprintf(' Done\n');

end

