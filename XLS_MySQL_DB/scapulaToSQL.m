function scapulaToSQL(scapula, conn)
% fill scapula structure array in mySQL
%   Detailed explanation goes here

fprintf('\nscapula to mySQL... ');

% Empty scapula table and reset AUTO_INCREMENT
sqlquery = 'TRUNCATE scapula';
curs = exec(conn,sqlquery);
if ~isempty(curs.Message)
    fprintf('\nMessage: %s\n', curs.Message);
end

colnames = {'CT_id','CT_angle','AI','comment'};

% Loop on glenoid structure array
for i=1:numel(scapula)
    
    CT_id          = scapula(i).CT_id;
    CT_angle       = scapula(i).CTangle;
    AI             = scapula(i).AI;
    comment        = scapula(i).comment;
    
    exdata   = {CT_id,CT_angle,AI,comment};
    % insert data in SQL
    datainsert(conn,'scapula',colnames,exdata);
end

fprintf('Done\n');

end