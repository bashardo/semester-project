function glenoidToSQL(glenoid, conn)
%GLENOIDTOSQL fill glenoid structure array in mySQL
%   Detailed explanation goes here

fprintf('\nglenoid to mySQL... ');

% Empty glenoid table and reset AUTO_INCREMENT
sqlquery = 'TRUNCATE glenoid';
curs = exec(conn,sqlquery);
if ~isempty(curs.Message)
    fprintf('\nMessage: %s\n', curs.Message);
end

colnames = {'CT_id','radius','sphericity','biconcave','depth',...
        'width','height','version_ampl','version_orient',...
        'center_PA','center_IS','center_ML','version','inclination',...
        'version_2D','walch_class','comment'};


% Loop on glenoid structure array
for i=1:numel(glenoid)

    CT_id          = glenoid(i).CT_id;
    radius         = glenoid(i).radius;
    biconcave = '';        
    sphericity = 0;
%    sphericity     = glenoid(i).sphericity;
%    biconcave      = glenoid(i).biconcave;
    depth          = glenoid(i).depth;
    width          = glenoid(i).width;
    height         = glenoid(i).height;
    version_ampl    = glenoid(i).version_ampl;
    version_orient = glenoid(i).version_orient;
    center_PA      = glenoid(i).center_PA;
    center_IS      = glenoid(i).center_IS;
    center_ML      = glenoid(i).center_ML;
    version        = glenoid(i).version;
    inclination    = glenoid(i).inclination;
    version_2D     = glenoid(i).version_2D;
    walch_class    = glenoid(i).walch_class;
    comment    = glenoid(i).comment;
    
    if isnan(version_2D)
        version_2D = '';
    end
    if isnan(walch_class)
        walch_class = '';
    end

%     if isnan(biconcave)
%         biconcave = '';
%     end
    if isnan(width)
        width = '';
        height = '';
    end
        
    exdata   = {CT_id,radius,sphericity,biconcave,depth,...
        width,height,version_ampl,version_orient,...
        center_PA,center_IS,center_ML,version,inclination,...
        version_2D,walch_class,comment};

    % insert data in SQL

    datainsert(conn,'glenoid',colnames,exdata);
end

fprintf('Done\n');

end