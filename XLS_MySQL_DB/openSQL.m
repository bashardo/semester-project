function [ conn ] = openSQL( ~ )
% OPENSQL open the mySQL database
%   The JDBC driver is required to connect MATLAB to the MySQL database.
%   Refer to documentation at: 
%   https://ch.mathworks.com/help/database/ug/mysql-jdbc-windows.html

fprintf('\nOpen mySQL connection... ');

instance = 'shoulder_test';
username = 'matlabA';
password = 'M@tL@b1,';
url      = 'lbovenus.epfl.ch';

% Open SQL databse
conn = database(instance,username,password,'Vendor','MySQL',...
    'Server',url);

if ~isempty(conn.Message)
    fprintf('\nMessage: %s\n', conn.Message);
end
   
fprintf('Done\n');

end

