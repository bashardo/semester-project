function CTtoSQL(CT, conn)
%CTTOSQL fill ct structure array in mySQL
%   Detailed explanation goes here

fprintf('\nCT to mySQL... ');

testquery= 'SET FOREIGN_KEY_CHECKS = 0';
exec(conn,testquery);

% Empty CT table and reset AUTO_INCREMENT
sqlquery = 'TRUNCATE CT';
curs = exec(conn,sqlquery);
if ~isempty(curs.Message)
    fprintf('\nMessage: %s\n', curs.Message);
end

testquery= 'SET FOREIGN_KEY_CHECKS = 1';
exec(conn,testquery);

colnames = {'CT_id','shoulder_id','date','kernel','pixel_size',...
        'slice_spacing','slice_thickness','tension','current',...
        'manufacturer','model','institution','folder_name','comment'};

% Loop on ct structure array
for i=1:numel(CT)
    CT_id           = CT(i).CT_id;
    shoulder_id     = CT(i).shoulder_id;
    date            = datestr(CT(i).date,'yyyy-mm-dd');
    kernel          = CT(i).kernel;
    pixel_size      = CT(i).pixel_size;
    slice_spacing   = CT(i).slice_spacing;
    slice_thickness = CT(i).slice_thickness;
    tension         = CT(i).tension;
    current         = 0;
    manufacturer    = CT(i).manufacturer;
    model           = CT(i).model;
    institution     = CT(i).institution;
    if isempty (institution)
        institution = ' ';
    end
    folder_name     = CT(i).folder_name;
    comment         = CT(i).comment;  
    exdata   = {CT_id,shoulder_id,date,kernel,pixel_size,...
        slice_spacing,slice_thickness,tension,current,...
        manufacturer,model,institution,folder_name,comment};
    % insert data in SQL
    datainsert(conn,'CT',colnames,exdata);
end

fprintf('Done\n');

end

