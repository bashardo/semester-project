function sCaseToSQL(sCase, conn)
%PCASETOSQL fill sCase structure array in mySQL
%   Detailed explanation goes here

fprintf('\nsCase to mySQL... ');

testquery= 'SET FOREIGN_KEY_CHECKS = 0';
exec(conn,testquery);

% Empty sCase table and reset AUTO_INCREMENT
sqlquery = 'TRUNCATE sCase';
curs = exec(conn,sqlquery);
if ~isempty(curs.Message)
    fprintf('\nMessage: %s\n', curs.Message);
end

testquery= 'SET FOREIGN_KEY_CHECKS = 1';
exec(conn,testquery);

colnames = {'sCase_id','shoulder_id','folder_name','comment'};

% Loop on pCase structure array
for i=1:numel(sCase)
    sCase_id     = sCase(i).sCase_id;
    shoulder_id  = sCase(i).shoulder_id;
    folder_name  = sCase(i).folder_name;
    comment      = sCase(i).comment;
    
    exdata   = {sCase_id,shoulder_id,folder_name,comment};
    % insert data in SQL
    datainsert(conn,'sCase',colnames,exdata);
end

fprintf('Done\n');

end

