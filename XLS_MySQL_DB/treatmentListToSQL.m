function treatmentListToSQL(treatmentList, conn)
%TREATMENTTOSQL Summary of this function goes here
%   Detailed explanation goes here

fprintf('\ntreatmentList to mySQL...');

colnames = {'treatmentList_id','name','description'};

% Loop on pCase structure array
for i=1:numel(treatmentList)
    treatmentList_id = treatmentList(i).treatmentList_id;
    name         = treatmentList(i).name;
    description  = treatmentList(i).description;
    
    exdata   = {treatmentList_id,name,description};
    % insert data in SQL
    datainsert(conn,'treatmentList',colnames,exdata);
end

fprintf(' Done\n');

end

