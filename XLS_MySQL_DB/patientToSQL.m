function patientToSQL(patient, conn)
%PATIENDBUPDATE fill patient structure array in mySQL
%   Detailed explanation goes here

fprintf('\npatient to mySQL... ');

testquery= 'SET FOREIGN_KEY_CHECKS = 0';
exec(conn,testquery);

% Empty patient table and reset AUTO_INCREMENT
sqlquery = 'TRUNCATE patient';
curs = exec(conn,sqlquery);
if ~isempty(curs.Message)
    fprintf('\nMessage: %s\n', curs.Message);
end

testquery= 'SET FOREIGN_KEY_CHECKS = 1';
exec(conn,testquery);

colnames = {'gender','birth_date','height','weight','comment'};

% Loop on patient structure array
for i=1:numel(patient)
    gender     = patient(i).gender;
    birth_date = datestr(patient(i).birth_date,'yyyy-mm-dd');
    height     = patient(i).height;
    comment    = patient(i).comment;
    
    if isnan(height)
         height = '';
    end
    weight     = patient(i).weight;
    if isnan(weight)
         weight = '';
    end
    
    exdata   = {gender,birth_date,height,weight,comment};
    % insert data in SQL
    datainsert(conn,'patient',colnames,exdata);
end

fprintf('Done\n');

end

