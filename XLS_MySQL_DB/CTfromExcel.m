function [CT, scapula, glenoid, glenoid_density, humerus, muscle] = CTfromExcel(shoulder, patient, rawExcel)
% ctFromExcel build the ct structure array from the excelRaw, patient and
% shoulder structure array.
%   ct contains all data directly related to the imaging

fprintf('\nGet CT & glenoid from Excel...');

% define CT structure array
CT = struct(...
    'CT_id',           [], ...
    'shoulder_id',     [], ...
    'date',            [], ...
    'kernel',          [], ...
    'pixel_size',      [], ...
    'slice_spacing',   [], ...
    'slice_thickness', [], ...
    'tension',         [], ...
    'current',         [], ...
    'manufacturer',    [], ...
    'model',           [], ...
    'institution',     [], ...
    'folder_name',     [],  ...
    'comment',     []  ...
    );

% define scapula array
scapula = struct(...
    'CT_id',           [], ...
    'CTangle',         [], ...
    'AI',              [],  ...
    'comment',     []  ...
    );

% define glenoid structure array
glenoid = struct(...
    'CT_id',          [], ...
    'radius',         [], ...
    'sphericity',     [], ...
    'biconcave',      [], ...
    'depth',          [], ...
    'width',          [], ...
    'height',         [], ...
    'version_ampl',    [], ...
    'version_orient', [], ...
    'center_PA',      [], ...
    'center_IS',      [], ...
    'center_ML',      [], ...
    'version',        [], ...
    'inclination',    [], ...
    'version_2D',     [], ...
    'walch_class',    [],  ...
    'comment',     []  ...
    );

% define glenoid_density structure array
glenoid_density = struct(...
    'CT_id',[], ...
    'CO'   ,[], ...
    'SC'   ,[], ...
    'ST'   ,[], ...
    'T1'   ,[], ...
    'T2'   ,[], ...
    'T3'   ,[], ...
    'RE'   ,[],  ...
    'comment',     []  ...
    );

% define humerus structure array
humerus = struct(...
    'CT_id'          ,[], ...
    'joint_radius'   ,[], ...
    'head_radius'    ,[], ...
    'SHsublux_ampl'  ,[], ...
    'SHsublux_orient',[], ...
    'GHsublux_ampl'  ,[], ...
    'GHsublux_orient',[], ...
    'SHsublux_2D'    ,[], ...
    'GHsublux_2D'    ,[],  ...
    'comment',     []  ...
    );

%define muscle structure array
muscle = struct(...
    'CT_id',[], ...
    'SSA'  ,[],...
    'SSI'  ,[],...
    'SSO'  ,[],...
    'SSD'  ,[],...
    'ISA'  ,[],...
    'ISI'  ,[],...
    'ISO'  ,[],...
    'ISD'  ,[],...
    'SCA'  ,[],...
    'SCI'  ,[],...
    'SCO'  ,[],...
    'SCD'  ,[],...
    'TMA'  ,[],...
    'TMI'  ,[],...
    'TMO'  ,[],...
    'TMD'  ,[],  ...
    'comment',     []  ...
    );

% get column of variables
header = rawExcel(1,:);
sCase_id_col           = find(strcmp([header],'sCase.id'));
IPP_col                = find(strcmp([header],'anonymity.IPP'));
side_col               = find(strcmp([header],'shoulder.side'));

CT_date_col            = find(strcmp([header],'CT.date'));
CT_kernel_col          = find(strcmp([header],'CT.kernel'));
CT_pixel_size_col      = find(strcmp([header],'CT.pixel_size'));
CT_slice_spacing_col   = find(strcmp([header],'CT.slice_spacing'));
CT_slice_thickness_col = find(strcmp([header],'CT.slice_thickness'));
CT_tension_col         = find(strcmp([header],'CT.tension'));
CT_current_col         = find(strcmp([header],'CT.current'));
CT_manufacturer_col    = find(strcmp([header],'CT.manufacturer'));
CT_model_col           = find(strcmp([header],'CT.model'));
CT_institution_col     = find(strcmp([header],'CT.institution'));
CTcom_col              = find(strcmp([header],'CT.comment'));

scapula_CTangle_col = find(strcmp([header],'scapula.CTangle'));
scapula_AI_col = find(strcmp([header],'scapula.AI'));
scapulacom_col        = find(strcmp([header],'scapula.comment'));

glenoid_radius_col         = find(strcmp([header],'glenoid.radius'));
% glenoid_sphericity_col     = find(strcmp([header],'glenoid.sphericity'));
% glenoid_biconcave_col      = find(strcmp([header],'glenoid.biconcave'));
glenoid_depth_col          = find(strcmp([header],'glenoid.depth'));
glenoid_width_col          = find(strcmp([header],'glenoid.width'));
glenoid_height_col         = find(strcmp([header],'glenoid.height'));
glenoid_version_ampl_col    = find(strcmp([header],'glenoid.version_ampl'));
glenoid_version_orient_col = find(strcmp([header],'glenoid.version_orient'));
glenoid_center_PA_col      = find(strcmp([header],'glenoid.center_PA'));
glenoid_center_IS_col      = find(strcmp([header],'glenoid.center_IS'));
glenoid_center_ML_col      = find(strcmp([header],'glenoid.center_ML'));
glenoid_version_col        = find(strcmp([header],'glenoid.version'));
glenoid_inclination_col    = find(strcmp([header],'glenoid.inclination'));
glenoid_version_2D_col     = find(strcmp([header],'glenoid.version_2D'));
glenoid_walch_class_col    = find(strcmp([header],'glenoid.walch_class'));
glenoidcom_col        = find(strcmp([header],'glenoid.comment'));

glenoid_density_CO_col = find(strcmp([header],'glenoid_density.CO'));
glenoid_density_SC_col = find(strcmp([header],'glenoid_density.SC'));
glenoid_density_ST_col = find(strcmp([header],'glenoid_density.ST'));
glenoid_density_T1_col = find(strcmp([header],'glenoid_density.T1'));
glenoid_density_T2_col = find(strcmp([header],'glenoid_density.T2'));
glenoid_density_T3_col = find(strcmp([header],'glenoid_density.T3'));
glenoid_density_RE_col = find(strcmp([header],'glenoid_density.RE'));
glenoid_densitycom_col        = find(strcmp([header],'glenoid_density.comment'));

humerus_joint_radius_col    = find(strcmp([header],'humerus.joint_radius'));
humerus_head_radius_col     = find(strcmp([header],'humerus.head_radius'));
humerus_SHsublux_ampl_col   = find(strcmp([header],'humerus.SHsublux_ampl'));
humerus_SHsublux_orient_col = find(strcmp([header],'humerus.SHsublux_orient'));
humerus_GHsublux_ampl_col   = find(strcmp([header],'humerus.GHsublux_ampl'));
humerus_GHsublux_orient_col = find(strcmp([header],'humerus.GHsublux_orient'));
humerus_SHsublux_2D_col     = find(strcmp([header],'humerus.SHsublux_2D'));
humerus_GHsublux_2D_col     = find(strcmp([header],'humerus.GHsublux_2D'));
humeruscom_col        = find(strcmp([header],'humerus.comment'));

muscle_SSA_col = find(strcmp([header],'muscle.SSA'));
muscle_SSI_col = find(strcmp([header],'muscle.SSI'));
muscle_SSO_col = find(strcmp([header],'muscle.SSO'));
muscle_SSD_col = find(strcmp([header],'muscle.SSD'));
muscle_ISA_col = find(strcmp([header],'muscle.ISA'));
muscle_ISI_col = find(strcmp([header],'muscle.ISI'));
muscle_ISO_col = find(strcmp([header],'muscle.ISO'));
muscle_ISD_col = find(strcmp([header],'muscle.ISD'));
muscle_SCA_col = find(strcmp([header],'muscle.SCA'));
muscle_SCI_col = find(strcmp([header],'muscle.SCI'));
muscle_SCO_col = find(strcmp([header],'muscle.SCO'));
muscle_SCD_col = find(strcmp([header],'muscle.SCD'));
muscle_TMA_col = find(strcmp([header],'muscle.TMA'));
muscle_TMI_col = find(strcmp([header],'muscle.TMI'));
muscle_TMO_col = find(strcmp([header],'muscle.TMO'));
muscle_TMD_col = find(strcmp([header],'muscle.TMD'));
musclecom_col        = find(strcmp([header],'muscle.comment'));

% initialize counters
[rowN, ~] = size(rawExcel);
CT_id               = 0;
CT_idx              = 0;
scapula_idx         = 0;
glenoid_idx         = 0;
glenoid_density_idx = 0;
humerus_idx         = 0;
muscle_idx          = 0;

% loop over rows of Excel table
for row_idx = 2:rowN
    row = rawExcel(row_idx, :); % get the entire row
    sCaseE  = row{sCase_id_col};
    if ~isnan(sCaseE) % check that this sCase is not empty
        IPP = row{IPP_col}; % IPP of the patient
        patient_idx = find([patient.IPP] == IPP);
        % get patient index of this sCase
        % this patient should already exist in patient structure array and
        % should be unique
        if ~isempty(patient_idx)
            patient_id = patient(patient_idx).patient_id; % get patient_id
            
            shoulder_idx = find([shoulder.patient_id] == patient_id); % get shoulder_id
            % should be 1 or 2 (R/L), but not 0 (test it however)
            shoulder_idxN = length(shoulder_idx);
            side = row{side_col};
            
            switch shoulder_idxN
                case 0
                    % this sCase patient is not in the shoulder array
                    % report an error (probably not side identified)
                    fprintf('\nsCase %s not in shoulder array', sCaseE);
                    addCT = 0;
                case 1
                    % this sCase patient has 1 match in the shoulder array
                    % so we have to check side
                    if side == shoulder(shoulder_idx).side
                        shoulder_id = shoulder(shoulder_idx).shoulder_id;
                        addCT = 1;
                    else
                        % not same side so report a problem, probably side not
                        % defined
                        fprintf('\nsCase %s in shoulder array but not same side', sCaseE);
                    end
                case 2
                    % both shoulder sides of this patient are in the
                    % shoulder array, so we have to check side
                    if side == shoulder(shoulder_idx(1)).side
                        shoulder_id = shoulder(shoulder_idx(1)).shoulder_id;
                        addCT = 1;
                    else
                        if side == shoulder(shoulder_idx(2)).side
                            shoulder_id = shoulder(shoulder_idx(2)).shoulder_id;
                            addCT = 1;
                        end
                    end
            end
            % check if CT kernel is defined
            kernel = row{CT_kernel_col};
            if isnan(kernel)
                addCT = 0;
            end
            
            if addCT % add CT to the array
                CT_id = CT_id + 1;
                CT_idx = CT_id;
                
                CT_date = row{CT_date_col};
                CT_date = datetime(CT_date,'ConvertFrom','excel','InputFormat','dd.MM.yyyy');
                CT_date.Format = 'yyyy-MM-dd';
                
                % get CT comment
                CT_comment = row{CTcom_col};
                if isnan(CT_comment)
                    CT_comment = '';
                end
                
                CT(CT_idx).CT_id           = CT_id;
                CT(CT_idx).shoulder_id     = shoulder_id;
                CT(CT_idx).date            = CT_date;
                CT(CT_idx).kernel          = row{CT_kernel_col};
                CT(CT_idx).pixel_size      = row{CT_pixel_size_col};
                CT(CT_idx).slice_spacing   = row{CT_slice_spacing_col};
                CT(CT_idx).slice_thickness = row{CT_slice_thickness_col};
                CT(CT_idx).tension         = row{CT_tension_col};
%                CT(CT_idx).current         = row{CT_current_col};
                CT(CT_idx).manufacturer    = row{CT_manufacturer_col};
                CT(CT_idx).model           = row{CT_model_col};
                CT(CT_idx).institution     = row{CT_institution_col};
                CT(CT_idx).folder_name     = sCaseE;
                CT(CT_idx).comment         = CT_comment;
                
                % check for adding in scapula structure array
                CTangle = row{scapula_CTangle_col};
                AI = row{scapula_AI_col};
                % get scapula comment
                scapula_comment = row{scapulacom_col};
                if isnan(scapula_comment)
                    scapula_comment = '';
                end
                
                if ~isnan(CTangle)
                    scapula_idx = scapula_idx + 1;
                    
                    scapula(scapula_idx).CT_id   = CT_id;
                    scapula(scapula_idx).CTangle = CTangle;
                    scapula(scapula_idx).AI = AI;
                    scapula(scapula_idx).comment = scapula_comment;
                end
                
                % check for adding in glenoid structure array
                radius = row{glenoid_radius_col};
                if ~isnan(radius)
                    % Assumes that all glenoid data are present & valid is
                    % radius is defined
                    glenoid_idx = glenoid_idx + 1;
                    
                    %                    sphericity     = row{glenoid_sphericity_col};
                    %                    biconcave      = row{glenoid_biconcave_col};
                    depth          = row{glenoid_depth_col};
                    width          = row{glenoid_width_col};
                    height         = row{glenoid_height_col};
                    version_ampl    = row{glenoid_version_ampl_col};
                    version_orient = row{glenoid_version_orient_col};
                    center_PA      = row{glenoid_center_PA_col};
                    center_IS      = row{glenoid_center_IS_col};
                    center_ML      = row{glenoid_center_ML_col};
                    version        = row{glenoid_version_col};
                    inclination    = row{glenoid_inclination_col};
                    version_2D     = row{glenoid_version_2D_col};
                    walch_class    = row{glenoid_walch_class_col};
                    glenoid_comment = row{glenoidcom_col};
                    if isnan(glenoid_comment)
                        glenoid_comment = '';
                    end
                
                    glenoid(glenoid_idx).CT_id          = CT_id;
                    glenoid(glenoid_idx).radius         = radius;
                    %                    glenoid(glenoid_idx).sphericity     = sphericity;
                    %                    glenoid(glenoid_idx).biconcave      = biconcave;
                    glenoid(glenoid_idx).depth          = depth;
                    glenoid(glenoid_idx).width          = width;
                    glenoid(glenoid_idx).height         = height;
                    glenoid(glenoid_idx).version_ampl    = version_ampl;
                    glenoid(glenoid_idx).version_orient = version_orient;
                    glenoid(glenoid_idx).center_PA      = center_PA;
                    glenoid(glenoid_idx).center_IS      = center_IS;
                    glenoid(glenoid_idx).center_ML      = center_ML;
                    glenoid(glenoid_idx).version        = version;
                    glenoid(glenoid_idx).inclination    = inclination;
                    glenoid(glenoid_idx).version_2D     = version_2D;
                    glenoid(glenoid_idx).walch_class    = walch_class;
                    glenoid(glenoid_idx).comment    = glenoid_comment;

                end
                
                % check for adding in glenoid_density structure array
                CO = row{glenoid_density_CO_col};
                if ~isnan(CO)
                    % Assumes that all glenoid_density data are present & valid is
                    % CO is defined. RE can be empty
                    glenoid_density_idx = glenoid_density_idx + 1;
                    
                    SC = row{glenoid_density_SC_col};
                    ST = row{glenoid_density_ST_col};
                    T1 = row{glenoid_density_T1_col};
                    T2 = row{glenoid_density_T2_col};
                    T3 = row{glenoid_density_T3_col};
                    RE = row{glenoid_density_RE_col};
                    % get glenoid_density comment
                    glenoid_density_comment = row{glenoid_densitycom_col};
                    if isnan(glenoid_density_comment)
                        glenoid_density_comment = '';
                    end
                    
                    glenoid_density(glenoid_density_idx).CT_id = CT_id;
                    glenoid_density(glenoid_density_idx).CO    = CO;
                    glenoid_density(glenoid_density_idx).SC    = SC;
                    glenoid_density(glenoid_density_idx).ST    = ST;
                    glenoid_density(glenoid_density_idx).T1    = T1;
                    glenoid_density(glenoid_density_idx).T2    = T2;
                    glenoid_density(glenoid_density_idx).T3    = T3;
                    glenoid_density(glenoid_density_idx).RE    = RE;
                    glenoid_density(glenoid_density_idx).comment    = glenoid_density_comment;

                end
                
                % check for adding in humerus structure array
                head_radius = row{humerus_head_radius_col};
                if ~isnan(head_radius)
                    % Assumes that all humerus data are present & valid is
                    % head_radius is defined.
                    humerus_idx = humerus_idx + 1;
                    
                    joint_radius    = row{humerus_joint_radius_col};
                    SHsublux_ampl   = row{humerus_SHsublux_ampl_col};
                    SHsublux_orient = row{humerus_SHsublux_orient_col};
                    GHsublux_ampl   = row{humerus_GHsublux_ampl_col};
                    GHsublux_orient = row{humerus_GHsublux_orient_col};
                    SHsublux_2D     = row{humerus_SHsublux_2D_col};
                    GHsublux_2D     = row{humerus_GHsublux_2D_col};
                    % get humerus comment
                    humerus_comment = row{humeruscom_col};
                    if isnan(humerus_comment)
                        humerus_comment = '';
                    end  
                    
                    humerus(humerus_idx).CT_id           = CT_id;
                    humerus(humerus_idx).head_radius     = head_radius;
                    humerus(humerus_idx).joint_radius    = joint_radius;
                    humerus(humerus_idx).SHsublux_ampl   = SHsublux_ampl;
                    humerus(humerus_idx).SHsublux_orient = SHsublux_orient;
                    humerus(humerus_idx).GHsublux_ampl   = GHsublux_ampl;
                    humerus(humerus_idx).GHsublux_orient = GHsublux_orient;
                    humerus(humerus_idx).SHsublux_2D     = SHsublux_2D;
                    humerus(humerus_idx).GHsublux_2D     = GHsublux_2D;
                    humerus(humerus_idx).comment     = humerus_comment;
                end
                
                % check for adding in muscle structure array
                SSA = row{muscle_SSA_col};
                if ~isnan(SSA)
                    % Assumes that all muscle data are present & valid is
                    % SSA is defined.
                    muscle_idx = muscle_idx + 1;
                    
                    SSA = row{muscle_SSA_col};
                    SSI = row{muscle_SSI_col};
                    SSO = row{muscle_SSO_col};
                    SSD = row{muscle_SSD_col};
                    ISA = row{muscle_ISA_col};
                    ISI = row{muscle_ISI_col};
                    ISO = row{muscle_ISO_col};
                    ISD = row{muscle_ISD_col};
                    SCA = row{muscle_SCA_col};
                    SCI = row{muscle_SCI_col};
                    SCO = row{muscle_SCO_col};
                    SCD = row{muscle_SCD_col};
                    TMA = row{muscle_ISA_col};
                    TMI = row{muscle_ISI_col};
                    TMO = row{muscle_ISO_col};
                    TMD = row{muscle_ISD_col};
                    % get muscle comment
                    muscle_comment = row{musclecom_col};
                    if isnan(muscle_comment)
                        muscle_comment = '';
                    end 
                    
                    muscle(muscle_idx).CT_id = CT_id;
                    muscle(muscle_idx).SSA   = SSA;
                    muscle(muscle_idx).SSI   = SSI;
                    muscle(muscle_idx).SSO   = SSO;
                    muscle(muscle_idx).SSD   = SSD;
                    muscle(muscle_idx).ISA   = ISA;
                    muscle(muscle_idx).ISI   = ISI;
                    muscle(muscle_idx).ISO   = ISO;
                    muscle(muscle_idx).ISD   = ISD;
                    muscle(muscle_idx).SCA   = SCA;
                    muscle(muscle_idx).SCI   = SCI;
                    muscle(muscle_idx).SCO   = SCO;
                    muscle(muscle_idx).SCD   = SCD;
                    muscle(muscle_idx).TMA   = TMA;
                    muscle(muscle_idx).TMI   = TMI;
                    muscle(muscle_idx).TMO   = TMO;
                    muscle(muscle_idx).TMD   = TMD;
                    muscle(muscle_idx).comment   = muscle_comment;

                end
            end
        end
    end
end

fprintf(' Done\n');

end

