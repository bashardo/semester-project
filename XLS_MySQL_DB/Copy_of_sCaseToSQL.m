function sCaseToSQL(sCase, conn)
%PCASETOSQL fill sCase structure array in mySQL
%   Detailed explanation goes here

fprintf('\nsCase to mySQL... ');

% Empty sCase table and reset AUTO_INCREMENT
sqlquery = 'TRUNCATE sCase';
curs = exec(conn,sqlquery);
if ~isempty(curs.Message)
    fprintf('\nMessage: %s\n', curs.Message);
end

colnames = {'sCase_id','shoulder_id','folder_name'};

% Loop on pCase structure array
for i=1:numel(sCase)
    sCase_id     = sCase(i).sCase_id;
    shoulder_id  = sCase(i).shoulder_id;
    pathology_id = sCase(i).pathology_id;
    treatment_id = pCase(i).treatment_id;
    operation_date = pCase(i).operation_date;
    if ~isnat(operation_date)
        operation_date = datestr(pCase(i).operation_date,'yyyy-mm-dd');
    else
        operation_date = '';
    end
    folder_name  = pCase(i).folder_name;
    
    exdata   = {pCase_id,shoulder_id,pathology_id,treatment_id,operation_date,folder_name};
    % insert data in SQL
    datainsert(conn,'pCase',colnames,exdata);
end

fprintf('Done\n');

end

