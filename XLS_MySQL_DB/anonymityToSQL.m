function anonymityToSQL(patient, conn)
%PATIENDBUPDATE fill patient structure array in mySQL
%   Detailed explanation goes here

fprintf('\nanonymity to mySQL... ');

% Empty anonymity table and reset AUTO_INCREMENT
sqlquery = 'TRUNCATE anonymity';
curs = exec(conn,sqlquery);
if ~isempty(curs.Message)
    fprintf('\nMessage: %s\n', curs.Message);
end

colnames = {'ipp','initials','birth_date'};

% Loop on patient structure array
for i=1:numel(patient)
    ipp        = patient(i).IPP;
    initials   = patient(i).initials;
    birth_date = datestr(patient(i).anonym_birth_date,'yyyy-mm-dd');
    
    exdata   = {ipp,initials,birth_date};
    % insert data in SQL
    datainsert(conn,'anonymity',colnames,exdata);
end

fprintf('Done\n');

end

