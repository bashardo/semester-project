function [status,message] = csv2xlsSCase(csvFilename)
%CSV2XLSSCASE Read csv file and write xls (only from windows)
%   Detailed explanation goes here


% Input: name of a csv file

% Output: status and message

% Author: Alexandre Terrier, EPFL-LBO
% Creation date: 2018-08-13
%

% TO DO:

%% Delete previous xls to avoid outdated xls (compared to current cvs)

xlsFilename = strrep(csvFilename, '.csv', '.xls'); % Replace .csv by .xls
% Delete file if exist to avoid potential issue
if exist(xlsFilename, 'file')
    delete xlsFilename;
end

%% Write xls file

if ispc % Check if run from windows!
    % Read csv file as table (only way to read non-numeric data) and keep
    % date as char.
    csvTable = readtable(csvFilename,'DatetimeType', 'text');
    % If 'DatetimeType' is specified as 'text', then the type for imported
    % date and time data depends on the value specified in the 'TextType'
    % parameter: If 'TextType' is 'char', then readtable returns dates as a
    % cell array of character vectors. If 'TextType' is 'string', then
    % readtable returns dates as an array of strings.
    csvCell = table2cell(csvTable); % Tranform table cell array (required by xlswrite)
    dataHeader = csvTable.Properties.VariableNames; % Get header
    csvCell = [dataHeader; csvCell]; % Add header

    % Sheet name with same name of the xls file
    sheet = split(xlsFilename,'/'); % Split subdir
    sheet = sheet(end); % Take only filename
    sheet = cell2mat(sheet); % Tranform cell to char
    sheet = strrep(sheet, '.xls', ''); % Remove xls suffix)
    
    [status,msg] = xlswrite(xlsFilename, csvCell, sheet); % Save xls
    if status 
        message = ['\nSaved xls file: ' xlsFilename];
    else
        message =  ['\nCould not save xls file: ' xlsFilename];
        message =  strcat(message, ['\n' msg.identifier]);
        message =  strcat(message, ['\n' msg.message]);
    end
else
    message = '\nCould not save xls file (Not Windows system). Open and save as xls from Excel';
    status = 0;
end

end

