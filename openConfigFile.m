function dataDir = openConfigFile(configFile, logFileID)
%OPENCONFIGFILE open file config.txt and read dataDir
%   Config file must follow specific/restrictive rules

% The file config.txt should be writen as below.
% One line should contain dataDir = <directory>

%{
% This configuration file contains the definition of variable dataDir for database access.
% It is used by functions: listSCase.m, measureSCase.m, plotScase.m
% /home/shoulder/data        <-- acces data    dir from lbovenus or lbomars (default)
% /home/shoulder/dataDev     <-- acces dataDev dir from lbovenus or lbomars
% /Volumes/shoulder/data     <-- acces data    dir from lbovenus/lbomars mounted on macos
% /Volumes/shoulder/dataDev  <-- acces dataDev dir from lbovenus mounted on macos
% Z:\data                    <-- acces data    dir from lbovenus/lbomars mounted on windows
% Z:\dataDev                 <-- acces dataDev dir from lbovenus mounted on windows
dataDir = /Volumes/shoulder/dataDev
%}

% Author: AT
% Date: 2019-01-15
% Modified by: JSM, 2019-02-13
% TODO: Less restrict rules for writting config.txt
dataDir = [];
if nargin==2
    if exist(configFile, 'file')
        fileID = fopen(configFile,'r');
        dataDir = fscanf(fileID, '%s'); % Read config file without spaces
        fclose(fileID);
        k = strfind(dataDir, 'dataDir='); % Find definition of dataDir
        k = k + 8; % Remove 'dataDir='
        % Check if dataDir exists
        dataDir = dataDir(k:end); % Assume that config file ends with dataDir content
        if ~exist(dataDir, 'dir')
            fprintf(logFileID, ['Data directory not found, check ' configFile]);
%             error(['Data directory not found, check ' configFile]);
        end
    end
end
if ~exist(dataDir, 'dir')
    errordlg(sprintf('%s%s%s%s',...
        'Please select manually the data directory',...
        'Error: ConfigFile not provided or not found',...
        'dataDir not found = ',dataDir))
    %dataDir = uigetdir(pwd,'Please select the directory ''data or dataDev''');
end
end
