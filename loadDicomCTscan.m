% This script tries to load any CT dicom scan

dicomDir = '/Volumes/shoulder/data/N/1/8/N180-753671/CT-N180-753671-1/dicom'; % Not working
%dicomDir = '/Volumes/shoulder/data/N/0/2/N29-40883/CT-N29-40883-1/dicom'; % Working

%%
dicomFiles = dir(dicomDir);
dicomFiles = dicomFiles(~ismember({dicomFiles.name},{'.','..'}));

filename = fullfile(dicomFiles(10).folder, dicomFiles(10).name);

try
    dicomInfoRes = dicominfo(filename, 'UseVRHeuristic', true);
    dicomInfoError = false;
    disp('dicominfo ok');
    % If dicominfo didn't worked, try with matlab function dicomdisp
catch ME
    dicomInfoError = true;
    disp('dicominfo error --> using dicomdisp');
    disp(ME.message);
    dicomdispStr = evalc('dicomdisp(filename)');
end
%%
a=1;
b=2;
X = dicomread(filename);
    
if ~dicomInfoError
    X = dicomread(filename);
    imshow(X);
    source = dicomDir;
    [V,spatial,dim] = dicomreadVolume(source); % Should be transformed to match Elham segmentation code
end


%%
tic;
source = dicomDir;
    [V,spatial,dim] = dicomreadVolume(source); % Should be transformed to match Elham segmentation code
toc

%%
Volume = squeeze(V); % Remove singleton dimension
% We shoudl set a grid before using isosurface
[fo,vo] = isosurface(Volume,150); % Could be used in measureSCase when no manual segmentation
figure
p1 = patch('Faces', fo, 'Vertices', vo); % draw the outside of the volume
p1.FaceColor = 'red';
p1.EdgeColor = 'none';


dicomBrowser(dicomDir);