function output = plotSCase(SCaseID)
    % Call a ShoulderCase.plot function
    %
    % Inputs: id char of shoulder case (e.g. 'P315')
    %
    % Output: Corresponding ShoulderCase object
    %
    % Example: SCase = plotSCase('P315');
    %
    % Author: Alexandre Terrier, EPFL-LBO
    %         Matthieu Boubat, EPFL-LBO
    % Creation date: 2018-07-01
    % Revision date: 2020-07-22

    addpath(genpath('ShoulderCase'));

    database = ShoulderCaseLoader();
    SCase = database.loadCase(SCaseID);
    plot(SCase);

    output = SCase;
end
