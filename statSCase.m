% This script load the entire SCaseDB and performs statisctical analyses


%% Initialisation of paths
% Should be replaced by a function
dataDir = '../../../data'; % location of data
logDir = 'log'; % log file in xls folder
xlsDir = [dataDir '/Excel/xlsFromMatlab'];
matlabDir = [dataDir '/matlab'];

%% Add path to ShoulderCase class
addpath('ShoulderCase');

%% Load ScaseDB
matlabFile = 'SCaseDB.mat';
matlabFile = [matlabDir '/' matlabFile];
if exist(matlabFile, 'file')
    load(matlabFile); % Load SCase dat file
else
    error('Not valid matlab file');
end

%% Select SCase with valid anatomical measurements
SCase = ShoulderCase; % Create new object ShoulderCase
iSCase = 0; % Index for SCase
for iSCaseDB = 1:length(SCaseDB)  % Loop on all SCase
    if ~isempty(SCaseDB(iSCaseDB).shoulder.side)
        iSCase = iSCase + 1;
        SCase(iSCase) = SCaseDB(iSCaseDB);
    end
end

%% Select normal and pathological SCase
SCaseN = ShoulderCase; % Create new object ShoulderCase for normal
SCaseP = ShoulderCase; % Create new object ShoulderCase for pahtological
iSCaseN = 0; % Index for SCaseN
iSCaseP = 0; % Index for SCaseP
for iSCase = 1:length(SCase) % Loop on all valide SCase
    if strcmp(SCase(iSCase).id(1), 'N') % SCase is normal
        iSCaseN = iSCaseN + 1;
        SCaseN(iSCaseN) = SCase(iSCase);
    else
        iSCaseP = iSCaseP + 1;
        SCaseP(iSCaseP) = SCase(iSCase);
    end
end

%% Obj to struct

% for iSCaseN = 1:length(SCaseN)
%     ParamN(iSCaseN).acromionIE = SCaseN(iSCaseN).shoulder.scapula.acromion.IE;
% end

for iSCaseP = 1:length(SCaseP)
    glenoidAngle(iSCaseP) = SCaseP(iSCaseP).shoulder.scapula.glenoid.versionAmpl;
    glenoidOrient(iSCaseP) = SCaseP(iSCaseP).shoulder.scapula.glenoid.versionOrient;
    version(iSCaseP) = SCaseP(iSCaseP).shoulder.scapula.glenoid.version;
    inclination(iSCaseP) = SCaseP(iSCaseP).shoulder.scapula.glenoid.inclination;
    SHSAmpl(iSCaseP) = SCaseP(iSCaseP).shoulder.humerus.SHSAmpl;
    SHSorient(iSCaseP) = SCaseP(iSCaseP).shoulder.humerus.SHSOrient;
    SHSAngle(iSCaseP) = SCaseP(iSCaseP).shoulder.humerus.SHSAngle;
    SHSPA(iSCaseP) = SCaseP(iSCaseP).shoulder.humerus.SHSPA;
    SHSIS(iSCaseP) = SCaseP(iSCaseP).shoulder.humerus.SHSIS;
    acromionAI(iSCaseP) = SCaseP(iSCaseP).shoulder.scapula.acromion.AI;
    acromionCSA(iSCaseP) = SCaseP(iSCaseP).shoulder.scapula.acromion.CSA;
    acromionPS(iSCaseP) = SCaseP(iSCaseP).shoulder.scapula.acromion.PS;
    acromionIE(iSCaseP) = SCaseP(iSCaseP).shoulder.scapula.acromion.IE;
end

%% Linear regression
data=[acromionPS; acromionIE; acromionCSA; acromionAI; SHSPA];
dataTable = array2table(data');
lm = fitlm(dataTable)




%%
