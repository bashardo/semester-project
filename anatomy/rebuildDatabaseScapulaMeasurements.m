% This script builds a cell array listing all caseIDs in the CT database 
% for which an Amira folder exists and scapula measurements can be 
% computed. Then computes scapula anatomical data for these cases.

CTDatabaseLocation = 'Z://data'; % Location of the CT database
CaseType = ['N';'P']; % Folders to be reconstructed

CaseIDsList = cell(0,0);

for i=1:length(CaseType) %Loop
    for j=0:9
        for k=0:9
            listDirInCurrDir = dir([CTDatabaseLocation '/' CaseType(i) '/' int2str(j) '/' int2str(k) '/' CaseType(i) '*']);
            if (~isempty(listDirInCurrDir))
                for m=1:length(listDirInCurrDir)
                    if exist([CTDatabaseLocation '/' CaseType(i) '/' int2str(j) '/' int2str(k) '/' listDirInCurrDir(m).name '/CT-' listDirInCurrDir(m).name  '-1/amira'],'dir') == 7 %if a folder "amira" exist, the case ID is added to the list
                        CaseIDsList{end+1,1} = strtok(listDirInCurrDir(m).name,'-');
                    end
                end
            end
        end
    end
end

scapula_measure(CaseIDsList);