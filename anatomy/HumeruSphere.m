%% Humerusphere
%
% This script creates a tcl text file named "SphereScript.tcl" to display the humerus sphere in
% Amira.
% This script is used in the "Anatomical measurements with Amira".

% Subject is the patient number
% Type is 'normal' or 'pathologic'

% Updated 2019-07-05 by AT
% The script select humerus landmarks in amira folder
% It should be re-written to be more consistent with measureSCase or plotSCase
% and moved to database

function HumeruSphere(subject,type)

directory = '../../../../data'; % Define directory
%directory = '../../../../dataDev'; % Define directory

switch type


    case 'pathologic'
        type = 'P';
        folder = sprintf('P%03d',subject) % format the patient name to P###
        subjectString = num2str(subject);
        levelDir1 = str2num(subjectString(1));
        levelDir2 = str2num(subjectString(2));

        % Loop over the list of patients and extract the complete name of the specific patient
        listing = dir(sprintf('%s/%s/%d/%d', directory, type, levelDir1, levelDir2))%;
        for i = 1:1:numel(listing)
            name = listing(i).name;
            t = strfind (name, folder);
            if t == 1;
                patientName = listing(i).name;
            end
        end

        % Import HumeralHead landmarks
        if exist(sprintf('%s/%s/%d/%d/%s/CT-%s-1/amira/newHHLandmarks%s.landmarkAscii',directory, type, levelDir1, levelDir2, patientName,patientName,folder),'file') == 2
            newData1 = importdata(sprintf('%s/%s/%d/%d/%s/CT-%s-1/amira/newHHLandmarks%s.landmarkAscii',directory, type, levelDir1, levelDir2, patientName,patientName,folder), ' ', 14);
            HH = newData1.data;

        elseif exist(sprintf('%s/%s/%d/%d/%s/CT-%s-1/amira/HHLandmarks%s.landmarkAscii',directory, type, levelDir1, levelDir2, patientName,patientName,folder),'file') == 2
            warning('Using old humeral landmarks definition')
            newData1 = importdata(sprintf('%s/%s/%d/%d/%s/CT-%s-1/amira/newHHLandmarks%s.landmarkAscii',directory, type, levelDir1, levelDir2, patientName,patientName,folder), ' ', 14);
            HH = newData1.data;
        else
            error('MATLAB:rmpath:DirNotFound',...
                'Could not find humeral head landmarks file: %s/%s/%d/%d/%s/CT-%s-1/amira/newHHLandmarks%s.landmarkAscii',directory, type, levelDir1, levelDir2, patientName,patientName,folder)
        end



    case 'normal'
        type = 'N';
        folder = sprintf('N%03d',subject); % format the patient name to N###
        subjectString = num2str(subject);
        levelDir1 = str2num(subjectString(1));
        levelDir2 = str2num(subjectString(2));

        % Loop over the list of patients and extract the complete name of the specific patient
        listing = dir(sprintf('%s/%s/%d/%d', directory, type, levelDir1, levelDir2));
        for i = 1:1:numel(listing)
            name = listing(i).name;
            t = strfind (name, folder);
            if t == 1;
                patientName = listing(i).name;
            end
        end

        if exist(sprintf('%s/%s/%d/%d/%s/CT-%s-1/amira/newHHLandmarks%s.landmarkAscii',directory, type, levelDir1, levelDir2, patientName,patientName,folder),'file') == 2
            newData1 = importdata(sprintf('%s/%s/%d/%d/%s/CT-%s-1/amira/newHHLandmarks%s.landmarkAscii',directory, type, levelDir1, levelDir2, patientName,patientName,folder), ' ', 14);
            HH = newData1.data;

        elseif exist(sprintf('%s/%s/%d/%d/%s/CT-%s-1/amira/HHLandmarks%s.landmarkAscii',directory, type, levelDir1, levelDir2, patientName,patientName,folder),'file') == 2
            warning('Using old humeral landmarks definition')
            newData1 = importdata(sprintf('%s/%s/%d/%d/%s/CT-%s-1/amira/HHLandmarks%s.landmarkAscii',directory, type, levelDir1, levelDir2, patientName,patientName,folder), ' ', 14);
            HH = newData1.data;
        else
            error('MATLAB:rmpath:DirNotFound',...
                'Could not find humeral head landmarks file: %s/%s/%d/%d/%s/CT-%s-1b/amira/HHLandmarks%s.landmarkAscii',directory, type, levelDir1, levelDir2, patientName,patientName,folder)
        end

    otherwise
        error('Unexpected shoulder type.');
end

%% Fit sphere on Humeral Head landmarks
% [HC,HHRadius,HHResiduals] = fitSphere(HH);
[HC,HHRadius,~, ~] = fitSphere2(HH);

mkdir('Generated_Amira_TCL_Scripts/HumerusSphereScripts')


fid = fopen(sprintf('Generated_Amira_TCL_Scripts\\HumerusSphereScripts\\SphereScript_%s.tcl',folder),'w');
fprintf(fid,'remove CreateSphere%s HumSphere%s.surf SphereView%s Intersect\n', folder, folder, folder);
fprintf(fid,'create HxCreateSphere {CreateSphere%s}\n', folder);
fprintf(fid,'CreateSphere%s setIconPosition 20 520\n', folder);
fprintf(fid,'CreateSphere%s radius setMinMax 0 50\n', folder);
fprintf(fid,'CreateSphere%s radius setValue %f\n', folder, HHRadius);
fprintf(fid,'CreateSphere%s coords setValue 0 %f\n', folder, HC(1));
fprintf(fid,'CreateSphere%s coords setValue 1 %f\n', folder, HC(2));
fprintf(fid,'CreateSphere%s coords setValue 2 %f\n', folder, HC(3));
fprintf(fid,'CreateSphere%s fire\n', folder);
fprintf(fid,'[ {CreateSphere%s} create\n ] setLabel {HumSphere%s.surf}\n', folder, folder);
fprintf(fid,'HumSphere%s.surf setIconPosition 200 520\n', folder);
fprintf(fid,'HumSphere%s.surf master connect CreateSphere%s\n', folder, folder);
fprintf(fid,'HumSphere%s.surf fire\n', folder);
fprintf(fid,'create HxDisplaySurface {SphereView%s}\n', folder);
fprintf(fid,'SphereView%s setIconPosition 400 520\n', folder);
fprintf(fid,'SphereView%s data connect HumSphere%s.surf\n', folder, folder);
fprintf(fid,'SphereView%s drawStyle setValue 4\n', folder);
fprintf(fid,'SphereView%s fire\n', folder);
fprintf(fid,'create HxOverlayGrid Intersect\n');
fprintf(fid,'{Intersect} {data} connect HumSphere%s.surf\n', folder);
fprintf(fid,'{Intersect} {module} connect ObliqueSlice4\n');
fprintf(fid,'{Intersect} {lineWidth} setIndex 0 2\n');
fprintf(fid,'ObliqueSlice4 setPlane %f %f %f 1 0 0 0 1 0\n', HC);
fprintf(fid,'ObliqueSlice4 fire\n');
fclose(fid);
