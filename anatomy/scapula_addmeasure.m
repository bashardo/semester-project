%% scapula_measure
%
% This script returns additional measurements from the Amira files.
% 3 differents output are possible:

%  - 'References' creates a text file in References folder containing coordinates to built in Solidworks 2 Coordinates
%     Systems. One for Abaqus and one for the scapula. There are two
%     additional coordinates to built the glenoid centerline.
%
%  - 'density' creates a textfile in CylinderReferences folder containing
%     an Amira script used in the density measurements protocol. It is needed to adjust the Cylinder at the correct position and with the correct size.
%
%  - 'obliqueSlice' creates a textfile in obliqueSlice folder containing an
%     Amira script used in Amira to obtain an image of the Friedman plane (2D
%     glenoid orientation measurement) and an image for muscles measurement.
%
%  - 'display' creates a textfile in "display" folder containing an Amira
%     script used to display the glenoid sphere and the scapula plane.


% Author: EPFL-LBO
% Date: 2016-09-05
%
%%

% Output is 'References' or 'obliqueSlice' 'display' or 'density'
% Type is 'normal' or 'pathologic'
% Subject is the patient number


function scapula_addmeasure(output,type,subject)

directory = 'Y:/data';
%'../../../data';  % Define directory

switch type
    case 'pathologic'
        location = 'P';
        CTn = sprintf('P%d',str2num(subject));   % format the patient name to P###
        levelDir1 = str2num(subject(1));
        levelDir2 = str2num(subject(2));
        
        
        finalDirectory = sprintf('%s/%s/%d/%d', directory, location, levelDir1, levelDir2)
        
        listing = dir(sprintf('%s/%s/%d/%d', directory, location, levelDir1, levelDir2));
        for i = 1:1:numel(listing)
            name = listing(i).name;
            t = strfind(name, CTn);
            if t == 1;
                patientName = listing(i).name;
            end
        end
        
        
    case 'normal'
        location = 'N';
        CTn = sprintf('N%03d',subject);   % format the patient name to N###
        patient = num2str(subject);
        levelDir1 = str2num(patient(1));
        levelDir2 = str2num(patient(2));
        finalDirectory = sprintf('%s/%s/%d/%d', directory, location, levelDir1, levelDir2);
        
        listing = dir(sprintf('%s/%s/%d/%d', directory, location, levelDir1, levelDir2));
        for i = 1:1:numel(listing)
            name = listing(i).name;
            t = strfind(name, CTn);
            if t == 1;
                patientName = listing(i).name;
            end
        end
end

switch output
    
    case 'References'
        output = 'References';
        scapula_calculation(patientName, finalDirectory, location, output);
        
    case 'obliqueSlice'
        output = 'obliqueSlice';
        scapula_calculation(patientName, finalDirectory, location, output);
        
    case 'density'
        output = 'density';
        scapula_calculation(patientName, finalDirectory, location, output);
        
    case 'display'
        output = 'display';
        scapula_calculation(patientName, finalDirectory, location, output);
end
end
