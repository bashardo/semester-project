classdef (Abstract) MethodsMonitor < handle
% NOT SAFE TO USE -
% IMPLEMENTATION ONGOING
%
% This class purpose is to become a superclass for all other classes we want to monitor.
% Here, monitoring consist in analysing the subclass' methods and their execution.
% Monitored data would be the status of the methods ('done','failed','available'...),
% the error they raised, their running time.
% It will also be possible to 'order' methods (meaning calling them through MethodsMonitor)
% and set ordering requirements (e.g. the status of other methods). 

  properties (SetObservable)
    methodsMonitor
  end


  methods
    function startMethodsMonitoring(obj)
      objectMethods = obj.getObjectMethods;

      obj.methodsMonitor = table();
      for i = 1:length(objectMethods)
        obj.methodsMonitor = [obj.methodsMonitor, {'';'';''}];
      end
      obj.methodsMonitor.Properties.VariableNames = objectMethods;
      obj.methodsMonitor.Properties.RowNames = {'state';'runtime';'comment'};

      addlistener(obj,'methodsMonitor','PostSet',@obj.launchOrderedMethods);
      obj.setAllMethodsUnavailable;
    end

    function orderMethod(obj,method)
      obj.setMethodState(method,'ordered')
    end

    function setAllMethodsAvailable(obj)
      obj.setAllMethodsState('available');
    end

    function setMethodAvailable(obj,method)
      obj.setMethodState(method,'available')
    end

    function setAllMethodsUnavailable(obj)
      obj.setAllMethodsState('unavailable');
    end

    function setMethodUnavailable(obj,method)
      obj.setMethodState(method,'unavailable')
    end

    function setMethodComment(obj,method,comment)
      obj.setMethodVariableValue(method,'comment',comment);
    end

    function output = isMethodAvailable(obj,method)
      output = isequal(obj.(method)('state'),{'available'});
    end
  end



  methods (Access = protected)
    function output = getObjectMethods(obj)
      objectMethods = methods(obj);
      handleMethods = methods('handle');
      MethodsMonitorMethods = methods('MethodsMonitor');
      objectMethods = cellArraysDifference(objectMethods,handleMethods,MethodsMonitorMethods);

      output = objectMethods;
    end

    function launchOrderedMethods(obj,~,~)
      method = obj.getNextOrderedMethod;
      while not(isempty(method))
        obj.runMethod(method);
        method = obj.getNextOrderedMethod;
      end
    end

    function setAllMethodsState(obj,state)
      objectMethods = obj.getObjectMethods;
      for i = 1:length(objectMethods)
        obj.setMethodState(objectMethods{i},state);
      end
    end

    function setMethodState(obj,method,state)
      obj.setMethodVariableValue(method,'state',state);
    end

    function setMethodRuntime(obj,method,runtime)
      obj.setMethodVariableValue(method,'runtime',runtime);
    end

    function setMethodVariableValue(obj,method,variable,value)
      obj.methodsMonitor.(method)(variable) = {value};
    end

    function runMethod(obj,method)
      Timer.start;
      try
        eval(['obj.' method ';']);
        obj.setMethodState(method,'done');
        obj.setMethodComment(method,'');
      catch ME
        obj.setMethodState(method,'failed');
        obj.setMethodComment(method,ME.message)
      end
      obj.setMethodRuntime(method,Timer.stop);
    end

    function output = getNextOrderedMethod(obj)
      output = [];
      objectMethods = obj.methodsMonitor.Properties.VariableNames;
      for i = 1:length(objectMethods)
        if isequal(obj.methodsMonitor.(objectMethods{i})('state'),{'ordered'})
          output = objectMethods{i};
          return;
        end
      end
    end
  end

end
