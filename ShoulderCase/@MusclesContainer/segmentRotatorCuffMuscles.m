function segmentRotatorCuffMuscles(obj,sliceName)
  % sliceName is the part before the '_ForSegmentation.png' part of the name of
  % the file that is sent to rcseg.
  cleanRotatorCuffSegmentationWorkspace(obj);
  sendImageToRotatorCuffSegmentationWorkspace(obj,sliceName);
  callRotatorCuffSegmentation(obj);

  % (Re)initialize the muscle objects to refer to the new segmentation results
  obj.addMuscle('IS',sliceName);
  obj.addMuscle('SS',sliceName);
  obj.addMuscle('SC',sliceName);
  obj.addMuscle('TM',sliceName);

  saveSegmentationResults(obj);
  cleanRotatorCuffSegmentationWorkspace(obj);
end



function cleanRotatorCuffSegmentationWorkspace(obj)
  rotatorCuffSegmentationPath = ConfigFileExtractor.getVariable('pythonDir');
  delete(fullfile(rotatorCuffSegmentationPath,'input','*'));
  delete(fullfile(rotatorCuffSegmentationPath,'IS','*'));
  delete(fullfile(rotatorCuffSegmentationPath,'SC','*'));
  delete(fullfile(rotatorCuffSegmentationPath,'SS','*'));
  delete(fullfile(rotatorCuffSegmentationPath,'TM','*'));
end



function sendImageToRotatorCuffSegmentationWorkspace(obj,sliceName)
  SCase = obj.shoulder.SCase;
  rotatorCuffSegmentationPath = ConfigFileExtractor.getVariable('pythonDir');
  imageForSegmentationPath = fullfile(obj.slicesDataPath,[sliceName '_ForSegmentation.png']);
  copyfile(imageForSegmentationPath,fullfile(rotatorCuffSegmentationPath,'input',[SCase.id '.png']));
end



function callRotatorCuffSegmentation(obj)
  %% Apply UNIBE method to automatically segment normal muscle

  pythonDir = ConfigFileExtractor.getVariable('pythonDir');

  pythonCommandActivateEnvironment= ['source ' fullfile(pythonDir,'venv','bin','activate') ';'];
  pythonCommandMoveToSegmentationWorkspace = ['cd ' pythonDir ';'];
  pythonCommandExecuteSegmentation = [fullfile(pythonDir,'rcseg.py') ' segment input' ';'];

  [~, pythonCommandOutput] = system([pythonCommandActivateEnvironment,...
                                     pythonCommandMoveToSegmentationWorkspace,...
                                     pythonCommandExecuteSegmentation]);
  disp(pythonCommandOutput);
end



function saveSegmentationResults(obj)
  SCaseID = obj.shoulder.SCase.id;
  muscleNames = fields(obj.list);
  rotatorCuffSegmentationPath = ConfigFileExtractor.getVariable('pythonDir');

  for i = 1:length(muscleNames)
    try
      copyfile(fullfile(rotatorCuffSegmentationPath,muscleNames{i},[SCaseID '.png']),...
               fullfile(obj.list.(muscleNames{i}).maskDataPath,'auto_Mask.png') );
    catch ME
      warning(ME.message)
    end
  end
end
