function output = plot(obj,varargin)
  % For now this function is specific to plotting the rotator cuff segmentation
  % results

  if nargin == 2
    maskName = [varargin{1} '.png'];
  else
    maskName = 'auto_Mask.png';
  end

  SCase = obj.shoulder.SCase;
  muscleNames = fields(obj.list);

  rotatorCuffCrossSection = imread(fullfile(obj.slicesDataPath,...
                                            [obj.list.(muscleNames{1}).sliceName '_ForSegmentation.png']));

  leg = {};
  plotHandle(1) = imshow(rotatorCuffCrossSection);
  hold on
  colors = {'g','r','b','y'};

  for i = 1:length(muscleNames)
    try
      segmentedImage = imread(fullfile(obj.list.(muscleNames{i}).maskDataPath,maskName));
    catch
      continue
    end

    if (max(segmentedImage,[],'all') > 0)   % if segmentation result is not empty
      leg{end+1} = muscleNames{i};
      [~,plotHandle(i+1)] = contour(segmentedImage,'color',colors{i});
    end
  end
  plotHandle(i+2) = legend(leg);
  output = plotHandle;
end
