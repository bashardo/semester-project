classdef MusclesContainer < handle
% Replaces the deprecated Muscles class.
%
% One key features of MusclesContainer is to create the dicom
% slices and to call the automatic rotator cuff segmentation
% on them. These methods might be extracted at some point.
%
% The plot() methods create a figure with the rotator cuff slice
% with the segmented muscles contour superposed.
%
% Also handle a list of Muscle instances a display a summary of
% these muscles measurements.
%
% New muscles can easily be added and measured through this class,
% however their slicing/segmentation will remain to be implemented.

  properties
    summary = [];
    list = {};
  end



  properties (Hidden = true)
    dataPath = '';
    slicesDataPath
    shoulder
  end



  methods

    function obj = MusclesContainer(shoulder)
      obj.shoulder = shoulder;
      obj.clearSummary();
    end

    function propagateDataPath(obj)
      % Update current dataPath
      % Propagate the path to objects in properties

      obj.dataPath = fullfile(obj.shoulder.dataPath,'muscles');
      obj.slicesDataPath = fullfile(obj.dataPath,'oblique_slices');

      if not(isfolder(obj.dataPath))
        mkdir(obj.dataPath);
      end
      if not(isfolder(obj.slicesDataPath))
        mkdir(obj.slicesDataPath);
      end

      if obj.isEmpty
        return 
      end
      
      structfun(@(muscle) muscle.propagateDataPath, obj.list);
    end

    function addMuscle(obj,muscleName,sliceWithMuscleFilename)
      newMuscle = Muscle(obj,muscleName,sliceWithMuscleFilename);
      newMuscle.propagateDataPath;
      obj.list.(muscleName) = newMuscle;

      obj.updateSummary;
    end

    function updateSummary(obj)
      muscleValues = obj.getAllMusclesValues();
      if isempty(muscleValues)
        obj.clearSummary();
        return
      end

      muscleProperties = properties('Muscle');
      obj.summary = cell2table(muscleValues(:,2:end),...
                               'VariableNames',muscleProperties(2:end),...
                               'RowNames',muscleValues(:,1));
    end
    
    function clearSummary(obj)
      muscleProperties = properties('Muscle');
      obj.summary = array2table(zeros(1,length(muscleProperties)-1),...
                                'VariableNames',muscleProperties(2:end));
    end

    function output = getAllMusclesValues(obj)
      output = {};
      if obj.isEmpty
        return
      end

      muscleNames = fields(obj.list);
      for i = 1:length(muscleNames)
        output(end+1,:) = obj.list.(muscleNames{i}).getValues';
      end
    end

    function measureAllMuscles(obj,segmentationSet)
      if obj.isEmpty
        return 
      end

      muscleNames = fields(obj.list);
      for i = 1:length(fields(obj.list))
        try
          obj.list.(muscleNames{i}).measure(segmentationSet);
        catch ME
          warning(ME.message);
        end
      end
      obj.updateSummary;
    end

    function output = isEmpty(obj)
      output = isempty(obj.list);
    end 

  end

end
