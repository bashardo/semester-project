function createRotatorCuffSlices(obj)
  % Images and pixel spacings are saved at:
  % shoulder/dataDev/*/*/*/SCase-IPP/CT-SCase-IPP/matlab/shoulder/muscles/oblique_slice/
  assert(not(obj.shoulder.scapula.coordSys.isEmpty),'Scapula coordinate system not measured.');
  [imageForSegmentation,imageForMeasurements,imagesPixelSpacings] = getSlices(obj);
  saveImages(obj,imageForSegmentation,imageForMeasurements);
  saveImagesPixelSpacings(obj,imagesPixelSpacings);
end



function [slice8Bit,slice16Bit,slicesPixelSpacings] = getSlices(obj)
  SCase = obj.shoulder.SCase;
  scapula = obj.shoulder.scapula;
  path = SCase.dataDicomPath;
  rotatorCuffSlicer = DicomVolumeSlicer(path);

  % The 3D volume must be normalised for the slices not to be distorted
  rotatorCuffSlicer.setMinimalResolutionDividedBy(2);
  rotatorCuffSlicer.normaliseVolume;
  rotatorCuffSlicer.slice(scapula.coordSys.origin,scapula.coordSys.ML);

  % The following lines set the slice to display a 300x300 mm area around the
  % scapula with the "Y" shape of the scapula oriented up
  rotatorCuffSlicer.orientSliceUpwardVector(scapula.coordSys.origin,scapula.coordSys.IS);
  rotatorCuffSlicer.crop(scapula.coordSys.origin-100*scapula.coordSys.IS,300,300);

  % Rotator cuff segmentation code requires 1024x1024 pixels images
  if size(rotatorCuffSlicer.sliced,1) > size(rotatorCuffSlicer.sliced,2)
    rotatorCuffSlicer.resize([1024 nan]);
  else
    rotatorCuffSlicer.resize([nan 1024]);
  end
  rotatorCuffSlicer.addEmptyBackgroundToSlice(1024,1024);


  rawSlice = rotatorCuffSlicer.sliced;

  rotatorCuffSlicer.rescaleSliceToUint8;
  slice8Bit = rotatorCuffSlicer.sliced;
  rotatorCuffSlicer.sliced = rawSlice;

  rotatorCuffSlicer.rescaleSliceToInt16;
  slice16Bit = rotatorCuffSlicer.sliced;

  % Flip the slices for right and left shoulders to give similar slices
  if obj.shoulder.side=='L'
    slice8Bit=rot90(flipud(slice8Bit),2);
    slice16Bit=rot90(flipud(slice16Bit),2);
  end

  slicesPixelSpacings = rotatorCuffSlicer.slicedPixelSpacings;
end



function saveImages(obj,imageForSegmentation,imageForMeasurements)
  imwrite(imageForSegmentation,...
          fullfile(obj.slicesDataPath,'rotatorCuff_ForSegmentation.png'));
  save(fullfile(obj.slicesDataPath,'rotatorCuff_ForMeasurements.mat'),...
       'imageForMeasurements');
end



function saveImagesPixelSpacings(obj,imagesPixelSpacings)
  save(fullfile(obj.slicesDataPath,'rotatorCuff_PixelSpacings.mat'),...
       'imagesPixelSpacings');
end
