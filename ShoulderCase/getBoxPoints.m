function boxPoints = getBoxPoints(points,minXYZ,maxXYZ)
    % Returns the points from the given points that are in the given boundaries.
    
    constraintMin = repmat(minXYZ,size(points,1),1);
    constraintMax = repmat(maxXYZ,size(points,1),1);
    
    validCoordinates = and(points>=constraintMin, points<=constraintMax);
    validPoints = all(validCoordinates,2);
    
    boxPoints = points(validPoints,:);
end

