function index = findShortest3DVector(vectors)
  dimension = find(size(vectors) == 3);
  if (dimension ~= 2)
    error('You should provide an Nx3 vector array');
    index = 0;
    return
  end
  type = 2;
  norms = vecnorm(vectors,type,dimension);
  index = find(norms == min(norms));
end
