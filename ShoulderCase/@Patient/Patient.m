classdef Patient < handle
    %CLASSPATIENT Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        id % id in the anonymized database
        idMed % id of the medical center (anonymous key, but IPP while SecTrial not active)
        gender % M or F
        age % year at toime of preoperative CTscan
        ethnicity
        weight
        height
        BMI
        comment
    end
    
     properties (Hidden = true)
        SCase
    end
   
    methods (Access = ?ShoulderCase)  % Only ShoulderCase is allowed to construct a Patient
        function obj = Patient(SCase)
            %CLASSPATIENT Construct an instance of this class
            %   Detailed explanation goes here
            obj.SCase     = SCase;
            obj.id        = SCase.id;
            obj.idMed     = '';
            obj.gender    = '';
            obj.age       = [];
            obj.ethnicity = '';
            obj.weight    = [];
            obj.height    = [];
            obj.BMI       = [];
            obj.comment   = '';
        end
    end
end

