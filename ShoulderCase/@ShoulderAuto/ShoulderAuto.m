classdef ShoulderAuto < Shoulder
% Shoulder using automatically measured data.
% Instanciate a ScapulaAuto object.

  methods (Access = protected)

    function createScapula(obj)
      obj.scapula = ScapulaAuto(obj);
    end

  end



  methods

    function propagateDataPath(obj)
      % Update current dataPath
      % Propagate the path to objects in properties

      assert(not(isempty(obj.SCase.dataMatlabPath)),'SCase.dataMatlabPath is empty')
      obj.dataPath = fullfile(obj.SCase.dataMatlabPath,'shoulder_auto');

      if not(isfolder(obj.dataPath))
        mkdir(obj.dataPath);
      end

      obj.muscles.propagateDataPath;
    end

  end

end
