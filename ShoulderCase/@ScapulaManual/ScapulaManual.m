classdef ScapulaManual < Scapula
% To be used with ShoulderManual data.
% The load() method requires a specific implementation.
% Instanciate a GlenoidManual object.

  methods

    function createGlenoid(obj)
      obj.glenoid = GlenoidManual(obj);
    end

    function outputArg = load(obj)
      % LOAD Load segmented surface and landmnarks
      %   Load the segmented scapula surface (if exist) and the
      %   scapula landmarks (6 scapula, 5 groove, 5 pillar) from
      %   amira directory.

      SCase = obj.shoulder.SCase;
      SCaseId4C = SCase.id4C;
      amiraDir = SCase.dataAmiraPath;
      matlabDir = SCase.dataMatlabPath;
      outputArg = 1; % Should report on loading result

      % Scapula (6) landmarks
      fileName = ['ScapulaLandmarks' SCaseId4C '.landmarkAscii'];
      fileName = [amiraDir '/' fileName];

      if exist(fileName,'file') == 2
          importedData = importdata(fileName, ' ', 14);

      else
          disp(['MATLAB:rmpath:DirNotFound',...
              'Could not find file: ',fileName]);
          outputArg = 0;
          return;
      end
      % Check that imported data are well formed
      try
          landmarks = importedData.data;
      catch
          outputArg = 0;
          return;
      end

      obj.angulusInferior      = landmarks(1,:);
      obj.trigonumSpinae       = landmarks(2,:);
      obj.processusCoracoideus = landmarks(3,:);
      obj.acromioClavicular    = landmarks(4,:);
      obj.angulusAcromialis    = landmarks(5,:);
      obj.spinoGlenoidNotch    = landmarks(6,:);

      % Scapula suprapinatus groove (5) landmarks
      fileName = ['ScapulaGrooveLandmarks' SCaseId4C '.landmarkAscii'];
      fileName = [amiraDir '/' fileName];
      if exist(fileName,'file') == 2
          importedData = importdata(fileName, ' ', 14);
          % Check that imported data are well formed
          try
              landmarks = importedData.data;
          catch
              outputArg = 0;
              return;
          end
      else
          disp(['MATLAB:rmpath:DirNotFound',...
              'Could not find file: ',fileName]);
          outputArg = 0;
      end
      % Check groove landmarks validity
      if length(landmarks) > 5
          landmarks = landmarks(1:5,:);
          warning(['More than 5 groove landmarks(' SCase.id ')']);
      elseif length(landmarks) < 5
          error('Less than 5 groove landmarks');
      end

      obj.groove = landmarks;

      % Scapula pillar (5) landmarks
      fileName = ['ScapulaPillarLandmarks' SCaseId4C '.landmarkAscii'];
      fileName = [amiraDir '/' fileName];
      if exist(fileName,'file') == 2
          importedData = importdata(fileName, ' ', 14);
          % Check that imported data are well formed
          try
              landmarks = importedData.data;
          catch
              outputArg = 0;
              return;
          end
      else
          disp(['MATLAB:rmpath:DirNotFound',...
              'Could not find file: ',fileName]);
          outputArg = 0;
      end
      obj.pillar = landmarks;

      % Import scapula surface
      % Try manual segmentation in amira folder
      fileName = ['scapula_' SCaseId4C '.stl'];
      fileName = [amiraDir '/' fileName];
      if exist(fileName,'file') == 2
          try
              [points,faces,~]=loadStl(fileName,1);
              obj.surface.points = points;
              obj.surface.faces = faces;
              obj.segmentation = 'M'; % Manual segmentation from amira
          catch
              obj.segmentation = 'E'; % Error in loading
          end
      else
          obj.segmentation = 'N'; % No segmentation
      end
      try
        obj.measurePlane;
      catch
        outputArg = 0;
      end
    end

  end



end
