classdef CoordinateSystemScapula < CoordinateSystem
% Add the anatomical axes as properties.


  properties
    ML = [];
    PA = [];
    IS = [];
  end


  
  methods

    function obj = CoordinateSystemScapula()
    end

    function set.PA(obj,value)
      obj.xAxis = value;
      obj.PA = value;
    end

    function set.IS(obj,value)
      obj.yAxis = value;
      obj.IS = value;
    end

    function set.ML(obj,value)
      obj.zAxis = value;
      obj.ML = value;
    end

  end

end
