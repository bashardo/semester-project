function output = getSmoothDicomPath(obj)
  % Return the path to the dicom folder containing 'Smooth' files
  
  output = [];
  smoothDicomPath = fullfile([obj.dataCTPath(1:end-1) '2'],'dicom');
  assert(isfolder(smoothDicomPath),'No smooth dicom for this SCase. %s is not a valid folder',smoothDicomPath);
  output = smoothDicomPath;
end
