classdef ShoulderCase < handle
    % Properties and methods associated to the SoulderCase object.

    % Author: Alexandre Terrier, EPFL-LBO
    %         Matthieu Boubat, EPFL-LBO
    % Creation date: 2018-07-01
    % Revision date: 2020-07-23

    % TODO:
    % Need method to load properties (from amira, matlab, excel, SQL)
    % Need method to save properties (in matlab, excel, SQL)
    % Remove datapath from constants
    %
    properties
        id = [];    % id of the shoulder case, as it appears in the database (Pnnn)
        diagnosis = [];
        treatment = [];
        outcome = [];
        patient = [];
        shoulderManual = [];
        shoulderAuto = [];
        study = [];
        comment = [];
    end

%     properties (Constant, Hidden = true)
%         dataPath = '/Volumes/shoulder/dataDev'; % path to data folder from package folder
%         % This should be done differently. Check where we are, and where we
%         % should go.
%     end

    properties (Hidden = true)
        dataPath = [];

        dataCTPath = [];
        dataAmiraPath = [];
        dataMatlabPath = [];
        dataDicomPath = [];

        id4C = []; % SCaseId with P/N followed by 3 digits --> 4 char
    end



    methods (Access = ?ShoulderCaseLoader)

        function obj = ShoulderCase(SCaseID,dataCTPath)
            % SCaseID validation
            rawSCaseID = SCaseIDParser(SCaseID);
            assert(rawSCaseID.isValidID,'The input argument is not a valid SCaseID.')
            obj.id = SCaseID;
            obj.id4C = rawSCaseID.getIDWithNumberOfDigits(4);

            % path attribution
            obj.dataCTPath = dataCTPath;

            % Initialsation
            obj.patient   = Patient(obj);
            obj.shoulderManual  = ShoulderManual(obj);
            obj.shoulderAuto  = ShoulderAuto(obj);

            obj.propagateDataPath;
        end

      end



      methods


        function propagateDataPath(obj)
            % Update current dataPath
            % Propagate the path to objects in properties
            
            obj.dataAmiraPath  = fullfile(obj.dataCTPath,'amira');
            obj.dataMatlabPath = fullfile(obj.dataCTPath,'matlab');
            obj.dataDicomPath = fullfile(obj.dataCTPath,'dicom');

            obj.shoulderManual.propagateDataPath;
            obj.shoulderAuto.propagateDataPath;
        end

        function outputArg = output(obj, varargin)
            % Below is copy/past from previous version
            % This function is used to output the variable Results, used
            % by scapula_measurePKG the modified funcion scapula_measure.
            % inputArg could be as in scapula_calculation
            % 'density', 'References', 'obliqueSlice', 'display'

            Result.SCase_id               = obj.id;                                      %  1
            Result.glenoid_Radius         = obj.shoulderManual.scapula.glenoid.radius;         %  2
            Result.glenoid_radiusRMSE     = obj.shoulderManual.scapula.glenoid.fittedSphere.RMSE;     %  3                              %  3
            %Result.glenoidSphericity     = ' ';
            %Result.glenoid_biconcave     = ' ';
            Result.glenoid_depth          = obj.shoulderManual.scapula.glenoid.depth;          %  4
            Result.glenoid_width          = obj.shoulderManual.scapula.glenoid.width;          %  5
            Result.glenoid_height         = obj.shoulderManual.scapula.glenoid.height;         %  6
            Result.glenoid_center_PA      = obj.shoulderManual.scapula.glenoid.centerLocal(1); %  7
            Result.glenoid_center_IS      = obj.shoulderManual.scapula.glenoid.centerLocal(2); %  8
            Result.glenoid_center_ML      = obj.shoulderManual.scapula.glenoid.centerLocal(3); %  9
            Result.glenoid_version_ampl   = obj.shoulderManual.scapula.glenoid.versionAmplitude;    % 10
            Result.glenoid_version_orient = obj.shoulderManual.scapula.glenoid.versionOrientation;  % 11
            Result.glenoid_Version        = obj.shoulderManual.scapula.glenoid.version;        % 12
            Result.glenoid_Inclination    = obj.shoulderManual.scapula.glenoid.inclination;    % 13
            Result.humerus_joint_radius   = ' ';                                           % 14
            Result.humeral_head_radius    = obj.shoulderManual.humerus.radius;                 % 15
            % Result.humerus_GHsublux_2D = ' ';
            % Result.humerus_SHsublux_2D = ' ';
            Result.humerus_GHsubluxation_ampl   = obj.shoulderManual.humerus.GHSAmpl;          % 16
            Result.humerus_GHsubluxation_orient = obj.shoulderManual.humerus.GHSOrient;        % 17
            Result.humerus_SHsubluxation_ampl   = obj.shoulderManual.humerus.SHSAmpl;          % 18
            Result.humerus_SHsubluxation_orient = obj.shoulderManual.humerus.SHSOrient;        % 19
            Result.scapula_CSA            = obj.shoulderManual.scapula.acromion.criticalShoulderAngle; % radCSA;
            % 5 Lines below should be updated
            Result.scapula_CTangle = 0; %CTorientation; %20
            Result.scapula_CTangleVersion = 0; % WearPlaneAngle; %21
            Result.scapula_CTangleSHS = 0; % SHSPlaneAngle; % 22
            Result.scapula_CTangleGHS = 0; % GHSPlaneAngle; % 23
            Result.scapula_PlaneRMSE = 0; %PlaneRMSE;%24

            Result.scapula_AI             = obj.shoulderManual.scapula.acromion.acromionIndex;

            outputArg = Result;
        end

        function outputArg = saveMatlab(obj)
            % Save SCase to matlab file
            dir = obj.dataMatlabPath;
            % Create dir if not exist
            try
                if ~exist(dir, 'dir') % create directory if it does not exist
                    mkdir(dir);
                end
            catch
                fprintf('Error creating the matlab directory \n'); % Should be in log
            end

            % Save SCase in matlab directoty, in a file named SCaseCNNN.m
            filename = 'SCase';
            filename = [dir '/' filename '.mat'];
            try
                SCase = obj;
                save(filename, 'SCase');
                outputArg = 1;
            catch
                fprintf('Error creating SCase matlab file \n'); % Should be in log
                outputArg = -1;
            end
        end

        function outputArg = appendToCSV(obj,filename)
            % Save SCase to csv file
            logFid  = fopen('log/measureSCase.log', 'a');

            dataDir = ConfigFileExtractor.getVariable('dataDir');
            xlsDir  = [dataDir '/Excel/xlsFromMatlab'];

            fid = fopen([xlsDir '/' filename],'a');

            fprintf(fid,[...
                obj.id ','...                                                          % SCase_id
                obj.shoulderManual.side ','...                                           % shoulder_side
                num2str(obj.shoulderManual.scapula.glenoid.radius) ','...                % glenoid_radius
                num2str(obj.shoulderManual.scapula.glenoid.fittedSphere.RMSE) ','...     % glenoid_sphereRMSE
                num2str(obj.shoulderManual.scapula.glenoid.depth) ','...                 % glenoid_depth
                num2str(obj.shoulderManual.scapula.glenoid.width) ','...                 % glenoid_width
                num2str(obj.shoulderManual.scapula.glenoid.height) ','...                % glenoid_height
                num2str(obj.shoulderManual.scapula.glenoid.centerLocal.x) ','...         % glenoid_centerPA
                num2str(obj.shoulderManual.scapula.glenoid.centerLocal.y) ','...         % glenoid_centerIS
                num2str(obj.shoulderManual.scapula.glenoid.centerLocal.z) ','...         % glenoid_centerML
                num2str(obj.shoulderManual.scapula.glenoid.versionAmplitude) ','...           % glenoid_versionAmpl
                num2str(obj.shoulderManual.scapula.glenoid.versionOrientation) ','...         % glenoid_versionOrientation
                num2str(obj.shoulderManual.scapula.glenoid.version) ','...               % glenoid_version
                num2str(obj.shoulderManual.scapula.glenoid.inclination) ','...           % glenoid_inclination
                num2str(obj.shoulderManual.humerus.jointRadius) ','...                   % humerus_jointRadius
                num2str(obj.shoulderManual.humerus.radius) ','...                        % humerus_headRadius
                num2str(obj.shoulderManual.humerus.GHSAmpl) ','...                       % humerus_GHSAmpl
                num2str(obj.shoulderManual.humerus.GHSOrient) ','...                     % humerus_GHSOrient
                num2str(obj.shoulderManual.humerus.SHSAmpl) ','...                       % humerus_SHSAmpl
                num2str(obj.shoulderManual.humerus.SHSOrient) ','...                     % humerus_SHSOrient
                num2str(obj.shoulderManual.humerus.SHSAngle) ','...                      % humerus_SHSAgle
                num2str(obj.shoulderManual.humerus.SHSPA) ','...                         % humerus_SHSPA
                num2str(obj.shoulderManual.humerus.SHSIS) ','...                         % humerus_SHSIS
                num2str(obj.shoulderManual.scapula.acromion.AI) ','...                   % acromion_AI
                num2str(obj.shoulderManual.scapula.acromion.CSA) ','...                  % acromion_CSA
                num2str(obj.shoulderManual.scapula.acromion.PSA) ','...                  % acromion_PSA
                num2str(obj.shoulderManual.scapula.acromion.AAA) '\n'...                 % acromion_AAA
                ]);
            fclose(fid);
            fclose(logFid);

            outputArg = 1;

        end

        function outputArg = saveExcel(~)
            % Save SCase to Excel file


            outputArg = 1;

        end

        function outputArg = saveSQL(~)
            % Save SCase to MySQL database


            outputArg = 1;

        end
    end
end
