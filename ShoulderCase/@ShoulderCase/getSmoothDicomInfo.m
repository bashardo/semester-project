function output = getSmoothDicomInfo(obj)
  % Return the result of dicominfo() with the first dicom file found
  
  dicomFiles = dir(fullfile(obj.getSmoothDicomPath,'*.dcm'));
  assert(not(isempty(dicomFiles)),'No dicom file found there %s',obj.getSmoothDicomPath);
  output = dicominfo(fullfile(dicomFiles(1).folder,dicomFiles(1).name));
end
