function loadDicomInfoFromFolder(obj,dicomFolderPath)
  obj.assertPathContainsDicomFiles(dicomFolderPath);
  dicomFiles = dir(fullfile(dicomFolderPath,'*.dcm'));
  obj.dicomInfo = dicominfo(fullfile(dicomFiles(1).folder,dicomFiles(1).name));
end
