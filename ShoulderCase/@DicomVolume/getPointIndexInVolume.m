function output = getPointIndexInVolume(obj,point)
  % the output are the three indices of the given point in the given volume.
  % i,j,k: output indices
  k = find(abs(obj.spatial.PatientPositions(:,3)-point(3)) == min(abs(obj.spatial.PatientPositions(:,3)-point(3))));
  i = round( (point(1) - obj.spatial.PatientPositions(k,1)) / obj.spatial.PixelSpacings(k,1) );
  j = round( (point(2) - obj.spatial.PatientPositions(k,2)) / obj.spatial.PixelSpacings(k,2) );
  output = [i,j,k];
end
