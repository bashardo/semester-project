function output = getPointCoordinates(obj,pointIndex)
  % this function is the inverse of obj.getPointIndexInVolume
  x = obj.spatial.PatientPositions(pointIndex(3),1) + ( obj.spatial.PixelSpacings(pointIndex(3),1) * pointIndex(1) );
  y = obj.spatial.PatientPositions(pointIndex(3),2) + ( obj.spatial.PixelSpacings(pointIndex(3),2) * pointIndex(2) );
  z = obj.spatial.PatientPositions(pointIndex(3),3);
  output = [x,y,z];
end
