function setVolumeToSubvolume(obj,center,x,y,z)
  % NOT SAFE TO USE -
  % NEED FURTHER TEST AND FIX
  % 
  % Set the volume around the center point with x, y, and z given in milimeters.
  
  % find boundaries
  volumeSize = size(obj.volume);
  minXYZ = obj.getPointIndexInVolume(center - [x/2 y/2 z/2]);
  maxXYZ = obj.getPointIndexInVolume(center + [x/2 y/2 z/2]);
  left = max(1,minXYZ(1));
  right = min(volumeSize(1),maxXYZ(1));
  front = max(1,minXYZ(2));
  rear = min(volumeSize(2),maxXYZ(2));
  bottom = max(1,minXYZ(3));
  top = min(volumeSize(3),maxXYZ(3));

  % set subvolume
  obj.volume = obj.volume(left:right,front:rear,bottom:top);

  % update spatial
  obj.spatial.PixelSpacings = obj.spatial.PixelSpacings(bottom:top,:);

  obj.spatial.PatientPositions = obj.spatial.PatientPositions(bottom:top,:);
  obj.spatial.PatientPositions(:,1) = obj.spatial.PatientPositions(:,1) + left*obj.spatial.PixelSpacings(:,1);
  obj.spatial.PatientPositions(:,2) = obj.spatial.PatientPositions(:,2) + front*obj.spatial.PixelSpacings(:,2);

  obj.spatial.PatientOrientations = obj.spatial.PatientOrientations(:,:,bottom:top);
end
