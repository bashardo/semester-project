classdef (Abstract) DicomVolume < handle
% Used to manipulate the data given by dicomreadVolume() and dicominfo().
% 
% Can be used to retrieve a point coordinates given its indices, and vice
% versa.

  properties
    volume = []
    spatial = []
    dicomInfo = []
    dicomFolderPath = []
  end



  methods
    function set.volume(obj,volume)
      assert(length(size(volume))>=3,'Volume must be a 3D array')
      obj.volume = double(squeeze(volume));
    end

    function set.spatial(obj,spatial)
      requiredStructureField = {'PatientPositions','PixelSpacings','PatientOrientations'};
      assert(all(contains(fields(spatial),requiredStructureField)),'Invalid spatial structure');
      obj.spatial = spatial;
    end
  end
end
