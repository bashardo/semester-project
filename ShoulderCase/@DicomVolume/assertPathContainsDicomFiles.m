function assertPathContainsDicomFiles(obj,path)
  assert(isfolder(path),'Provided argument is not a valid folder path');
  dicomFiles = dir(fullfile(path,'*.dcm'));
  assert(not(isempty(dicomFiles)),'No dicom file found there %s',path);
end
