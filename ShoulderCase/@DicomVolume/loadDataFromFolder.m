function loadDataFromFolder(obj,dicomFolderPath)
  obj.assertPathContainsDicomFiles(dicomFolderPath);
  obj.dicomFolderPath = dicomFolderPath;
  [obj.volume,obj.spatial,~] = dicomreadVolume(dicomFolderPath);
  obj.loadDicomInfoFromFolder(dicomFolderPath);
end
