function [box1Points,normal,meanX,residuals,rmse,R2] = box1Plane(SCase)
j=0;
sPoints=SCase.shoulderAuto.scapula.coordSys.express(SCase.shoulderAuto.scapula.surface.points);
AI=SCase.shoulderAuto.scapula.coordSys.express(SCase.shoulderManual.scapula.angulusInferior);
TS=SCase.shoulderAuto.scapula.coordSys.express(SCase.shoulderManual.scapula.trigonumSpinae);
AG=SCase.shoulderAuto.scapula.coordSys.express(SCase.shoulderManual.scapula.spinoGlenoidNotch);
%AC=SCase.shoulderAuto.scapula.coordSys.express(SCase.shoulderManual.scapula.angulusInferior);

    for i=1:length(sPoints)
        if(sPoints(i,2)>=TS(1,2)-80 && sPoints(i,2)<TS(1,2) && sPoints(i,3)>TS(1,3) && sPoints(i,3)<AG(1,3) )
        j=j+1;
        box1Points(j,:)=sPoints(i,:);
        end
    end
    j=0;
    for i=1:length(box1Points)
        if(box1Points(i,2)>=(TS(1,2)-7.5) && (box1Points(i,1)>=(TS(1,1)+5) || box1Points(i,1)<=(TS(1,1)-5)))
        j=j+1;
        box1Points(i,:)= 0;
        end
    end
[normal,meanX,residuals,rmse,R2]=fitPlane(box1Points);
end

