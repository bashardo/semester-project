clc,clear,close all
planeGen;
syms x y z
%% Defining the plane equ
equ1 = SCase.shoulderAuto.scapula.InferoSuperior.plane.normal(1,1)*...
    (x-SCase.shoulderAuto.scapula.InferoSuperior.plane.point(1,1))....
    +SCase.shoulderAuto.scapula.InferoSuperior.plane.normal(1,2)...
    *(y-SCase.shoulderAuto.scapula.InferoSuperior.plane.point(1,2))...
    +SCase.shoulderAuto.scapula.InferoSuperior.plane.normal(1,3)...
    *(z-SCase.shoulderAuto.scapula.InferoSuperior.plane.point(1,3))==0;
equ2 = SCase.shoulderAuto.scapula.AntoriorSuperior.plane.normal(1,1)...
    *(x-SCase.shoulderAuto.scapula.AntoriorSuperior.plane.point(1,1))...
    +SCase.shoulderAuto.scapula.AntoriorSuperior.plane.normal(1,2)...
    *(y-SCase.shoulderAuto.scapula.AntoriorSuperior.plane.point(1,2))...
    +SCase.shoulderAuto.scapula.AntoriorSuperior.plane.normal(1,3)...
    *(z-SCase.shoulderAuto.scapula.AntoriorSuperior.plane.point(1,3))==0;
equ3 = SCase.shoulderAuto.scapula.PosteriorSuperior.plane.normal(1,1)...
    *(x-SCase.shoulderAuto.scapula.PosteriorSuperior.plane.point(1,1))...
    +SCase.shoulderAuto.scapula.PosteriorSuperior.plane.normal(1,2)...
    *(y-SCase.shoulderAuto.scapula.PosteriorSuperior.plane.point(1,2))...
    +SCase.shoulderAuto.scapula.PosteriorSuperior.plane.normal(1,3)...
    *(z-SCase.shoulderAuto.scapula.PosteriorSuperior.plane.point(1,3))==0;

%% Calculating the intersections
intersection12 = solve([equ1,equ2]);
intersection13 = solve([equ1,equ3]);
intersection23 = solve([equ2,equ3]);

xInter12 = sym2poly(intersection12.x);
yInter12 = sym2poly(intersection12.y);
p1l1 = [xInter12(2); yInter12(2) ;0];
p2l1 = [xInter12(1)+xInter12(2); yInter12(1)+yInter12(2) ;1];
dir1=(p2l1-p1l1)/norm(p2l1-p1l1);
plot3([p1l1(1,1),p2l1(1,1)],[p1l1(2,1),p2l1(2,1)],[p1l1(3,1),p2l1(3,1)],'r','lineWidth',10)
xInter13 = sym2poly(intersection13.x);
yInter13 = sym2poly(intersection13.y);
p1l2 = [xInter13(2); yInter13(2) ;0];
p2l2 = [xInter13(1)+xInter13(2); yInter13(1)+yInter13(2) ;1];
dir2=(p2l2-p1l2)/norm(p2l2-p1l2);

xInter23 = sym2poly(intersection23.x);
yInter23 = sym2poly(intersection23.y);
p1l3 = [xInter23(2); yInter23(2) ;0];
p2l3 = [xInter23(1)+xInter23(2); yInter23(1)+yInter23(2) ;1];
dir3=(p2l3-p1l3)/norm(p2l3-p1l3);


%% project SpinoGelenoid on 3 lines
SG=SCase.shoulderAuto.scapula.spinoGlenoidNotch;

SGl1=p1l1 + dot((SG' - p1l1),dir1)*dir1;
SGl2=p1l2 + dot((SG' - p1l2),dir2)*dir2;
SGl3=p1l3 + dot((SG' - p1l3),dir3)*dir3;

%% project of line 3 on scapula plane
point2project = [p1l3';p2l3'];
projected = project2Plane(point2project,SCase.shoulderAuto.scapula.InferoSuperior.plane.normal...
    ,SCase.shoulderAuto.scapula.InferoSuperior.plane.point,size(point2project,1));
dir3New = (projected(2,:)-projected(1,:))/norm(projected(2,:)-projected(1,:));

%% averging the axes
 meanDir=mean([dir1';dir2';dir3New]);
 
%% averaging the SpinoGelenoid
meanSG = mean([SGl1';SGl2';SGl3']);

%% making the z axis and origin
zAxisPoints = [meanSG+60*meanDir;meanSG-180*meanDir];
origin = meanSG +dot((SG - meanSG),meanDir)*meanDir;

%% Plotting
SCase.shoulderAuto.plot;
hold on
s=80;
scatter3(origin(1,1),origin(1,2),origin(1,3),s,'MarkerEdgeColor','r','LineWidth',4);
plot3(zAxisPoints(:,1),zAxisPoints(:,2),zAxisPoints(:,3),'k','LineWidth',5);

xaxispoint=[origin-50*SCase.shoulderAuto.scapula.InferoSuperior.plane.normal;origin+50*SCase.shoulderAuto.scapula.InferoSuperior.plane.normal];
plot3(xaxispoint(:,1),xaxispoint(:,2),xaxispoint(:,3),'b','LineWidth',5);

SCase.shoulderAuto.scapula.InferoSuperior.plane.plot(80);

plot3(xaxispoint(:,1),xaxispoint(:,2),xaxispoint(:,3),'b','LineWidth',5);
 
yaxisDir=cross(SCase.shoulderAuto.scapula.InferoSuperior.plane.normal,meanDir);
hold on
yAxisPoints = [origin+50*yaxisDir;origin-50*yaxisDir];
plot3(yAxisPoints(:,1),yAxisPoints(:,2),yAxisPoints(:,3),'G','LineWidth',5);














