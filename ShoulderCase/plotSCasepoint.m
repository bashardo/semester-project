function [] = plotSCasepoint(SCase,points)
plot(SCase.shoulderAuto);
hold on
scatter3(points(:,1),points(:,2),points(:,3),'k','*');
end