classdef LinkedList < handle
% Simple linked list implementation

  properties (Access = private, Hidden = true)
    ID
    value
    previous
    next
  end


  methods

    function obj = LinkedList
      obj.ID = 'head';
      obj.value = obj;
      obj.previous = [];
      obj.next = [];
    end

    function output = append(obj,newValue,varargin)
      if nargin == 3
        newID = varargin{1};
      else
        newID = num2str(obj.length+1);
      end
      newElement = LinkedList;
      obj.setLastElement(newElement);
      newElement.setID(newID);
      newElement.setValue(newValue);
      output = newID;
    end

    function remove(obj,IndexOrID)
      element = obj.getElement(IndexOrID);
      obj.removeElement(element);
    end

    function output = pop(obj,varargin)
      element = obj.getLastElement;
      if (nargin == 2)
        element = obj.getElement(varargin{1});
      end
      output = element.value;
      obj.removeElement(element);
    end

    function output = find(obj,ID)
      element = obj.getFirstElement;
      index = 1;
      foundLocations = [];
      if isequal(ID,element.ID)
        foundLocations = [foundLocations index];
      end
      while not(isempty(element.next))
        element = element.next;
        index = index + 1;
        if isequal(ID,element.ID)
          foundLocations = [foundLocations index];
        end
      end
      output = foundLocations;
    end

    function output = getValue(obj,IndexOrID)
      element = obj.getElement(IndexOrID);
      output = element.value;
    end

    function output = getID(obj,IndexOrID)
      element = obj.getElement(IndexOrID);
      output = element.ID;
    end

    function output = getAllValues(obj)
      element = obj.getFirstElement;
      output = {};
      while not(isempty(element.next))
        element = element.next;
        output{end+1} = element.value;
      end
    end

    function output = getAllIDs(obj)
      element = obj.getFirstElement;
      output = {};
      while not(isempty(element.next))
        element = element.next;
        output{end+1} = element.ID;
      end
    end

    function output = isEmpty(obj)
      output = (obj.length == 0);
    end

    function output = length(obj)
      element = obj.getFirstElement;
      count = 0;
      while not(isempty(element.next))
        element = element.next;
        count = count+1;
      end
      output = count;
    end

    function clear(obj)
      element = obj.getFirstElement;
      element.next = [];
    end

  end



  methods (Access = private, Hidden = true)

    function setID(obj,ID)
      assert(ischar(ID),'ID should be a character array.')
      obj.ID = ID;
    end

    function setValue(obj,value)
      obj.value = value;
    end

    function setLastElement(obj,element)
      lastElement = obj.getLastElement;
      lastElement.next = element;
      element.previous = lastElement;
    end

    function output = getElement(obj,IndexOrID)
      if ischar(IndexOrID)
        output = obj.getElementWithID(IndexOrID);
      elseif isnumeric(IndexOrID)
        output = obj.getElementWithIndex(IndexOrID);
      end
    end

    function output = getElementWithID(obj,ID)
      index = obj.find(ID);
      assert(not(isempty(index)),'There is no element with this ID.')
      index = index(1);
      output = obj.getElementWithIndex(index);
    end

    function output = getElementWithIndex(obj,index)
      assert((index >= 0),'The given element index should be greater or equal to 0.')
      element = obj.getFirstElement;
      for i = 2:index
        assert(not(isempty(element.next)),'Index exceeds the number of list elements.')
        element = element.next;
      end
      output = element;
    end

    function removeElement(obj,element)
      if isequal(obj,element)
        return
      end
      if not(isempty(element.next))
        element.next.previous = element.previous;
      end
      element.previous.next = element.next;
    end

    function output = getLastElement(obj)
      element = obj;
      while not(isempty(element.next))
        element = element.next;
      end
      output = element;
    end

    function output = getFirstElement(obj)
      element = obj;
      while not(isempty(element.previous))
        element = element.previous;
      end
      output = element;
    end

  end



end
