function output = plotSurface(obj,color,edgeColor)
    output = [];
    if not(isempty(obj.surface))
        points = obj.surface.points;
        faces  = obj.surface.faces;
        x = points(:,1);
        y = points(:,2);
        z = points(:,3);

        plotHandle = trisurf(faces,x,y,z,...
          'Facecolor',color,...
          'FaceAlpha',0.1,...
          'EdgeColor',edgeColor,...
          'EdgeAlpha',0.1,...
          'FaceLighting','gouraud');

        output = plotHandle;
    end
end