function setCoordinateSystemWithLandmarks(obj)
    % Calculate the (EPFL) scapular coordinate system
    % The EPFL scapular coordinate system is defined for a right scapula see
    % paper (10.1302/0301-620X.96B4.32641). The X axis is
    % antero-posterior, the Y axis is infero-superior, the Z axis
    % is meddio-lateral. This system is right-handed.
    % This orientation corresponds approximatively to the ISB
    % recommendadtion (doi:10.1016/j.jbiomech.2004.05.042).
    % For left scapula we keep the x axis as postero-anterior and
    % get a left-handed coordinate system.

    if (length(obj.groove) < 2)
    error(['Can''t set scapular coordinate system with actual',...
            ' obj.groove']);
    return;
    end

    % The four following methods have to be called in this specific order
    setPAAxis(obj);
    setMLAxis(obj);
    setISAxis(obj);
    setOrigin(obj);
end

function setPAAxis(obj)
    % Set the postero-anterior axis to be the scapula plane normal
    obj.coordSys.PA = obj.plane.normal;
end

function setMLAxis(obj)
    % Set the media-lateral axis to be the line fitted to the projection of
    % the groove points on the scapula plane
    lateral     = obj.spinoGlenoidNotch;
    medial      = obj.trigonumSpinae;
    groovePointsProjection = obj.plane.projectOnPlane(obj.groove);
    grooveAxis  = fitLine(groovePointsProjection);

    grooveAxis  = (grooveAxis/norm(grooveAxis))';
    obj.coordSys.ML   = orientVectorToward(grooveAxis,(lateral-medial));
end

function setISAxis(obj)
    % Set the infero-superior axis to be orthogonal with the two other axes
    superior    = obj.spinoGlenoidNotch;
    inferior    = obj.angulusInferior;
    obj.coordSys.IS   = cross(obj.coordSys.PA,obj.coordSys.ML);
    obj.coordSys.IS   = orientVectorToward(obj.coordSys.IS,(superior-inferior));
end

function setOrigin(obj)
    % Set the origin of the coordinate system to the spino-glenoid notch
    % projected on the scapular axis
    spinoGlenoid         = obj.spinoGlenoidNotch;
    grooveMeanProjection = mean(obj.plane.projectOnPlane(obj.groove));

    obj.coordSys.origin = grooveMeanProjection + ...
                dot((spinoGlenoid - grooveMeanProjection),obj.coordSys.ML)*obj.coordSys.ML;
end
