function output = plotLandmarksWireframe(obj,color,faceColor)
    % get landmarks
    AI = obj.angulusInferior;
    TS = obj.trigonumSpinae;
    PC = obj.processusCoracoideus;
    AC = obj.acromioClavicular;
    AA = obj.angulusAcromialis;
    SG = obj.spinoGlenoidNotch;
    if not(isempty(obj.groove))
        groove = obj.groove;
    end

    % plot wireframe
    acromion = [SG; AA; AC; AA; SG];
    clavicle = [SG; PC; SG];
    planarScapula = [SG; groove; TS; AI; SG];

    wireframe = [planarScapula; acromion; clavicle];

    plotHandle = plot3(wireframe(:,1),wireframe(:,2),wireframe(:,3),...
      '-o',...
      'Color',color,...
      'MarkerSize',10,...
      'MarkerFaceColor',faceColor);

  output = plotHandle;
end