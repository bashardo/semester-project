classdef (Abstract) Scapula < handle
    %   SCAPULA Summary of this class goes here
    %   This class defines the scapula. Landmarks are used to define its
    %   coordinate system. It includes the glenoid object.

    properties
      angulusInferior % Landmark Angulus Inferior read from amira
      angulusInferiorLocal % Same as above, expressed the scapular coordinate system rather than the CT coordinate system
      trigonumSpinae % Landmark Trigonum Spinae Scapulae (the midpoint of the triangular surface on the medial border of the scapula in line with the scaoular spine read from amira
      processusCoracoideus % Landmark Processus Coracoideus (most lateral point) read from amira
      acromioClavicular % Landmark Acromio-clavicular joint (most lateral part on acromion)read from amira
      angulusAcromialis % Landmark Angulus Acromialis (most laterodorsal point) read from amira
      spinoGlenoidNotch % Landmark Spino-glenoid notch read from amira
      pillar % 5 landmarks on the pillar
      groove % 5 landmarks on the scapula (supraspinatus) groove
      
      a
      coordSys % Scapular coordinate system
      oldCoordSysDeviate 
      newCoordSys %Point Base coodinate system
      newCoordSysDeviate
      plane % Plane class
      InferoSuperior
      AntoriorSuperior
      PosteriorSuperior
      segmentation % either none 'N', manual 'M' or automatic 'A'
      surface % surface points and triangles of the scapula
      glenoid % Glenoid class
      acromion % Acromion class
      comment % any comment
    end



    properties (Hidden = true)
      shoulder
    end



    methods (Abstract)
      load(obj);
      createGlenoid(obj);
    end



    methods (Access = protected)

      function obj = Scapula(shoulder)
        %SCAPULA Construct an instance of this class
        %   Constructor of the scapula object, sets all properties to
        %   zero.
        obj.shoulder = shoulder;

        obj.angulusInferior       = [];
        obj.angulusInferiorLocal  = [];
        obj.trigonumSpinae        = [];
        obj.processusCoracoideus  = [];
        obj.acromioClavicular     = [];
        obj.angulusAcromialis     = [];
        obj.spinoGlenoidNotch     = [];
        obj.pillar                = [];
        obj.groove                = [];
        InferoSuperior            = Box();
        AntoriorSuperior          = Box();
        PosteriorSuperior         = Box();

        obj.coordSys              = CoordinateSystemScapula();
        obj.oldCoordSysDeviate    = CoordinateSystemScapula();
        obj.newCoordSys           = PointBaseCoodinateSystem();
        obj.newCoordSysDeviate    = PointBaseCoodinateSystem();
        
        obj.plane                 = Plane();
        obj.segmentation          = 'N';

        obj.surface               = [];
        obj.createGlenoid();
        obj.acromion              = Acromion(obj);
        obj.acromion.scapula      = obj;

        obj.comment = '';
      end

    end



  methods
      
      
   

    function output = coordSysSet(obj)
      try
        obj.setCoordinateSystemWithLandmarks();
      catch ME 
        warning(ME.message);
        output = 0;
      end

      % To enable retro compatibility with scapula.coordSysSet former
      % implementation, the two following commands are called here
      obj.setShoulderSideWithCoordinateSystem;
      obj.angulusInferiorLocal = obj.coordSys.express(obj.angulusInferior);
      output = 1;
    end

    function measurePlane(obj)
      % Scapular plane is fitted on 3 points (angulusInferior,
      % trigonumSpinae, most laretal scapular goove landmark).
      inferior = obj.angulusInferior;
      medial = obj.trigonumSpinae;
      mostLateralGrooveIndex = findLongest3DVector(medial-obj.groove);
      mostLateralGroovePoint = obj.groove(mostLateralGrooveIndex(1),:);

      obj.plane.fit([inferior; medial; mostLateralGroovePoint]);

      anterior = obj.processusCoracoideus;
      posterior = obj.angulusAcromialis;
      obj.plane.normal = orientVectorToward(obj.plane.normal,(anterior-posterior));
    end
    
    function creatBox(obj)
       obj.InferoSuperior = Box();
       obj.AntoriorSuperior = Box();
       obj.PosteriorSuperior = Box(); 
        
    end
    
    function crearNewCoordSys(obj)
     obj.newCoordSys= PointBaseCoodinateSystem();
        
    end
    
    function planeGen(obj)
     % obj.setCoordinateSystemWithLandmarks();
      obj.creatBox;
      Origin = obj.coordSys.express(obj.coordSys.origin);
      AI = obj.coordSys.express(obj.angulusInferior);
      AG = obj.coordSys.express(obj.spinoGlenoidNotch);
      TS = obj.coordSys.express(obj.trigonumSpinae);
      PC = obj.coordSys.express(obj.processusCoracoideus);
      AA = obj.coordSys.express(obj.angulusAcromialis);
      AC = obj.coordSys.express(obj.acromioClavicular);
      points = obj.coordSys.express(obj.surface.points);
      
      minXYZ1=[TS(1,1)-10,TS(1,2)-80,TS(1,3)];
      maxXYZ1=[(TS(1,1)+10),TS(1,2),AG(1,3)];
      minXYZ2=[Origin(1,1),Origin(1,2),TS(1,3)];
      maxXYZ2=[PC(1,1), max(points(:,2)),Origin(1,3)];
      minXYZ3=[((AA(1,1)+AC(1,1))/2)-10,Origin(1,2),TS(1,3)];
      maxXYZ3=[((AA(1,1)+AC(1,1))/2)+10,max(points(:,2)),Origin(1,3)];
      
      obj.InferoSuperior.plane.fit(transformMatrix(obj,...
          (obj.InferoSuperior.getBoxPoints(points,minXYZ1,maxXYZ1))));
      obj.AntoriorSuperior.plane.fit(transformMatrix(obj,...
          (obj.AntoriorSuperior.getBoxPoints(points,minXYZ2,maxXYZ2))));
      obj.PosteriorSuperior.plane.fit(transformMatrix(obj,...
      (obj.PosteriorSuperior.getBoxPoints(points,minXYZ3,maxXYZ3))));

    end
    
    function [newCoordSysPoints]=transformMatrix(obj,oldPoints)    
        rotation = inv([obj.coordSys.xAxis',obj.coordSys.yAxis',obj.coordSys.zAxis'])*eye(3,3);
        transition = obj.coordSys.origin;
        newCoordSysPoints = oldPoints*rotation+transition.*ones(length(oldPoints),3);

    end
    

    
    

    
    
    function setShoulderSideWithCoordinateSystem(obj)
      % The scapula coordinate system is right-handed for the right shoulder only
      if obj.coordSys.isRightHanded
        obj.shoulder.side = 'R';
        return
      elseif obj.coordSys.isLeftHanded
        obj.shoulder.side = 'L';
        return
      end
      warning(['Couldn''t find the shoulder side by evaluating the scapula',...
               'coordinate system.']);
    end
    function setCoordinateSystemWithLandmarksDeviate(obj)
    % Calculate the (EPFL) scapular coordinate system
    % The EPFL scapular coordinate system is defined for a right scapula see
    % paper (10.1302/0301-620X.96B4.32641). The X axis is
    % antero-posterior, the Y axis is infero-superior, the Z axis
    % is meddio-lateral. This system is right-handed.
    % This orientation corresponds approximatively to the ISB
    % recommendadtion (doi:10.1016/j.jbiomech.2004.05.042).
    % For left scapula we keep the x axis as postero-anterior and
    % get a left-handed coordinate system.

    if (length(obj.groove) < 2)
    error(['Can''t set scapular coordinate system with actual',...
            ' obj.groove']);
    return;
    end

    % The four following methods have to be called in this specific order
    setPAAxis1(obj);
    setMLAxis1(obj);
    setISAxis1(obj);
    setOrigin1(obj);
    end

    function setPAAxis1(obj)
    % Set the postero-anterior axis to be the scapula plane normal
    obj.oldCoordSysDeviate.PA = obj.plane.normal;
    end

    function setMLAxis1(obj)
    % Set the media-lateral axis to be the line fitted to the projection of
    % the groove points on the scapula plane
    lateral     = obj.spinoGlenoidNotch;
    medial      = obj.trigonumSpinae;
    groovePointsProjection = obj.plane.projectOnPlane(obj.groove);
    grooveAxis  = fitLine(groovePointsProjection);

    grooveAxis  = (grooveAxis/norm(grooveAxis))';
     obj.oldCoordSysDeviate.ML   = orientVectorToward(grooveAxis,(lateral-medial));
    end

    function setISAxis1(obj)
    % Set the infero-superior axis to be orthogonal with the two other axes
    superior    = obj.spinoGlenoidNotch;
    inferior    = obj.angulusInferior;
    obj.oldCoordSysDeviate.IS   = cross( obj.oldCoordSysDeviate.PA, obj.oldCoordSysDeviate.ML);
    obj.oldCoordSysDeviate.IS   = orientVectorToward( obj.oldCoordSysDeviate.IS,(superior-inferior));
    end

    function setOrigin1(obj)
    % Set the origin of the coordinate system to the spino-glenoid notch
    % projected on the scapular axis
    spinoGlenoid         = obj.spinoGlenoidNotch;
    grooveMeanProjection = mean(obj.plane.projectOnPlane(obj.groove));

    obj.oldCoordSysDeviate.origin = grooveMeanProjection + ...
                dot((spinoGlenoid - grooveMeanProjection), obj.oldCoordSysDeviate.ML)* obj.oldCoordSysDeviate.ML;
end

    
  end
  
end
