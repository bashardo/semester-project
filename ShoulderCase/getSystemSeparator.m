function separator = getSystemSeparator
  if isunix
    separator = '/';
  else
    separator = '\';
  end
end
