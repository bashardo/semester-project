function output = cellArraysDifference(evaluatedArray,substractedArray,varargin)
  % Remove all occurence of substracted arrays' elements from the evaluated array.
  % Any number of substracted arrays can be given in arguments with at least done
  % substracted array.

  assert(iscell(evaluatedArray),'Arguments must be cell arrays.')
  assert(iscell(substractedArray),'Arguments must be cell arrays.')

  output = evaluatedArray(not(contains(evaluatedArray,substractedArray)));

  if (nargin > 2)
    output = cellArraysDifference(output,varargin{1},varargin{2:end});
  end
end
