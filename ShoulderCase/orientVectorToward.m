function vector = orientVectorToward(vector,orientation)
  if dot(vector,orientation) < 0
    vector = -vector;
  end
end
