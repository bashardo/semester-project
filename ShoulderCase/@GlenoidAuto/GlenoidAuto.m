classdef GlenoidAuto < Glenoid
% To be used with ShoulderAuto data.
% The load() method requires a specific implementation.

  methods

    function outputArg = load(obj)
      % LOADAUTO Load genoid surface from auto segmentation
      %   Load the points of the glenoid surface from matlab dir

      SCase = obj.scapula.shoulder.SCase;
      SCaseId4C = SCase.id4C;

      matlabDir = SCase.dataMatlabPath;
      side = SCase.shoulderAuto.side;

      % Import glenoid surface points
      fileName = ['glenoidSurfaceAuto' side '.ply'];
      fileName = [matlabDir '/' fileName];
      if exist(fileName,'file') == 2
        [points,faces]=loadPly(fileName,1);
        obj.surface.points = points;
        obj.surface.meanPoint = mean(points);
        obj.surface.faces = faces;
      else
        outputArg = 0;
        return
        % error(['Could not find file: ' fileName])
      end

      outputArg = 1; % Should report on loading result
    end

  end



end
