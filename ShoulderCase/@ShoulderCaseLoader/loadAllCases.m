function output = loadAllCases(obj)
  allCases = {};
  
  obj.findAllCases();
  allCasesID = obj.shoulderCasePath.keys();
  totalNumberOfCases = single(obj.getNumberOfFoundCases);

  progression = UpdatableText('',' All cases are being loaded.');
  for i = 1:totalNumberOfCases
    progression.printPercent(i/totalNumberOfCases);
    progression.printProgressBar(i/totalNumberOfCases);
    try
      allCases{end+1} = obj.loadCase(allCasesID{i});
    end
  end

  output = allCases;
end