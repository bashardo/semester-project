function output = findCase(obj,SCaseID)
  % Case already found
  if obj.shoulderCasePath.isKey(SCaseID)
      output = true;
      return
  end

  output = false;

  % Format ID to construct the case's path
  givenID = SCaseIDParser(SCaseID);
  assert(givenID.isValidID,'%s is not a valid ID.',SCaseID);
  SCaseIDmaxDigits = givenID.getIDWithNumberOfDigits(ConfigFileExtractor.getVariable('maxSCaseIDDigits'));

  SCasePath = fullfile(obj.dataRootPath,...
    SCaseIDmaxDigits(1),... % SCaseID type
    SCaseIDmaxDigits(2),... % SCaseID hundreds
    SCaseIDmaxDigits(3),... % SCaseID tens
    [SCaseID '-*']); % SCaseID folder whatever the IPP is

  SCaseFound = dir(SCasePath);
  if isempty(SCaseFound)
      return
  end

  obj.shoulderCasePath(SCaseID) = fullfile(SCaseFound.folder,SCaseFound.name);

  output = true;
end