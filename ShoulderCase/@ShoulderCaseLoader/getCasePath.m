function output = getCasePath(obj,SCaseID)
  assert(obj.findCase(SCaseID),'%s not found in the database.',SCaseID);
  output = obj.shoulderCasePath(SCaseID);
end