function output = getAllPathologicalCasesID(obj)
  obj.findAllCases();
  casesID = obj.shoulderCasePath.keys();
  output = casesID(startsWith(casesID,'P'));
end