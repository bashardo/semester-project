function setDataRootPath(obj,dataRootPath)
  assert(isfolder(dataRootPath),'%s is not a valid path.',dataRootPath);
  
  % Do not reset obj.shoulderCasePath if dataRootPath doesn't change
  if isequal(obj.dataRootPath,dataRootPath)
    return
  end
  
  obj.dataRootPath = dataRootPath;
  obj.shoulderCasePath = containers.Map;
  obj.allCasesFound = false;
end
