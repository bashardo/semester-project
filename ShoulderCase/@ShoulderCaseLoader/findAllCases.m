function findAllCases(obj)
  if obj.allCasesFound
    return
  end

  types = ConfigFileExtractor.getVariable('SCaseIDValidTypes');
  maxSCaseNumber = 10^ConfigFileExtractor.getVariable('maxSCaseIDDigits')-1;
  
  progression = UpdatableText('',' All valid cases are being looked for.');
  for i = 1:length(types)
    for j = 1:maxSCaseNumber
      progression.printPercent(getProgressionFraction(i,j,length(types),maxSCaseNumber));
      progression.printProgressBar(getProgressionFraction(i,j,length(types),maxSCaseNumber));
      SCaseID = [types(i) int2str(j)];
      try
        obj.findCase(SCaseID);
      end
    end
  end

  obj.allCasesFound = true;
end



function output = getProgressionFraction(i,j,numberOfTypes,maxSCaseNumber)
  currentNumber = (i-1)*(maxSCaseNumber+1)+j;
  maxNumber = numberOfTypes*(maxSCaseNumber+1)-1;
  output = currentNumber/maxNumber;
end