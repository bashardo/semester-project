function output = loadCase(obj,SCaseID)
  assert(obj.findCase(SCaseID),'%s not found in the database.',SCaseID);
  
  % Find SCase.mat files
  SCaseFilePath = dir(fullfile(obj.shoulderCasePath(SCaseID),'CT-*','matlab','SCase.mat'));
  assert(not(isempty(SCaseFilePath)),'No archive found for %s',SCaseID);

  % Load a verified ShoulderCase instance
  loaded = load(fullfile(SCaseFilePath(1).folder,SCaseFilePath(1).name));
  assert(isfield(loaded,'SCase'),'No SCase field found in the archive');  
  SCase = loaded.SCase;
  assert(isequal(class(SCase),'ShoulderCase'),'The found SCase is not a ShoulderCase instance');
  
  % Update data paths
  SCase.dataCTPath = extractBefore(SCaseFilePath(1).folder,'matlab');
  try
    SCase.propagateDataPath;
  catch ME 
    warning('Impossible to propagate dataPath. The object version might be deprecated.');
    warning(ME.message);
  end
  
  output = SCase;
end