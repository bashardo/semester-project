function output = createEmptyCase(obj,SCaseID)
  obj.findCase(SCaseID);
  SCaseDataCTPath = '';

  SCaseCTFolders = dir(fullfile(obj.shoulderCasePath(SCaseID),'CT-*'));
  assert(not(isempty(SCaseCTFolders)),'No CT folder found for %s',SCaseID);

  % Priority is given to folder containing a matlab archive
  for i = 1:length(SCaseCTFolders)
    if isfile(fullfile(SCaseCTFolders(i).folder,SCaseCTFolders(i).name,'matlab','SCase.mat'))
      SCaseDataCTPath = fullfile(SCaseCTFolders(i).folder,SCaseCTFolders(i).name);
    end
  end
  
  % Then, priority is given to folder containing a 'amira' folder
  if isempty(SCaseDataCTPath)
    for i = 1:length(SCaseCTFolders)
      if isfolder(fullfile(SCaseCTFolders(i).folder,SCaseCTFolders(i).name,'amira'))
        SCaseDataCTPath = fullfile(SCaseCTFolders(i).folder,SCaseCTFolders(i).name);
      end
    end
  end
  
  % Then, priority is given to CT folder with lowest ending number
  if isempty(SCaseDataCTPath)
    SCaseDataCTPath = fullfile(SCaseCTFolders(1).folder,SCaseCTFolders(1).name);
  end

  output = ShoulderCase(SCaseID,SCaseDataCTPath);
end