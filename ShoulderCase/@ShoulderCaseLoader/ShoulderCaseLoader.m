classdef ShoulderCaseLoader < handle
  % Class to interact with the database.
  %
  % The main purpose of the ShoulderCaseLoader is to
  % easily load the cases. By initialisation, a
  % ShoulderCaseLoader instance will set its dataRootPath
  % property to the dataDir variable found in the 
  % config.txt file.
  %
  % Because of cross-systems utilisation of the database
  % the paths stored in the ShoulderCase instances may 
  % vary from one execution to the other. 
  % Thus, this class evolved to be the access point to
  % the ShoulderCase constructor, to avoid misusing
  % the paths. This was also the incentive to create the
  % ShoulderCase.propagateDataPath() method.
  %
  % example:
  %   database = ShoulderCaseLoader() 
  %   SCase = database.loadCase('P500');

  properties (Access = private)
    shoulderCasePath = containers.Map;
    dataRootPath = '';
    allCasesFound = false;
  end



  methods 

    function obj = ShoulderCaseLoader()
      try
        obj.setDataRootPath(ConfigFileExtractor.getVariable('dataDir'));
      end
    end

  end



  methods (Access = private)

    caseHasBeenFound = findCase(obj,SCaseID);
    findAllCases(obj);

  end

end