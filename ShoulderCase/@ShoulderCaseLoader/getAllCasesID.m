function output = getAllCasesID(obj)
  obj.findAllCases();
  casesID = obj.shoulderCasePath.keys();
  output = casesID;
end