function output = getNumberOfFoundCases(obj)
  output = obj.shoulderCasePath.Count();
end