function output = getAllNormalCasesID(obj)
  obj.findAllCases();
  casesID = obj.shoulderCasePath.keys();
  output = casesID(startsWith(casesID,'N'));
end