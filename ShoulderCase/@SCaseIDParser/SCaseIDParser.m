classdef SCaseIDParser < handle
% Used to validate shoulder case ID.
% Check the shoulder case type (first character of the ID).
% Check the shoulder case number (last characters)
%
% Can give the shoulder case ID with filled digits.
% This has been implemented to give ShoulderCase.id4c.

  properties (Access = private)
    maxDigits
    SCaseValidTypes
    rawID
  end



  methods

    function obj = SCaseIDParser(rawID)
      obj.rawID = rawID;
      assert(ischar(rawID),'Input argument must be a char array.');

      obj.maxDigits = ConfigFileExtractor.getVariable('maxSCaseIDDigits');
      assert(not(isempty(obj.maxDigits)),'maxDigits property is empty');

      obj.SCaseValidTypes = ConfigFileExtractor.getVariable('SCaseIDValidTypes');
      assert(not(isempty(obj.SCaseValidTypes)),'SCaseValidTypes property is empty');
    end

    function output = isValidID(obj)
      types = obj.SCaseValidTypes;
      maxDigits = obj.maxDigits;
      output = obj.SCaseIDHasTypesAndMaxDigits(types,maxDigits);
    end

    function output = isNormalCase(obj)
      types = 'N';
      maxDigits = obj.maxDigits;
      output = obj.SCaseIDHasTypesAndMaxDigits(types,maxDigits);
    end

    function output = isPathologicalCase(obj)
      types = 'P';
      maxDigits = obj.maxDigits;
      output = obj.SCaseIDHasTypesAndMaxDigits(types,maxDigits);
    end

    function output = getID(obj)
      output = obj.rawID;
    end

    function output = getIDWithNumberOfDigits(obj,size)
      assert(obj.isValidID,'%s is not a valid ID.',obj.getID);
      type = obj.getCaseType;
      number = obj.getCaseNumber;
      fillingZeros = repmat('0',1,size-length(number));
      output = [type fillingZeros number];
    end

  end


  methods (Access = private)

    function output = SCaseIDHasTypesAndMaxDigits(obj,types,maxDigits)
      expression = ['^[' types ']\d{1,' num2str(maxDigits) '}$'];
      output = obj.textMatchesExpression(obj.rawID,expression);
    end

    function output = getCaseType(obj)
      output = obj.rawID(1);
    end

    function output = getCaseNumber(obj)
      output = obj.rawID(2:end);
    end

  end


end
