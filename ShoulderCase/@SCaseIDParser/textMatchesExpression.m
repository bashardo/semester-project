function output = textMatchesExpression(obj,text,expression)
  matchingResult = regexp(text,expression,'match','once');

  if isempty(matchingResult)
    output = false;
  else
    output = true;
  end
end
