function shoulderToSQL(shoulder, conn)
%SHOULDERTOSQL fill shoulder structure array in mySQL
%   Detailed explanation goes here

fprintf('\nshoulder to mySQL... ');

colnames = {'shoulder_id','patient_id','side','comment'};

% Loop on patient structure array
for i=1:numel(shoulder)
    shoulder_id = shoulder(i).shoulder_id;
    patient_id  = shoulder(i).patient_id;
    side        = shoulder(i).side;
    comment = shoulder(i).comment;
    
    exdata   = {shoulder_id,patient_id,side,comment};
    % insert data in SQL
    datainsert(conn,'shoulder',colnames,exdata);
end

fprintf('Done\n');

end

