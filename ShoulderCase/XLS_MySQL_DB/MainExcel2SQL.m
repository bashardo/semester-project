%% MainExcel2SQL
%
% This script reads the shoulder database Excel file and 
% write in the mySQL database. The existing tables are overwritten and 
% auto-counter are reset.

% Author: Alexandre Terrier, EPFL-LBO
% Date: 2016-09-29
%

%%
%  Initialization

clearvars; % Clear all matlab variables

%%
%**************************************************************************
%
%  Read data from Excel
%

excelRaw = rawFromExcel();

%%
%  Read from Excel: pathology array

diagnosisList = diagnosisListFromExcel(excelRaw);

%%
%  Read from Excel: treatement array 

treatmentList = treatmentListFromExcel(excelRaw);

%%
%  Read from Excel: patient structure array

patient = patientFromExcel(excelRaw);

%%
%  Read from Excel: shoulder structure array

shoulder = shoulderFromExcel(patient, excelRaw);

%%
%  Read from Excel: sCase struture array, and associated

[sCase, diagnosis, treatment, outcome, study] = sCaseFromExcel(...
    shoulder, patient, diagnosisList, treatmentList, excelRaw);

%%
%  Read from Excel: ct structure array, nd associated

[CT, scapula, glenoid, glenoid_density, humerus, muscle] = CTfromExcel(...
    shoulder, patient, excelRaw);

%% 
%**************************************************************************
%
%  Write data to mySQL database

%%
%  open mySQL connection

conn = openSQL();

%%
%  Initialize tables of mySQL database

initializeSQL(conn);

%%
%  Write in mySQL: patient table

patientToSQL(patient, conn);

%%
%  Write in mySQL: anonymity table

anonymityToSQL(patient, conn);

%%
%  Write in mySQL: shoulder table

shoulderToSQL(shoulder, conn);

%%
%  Write in mySQL: sCase

sCaseToSQL(sCase, conn);

%%
%  Write in mySQL: CT

CTtoSQL(CT, conn);

%%
%  Write in mySQL: glenoid 

scapulaToSQL(scapula, conn);

%%
%  Write in mySQL: glenoid 

glenoidToSQL(glenoid, conn);

%%
% Write in mySQL: diagnosisList

diagnosisListToSQL(diagnosisList, conn);

%%
% Write in mySQL: diagnosis

diagnosisToSQL(diagnosis, conn);

%%
% Write in mySQL: treatmentList

treatmentListToSQL(treatmentList, conn);

%%
% Write in mySQL: treatment

treatmentToSQL(treatment, conn);

%%
% Write in mySQL: outcome

outcomeToSQL(outcome, conn);

%% 
%  Close mySQL database

closeSQL(conn);
