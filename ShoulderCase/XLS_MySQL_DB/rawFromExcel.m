function [ rawExcel ] = rawFromExcel(filename)
%RAWFROMEXCERL read the Excel file filename and output content in raw
%   Set the sheet and the range
%   Returns the raw data

fprintf('\nGet rawExcel from Excel... ');

% Set Excel file name & directory
% directory = '../../../../data/Excel/';
% filename  = 'ShoulderDataBase.xlsx';filename  = strcat(directory,filename);
sheet     = 'SCase'; % 'Normal' & 'TSA'
xlRange   = 'A1:CX1999'; % range of column and row to be imported

[~,~,rawExcel] = xlsread(filename,sheet,xlRange,'basic');

% Get column of variables
header = rawExcel(1,:);
SCase_id_col = find(strcmp([header],'SCase_ID'));

% Delete rows without SCase
row_idx = 2;
[rowN, ~] = size(rawExcel);
while row_idx <= rowN
    row = rawExcel(row_idx, :); % get the entire row
    SCase_id = row{SCase_id_col};
    if isnan(SCase_id)
        rawExcel(row_idx, :) = [];
        rowN = rowN - 1;
    else
        row_idx = row_idx + 1;
    end
end

fprintf('Done\n');

end
