function [ treatmentList ] = treatmentListFromExcel( rawExcel )
%TREATEMENT built the tratement structure array that identify the treatments 
%   Detailed explanation goes here

fprintf('\nGet treatmentList from Excel... ');
    
% define treatment structure array
treatmentList = struct(...
    'treatmentList_id',[], ...
    'name'            ,[], ...
    'description'     ,[] ...
    );

% define undefined treatment
treatmentList(1).treatmentList_id = 1;
treatmentList(1).name = 'undefined';
treatmentList(1).description = 'The treatement is not defined yet';

% define no treatment
treatmentList(2).treatmentList_id = 2;
treatmentList(2).name = 'none';
treatmentList(2).description = 'No planed treatment, for healthy shoulders';

% get column of variables
header = rawExcel(1,:);
treatmentName_col = find(strcmp([header],'treatment.name'));

% loop over rows of Excel table
[rowN, ~] = size(rawExcel);
treatmentList_id = 2;
for row_idx = 2:rowN
    row = rawExcel(row_idx, :); % get the entire row
    treatmentName = row(treatmentName_col);
    treatmentName = treatmentName{1};
    if ~isempty(treatmentName) & ~isnan(treatmentName)
        sameTreatment = find(strcmp({treatmentList.name}, treatmentName)==1);
        if isempty(sameTreatment)
            treatmentList_id = treatmentList_id + 1;
            treatmentList_idx = treatmentList_id;
            treatmentList(treatmentList_idx).treatmentList_id = treatmentList_id;
            treatmentList(treatmentList_idx).name             = treatmentName;
            treatmentList(treatmentList_idx).description      = '';
        end
    end
end

fprintf('Done\n');

end

