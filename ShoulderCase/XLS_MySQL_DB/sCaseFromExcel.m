function [ sCase, diagnosis, treatment, outcome, study ] = sCaseFromExcel(shoulder, patient, diagnosisList, treatmentList, rawExcel)
% PCASEFROMEXCEL build the sCase (shoulder case) structure array from Excel and from
% patient and shoulder structure. A sCase is associated to a shoulder. The
% same shoulder can be associated to more than one sCase.
%   pCase stand for patient case. The variable case could not be used since
%   alrewad usd by Matlab

fprintf('\nGet sCase from Excel... ');

% define sCase structure array
sCase = struct(...
    'sCase_id'      ,[],...
    'shoulder_id'   ,[],...
    'folder_name'   ,[],...
    'comment'   ,[] ...
    );

% defne diagnosis structure
diagnosis = struct(...
    'diagnosis_id'    ,[],...
    'sCase_id'        ,[],...
    'diagnosisList_id',[],...
    'date'            ,[],...
    'comment'         ,[]...
    );

% defne treatment structure
treatment = struct(...
    'treatment_id'    ,[],...
    'sCase_id'        ,[],...
    'treatmentList_id',[],...
    'date'            ,[],...
    'comment'         ,[]...
    );

% defne outcome structure
outcome = struct(...
    'outcome_id'    ,[],...
    'sCase_id'        ,[],...
    'date'            ,[],...
    'comment'         ,[],...
    'loosening'         ,[]...
    );

% define study structure
study = struct(...
    'study_id' ,[],...
    'sCase_id' ,[],...
    'name'     ,[],...
    'comment'  ,[]...
    );

% get column of variables
header = rawExcel(1,:);
sCase_id_col      = find(strcmp([header],'sCase.id'));
sCaseComm_col      = find(strcmp([header],'sCase.comment'));
diagnosisName_col = find(strcmp([header],'diagnosis.name'));
diagnosisDate_col = find(strcmp([header],'diagnosis.date'));
diagnosisComm_col = find(strcmp([header],'diagnosis.comment'));
treatmentName_col = find(strcmp([header],'treatment.name'));
treatmentDate_col = find(strcmp([header],'treatment.date'));
treatmentComm_col = find(strcmp([header],'treatment.comment'));
outcomeDate_col   = find(strcmp([header],'outcome.date'));
outcomeComm_col   = find(strcmp([header],'outcome.comment'));
outcomeLoos_col   = find(strcmp([header],'outcome.loosening'));
IPP_col           = find(strcmp([header],'anonymity.IPP'));
side_col          = find(strcmp([header],'shoulder.side'));

% loop over rows of Excel table
[rowN, ~] = size(rawExcel);
sCase_id    = 0;
diagnosis_id = 0;
treatment_id = 0;
outcome_id   = 0;
for row_idx = 2:rowN
    row = rawExcel(row_idx, :); % get the entire row
    sCaseE  = row{sCase_id_col}; % sCase id from Excel -> folder_name
    side = row{side_col};
    if ~isnan(sCaseE) & ~isnan(side) % check that sCase & side are defined
        IPP = row{IPP_col};
        patient_idx = find([patient.IPP] == IPP);
        % get patient index of this caseExcel
        if ~isempty(patient_idx)
            % there is a patient with same IPP
            patient_idx  = patient(patient_idx).patient_id; % get patient_id
            shoulder_idx = find([shoulder.patient_id] == patient_idx); % get shoulder_id
            % should be 1 or 2 (R/L), but not 0 (test it however)
            shoulder_idxN = length(shoulder_idx);
            switch shoulder_idxN
                case 0
                    % this sCase patient is not in the shoulder array
                    % report an error
                    fprintf('\nsCase %s not in shoulder array', sCaseE);
                    addCase = 0;
                case 1
                    % this sCase patient has 1 match in the shoulder array
                    % so we have to check side
                    if side == shoulder(shoulder_idx).side
                        shoulder_id = shoulder(shoulder_idx).shoulder_id;
                        addCase = 1;
                    else
                        % not same side so report a problem
                        fprintf('\nsCase in soulder array but not same side');
                    end
                case 2
                    % both shoulder sides of this patient are in the
                    % shoulder array, so we have to check side
                    if side == shoulder(shoulder_idx(1)).side
                        shoulder_id = shoulder(shoulder_idx(1)).shoulder_id;
                        addCase = 1;
                    else
                        if side == shoulder(shoulder_idx(2)).side
                            shoulder_id = shoulder(shoulder_idx(2)).shoulder_id;
                            addCase = 1;
                        end
                    end
            end
            if addCase
                % add sCase
                sCase_id  = sCase_id + 1;
                sCase_idx = sCase_id;
                folder_name = sCaseE;
                % get sCase comment
                sCase_comment = row{sCaseComm_col};
                if isnan(sCase_comment)
                    sCase_comment = '';
                end
                
                sCase(sCase_idx).sCase_id       = sCase_id;
                sCase(sCase_idx).shoulder_id    = shoulder_id;
                sCase(sCase_idx).folder_name    = folder_name;
                sCase(sCase_idx).comment    = sCase_comment;
                

                
                % check & add diagnosis
                diagnosisName  = row{diagnosisName_col};
                diagnosisList_idx = find(strcmp({diagnosisList.name}, diagnosisName) == 1);
                if isempty(diagnosisList_idx)
                    fprintf('\nsCase %s has not associated diagnosisList_id', sCaseE);
                else
                    diagnosis_id = diagnosis_id + 1;
                    diagnosis_idx = diagnosis_id;
                    % get diagnosis date
                    diagnosis_date = row{diagnosisDate_col};
                    diagnosis_date = datetime(diagnosis_date,'ConvertFrom','excel');
                    diagnosis_date.Format = 'yyyy-MM-dd';
                    % get diagnosis comment
                    diagnosis_comment = row{diagnosisComm_col};
                    if isnan(diagnosis_comment)
                        diagnosis_comment = '';
                    end
                    
                    diagnosis(diagnosis_idx).diagnosis_id     = diagnosis_id;
                    diagnosis(diagnosis_idx).sCase_id         = sCase_id;
                    diagnosis(diagnosis_idx).diagnosisList_id = diagnosisList(diagnosisList_idx).diagnosisList_id;
                    diagnosis(diagnosis_idx).date             = diagnosis_date;
                    diagnosis(diagnosis_idx).comment          = diagnosis_comment;
                end
                
                % check & add treatment
                treatmentName = row{treatmentName_col};
                treatmentList_idx = find(strcmp({treatmentList.name}, treatmentName) == 1);
                if isempty(treatmentList_idx)
                    fprintf('\nsCase %s has not associated treatmentList_id', sCaseE);
                else
                    treatment_id = treatment_id + 1;
                    treatment_idx = treatment_id;
                    % get treatment date
                    treatment_date = row{treatmentDate_col};
                    treatment_date = datetime(treatment_date,'InputFormat','dd.MM.yyyy','ConvertFrom','excel');
                    treatment_date.Format = 'yyyy-MM-dd';
                    % get treatment comment
                    treatment_comment = row{treatmentComm_col};
                    
                    treatment(treatment_idx).treatment_id     = treatment_id;
                    treatment(treatment_idx).sCase_id         = sCase_id;
                    treatment(treatment_idx).treatmentList_id = treatmentList(treatmentList_idx).treatmentList_id;
                    treatment(treatment_idx).date             = treatment_date;
                    treatment(treatment_idx).comment          = treatment_comment;
                end
                
                % check & add outcome
                % get outcome date
                outcome_date = row{outcomeDate_col};
                if ~isempty(outcome_date)
                    outcome_date = datetime(outcome_date,'InputFormat','dd.MM.yyyy','ConvertFrom','excel');
                    outcome_date.Format = 'yyyy-MM-dd';
                end
                % get outcome comment
                outcome_comment = row{outcomeComm_col};
                % get outcome loosening
                outcome_loosening = row{outcomeLoos_col};
                
                if isempty(outcome_comment) || isempty(outcome_loosening)
                    fprintf('\nsCase %s has not associated outcome', sCaseE);
                else
                    outcome_id = outcome_id + 1;
                    outcome_idx = outcome_id;
                    
                    outcome(outcome_idx).outcome_id     = outcome_id;
                    outcome(outcome_idx).sCase_id       = sCase_id;
                    outcome(outcome_idx).date           = outcome_date;
                    outcome(outcome_idx).comment        = outcome_comment;
                    outcome(outcome_idx).loosening      = outcome_loosening;
                end
                
                % check & add study
                % to be done
                study(1).study_id = 1;
                study(1).sCase_id = 1;
                study(1).name     = 'studyTest';
                study(1).comment  = 'studyComment';
            end
        end
    end
end

fprintf(' Done\n');

end

