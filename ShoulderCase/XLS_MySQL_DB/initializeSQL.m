function [ curs ] = initializeSQL( conn )
%INITIALIZESQL Summary of this function goes here
%   Detailed explanation goes here

fprintf('\nInitialize mySQL database... ');

testquery= 'SET FOREIGN_KEY_CHECKS = 0';
exec(conn,testquery);

% Empty diagnosis table and reset AUTO_INCREMENT
sqlquery = 'TRUNCATE diagnosis';
curs = exec(conn,sqlquery);
if ~isempty(curs.Message)
    fprintf('\nMessage: %s\n', curs.Message);
end

% Empty diagnosisList table and reset AUTO_INCREMENT
sqlquery = 'TRUNCATE diagnosisList';
curs = exec(conn,sqlquery);
if ~isempty(curs.Message)
    fprintf('\nMessage: %s\n', curs.Message);
end

% Empty treatment table and reset AUTO_INCREMENT
sqlquery = 'TRUNCATE treatment';
curs = exec(conn,sqlquery);
if ~isempty(curs.Message)
    fprintf('\nMessage: %s\n', curs.Message);
end

% Empty treatmentList table and reset AUTO_INCREMENT
sqlquery = 'TRUNCATE treatmentList';
curs = exec(conn,sqlquery);
if ~isempty(curs.Message)
    fprintf('\nMessage: %s\n', curs.Message);
end

% Empty outcome table and reset AUTO_INCREMENT
sqlquery = 'TRUNCATE outcome';
curs = exec(conn,sqlquery);
if ~isempty(curs.Message)
    fprintf('\nMessage: %s\n', curs.Message);
end

% Empty study table and reset AUTO_INCREMENT
sqlquery = 'TRUNCATE study';
curs = exec(conn,sqlquery);
if ~isempty(curs.Message)
    fprintf('\nMessage: %s\n', curs.Message);
end

% Empty studyList table and reset AUTO_INCREMENT
sqlquery = 'TRUNCATE studyList';
curs = exec(conn,sqlquery);
if ~isempty(curs.Message)
    fprintf('\nMessage: %s\n', curs.Message);
end

% Empty glenoid table and reset AUTO_INCREMENT
sqlquery = 'TRUNCATE glenoid';
curs = exec(conn,sqlquery);
if ~isempty(curs.Message)
    fprintf('\nMessage: %s\n', curs.Message);
end

% Empty glenoid_density table and reset AUTO_INCREMENT
sqlquery = 'TRUNCATE glenoid_density';
curs = exec(conn,sqlquery);
if ~isempty(curs.Message)
    fprintf('\nMessage: %s\n', curs.Message);
end

% Empty humerus table and reset AUTO_INCREMENT
sqlquery = 'TRUNCATE humerus';
curs = exec(conn,sqlquery);
if ~isempty(curs.Message)
    fprintf('\nMessage: %s\n', curs.Message);
end

% Empty muscle table and reset AUTO_INCREMENT
sqlquery = 'TRUNCATE muscle';
curs = exec(conn,sqlquery);
if ~isempty(curs.Message)
    fprintf('\nMessage: %s\n', curs.Message);
end

% Empty scapula table and reset AUTO_INCREMENT
sqlquery = 'TRUNCATE scapula';
curs = exec(conn,sqlquery);
if ~isempty(curs.Message)
    fprintf('\nMessage: %s\n', curs.Message);
end

% Empty CT table and reset AUTO_INCREMENT
sqlquery = 'TRUNCATE CT';
curs = exec(conn,sqlquery);
if ~isempty(curs.Message)
    fprintf('\nMessage: %s\n', curs.Message);
end

% Empty shoulder table and reset AUTO_INCREMENT
sqlquery = 'TRUNCATE shoulder';
curs = exec(conn,sqlquery);
if ~isempty(curs.Message)
    fprintf('\nMessage: %s\n', curs.Message);
end

% Empty patient table and reset AUTO_INCREMENT
sqlquery = 'TRUNCATE patient';
curs = exec(conn,sqlquery);
if ~isempty(curs.Message)
    fprintf('\nMessage: %s\n', curs.Message);
end

% Empty anonymity table and reset AUTO_INCREMENT
sqlquery = 'TRUNCATE anonymity';
curs = exec(conn,sqlquery);
if ~isempty(curs.Message)
    fprintf('\nMessage: %s\n', curs.Message);
end

% Empty sCase table and reset AUTO_INCREMENT
sqlquery = 'TRUNCATE sCase';
curs = exec(conn,sqlquery);
if ~isempty(curs.Message)
    fprintf('\nMessage: %s\n', curs.Message);
end

testquery= 'SET FOREIGN_KEY_CHECKS = 1';
exec(conn,testquery);

fprintf('Done\n');

end