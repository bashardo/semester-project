function [patient] = patientFromExcel(rawExcel)
%PATIENT builds the structure array patients from
%   Detailed explanation goes here

fprintf('\nGet patient from Excel... ');

% define the patient structure array
patient = struct(...
    'patient_id', [], ...
    'IPP',        [], ... % to be later move to another table for anonymous constrain
    'initials',   [], ... % to be later move to another table for anonymous constrain
    'anonym_birth_date',   [], ... % to be later move to another table for anonymous constrain
    'gender',     [], ...
    'birth_date', [], ...
    'height',     [], ...
    'weight',     [],...  % should we put weight here since it can vary?
    'comment',     []...  
    );

% get column of variables
header = rawExcel(1,:);
sCase_id_col   = find(strcmp([header],'sCase.id'));
IPP_col        = find(strcmp([header],'anonymity.IPP'));
initials_col   = find(strcmp([header],'anonymity.initials'));
anonym_birth_date_col   = find(strcmp([header],'anonymity.birth_date'));
gender_col     = find(strcmp([header],'patient.gender'));
birth_date_col = find(strcmp([header],'patient.birth_date'));
height_col     = find(strcmp([header],'patient.height'));
weight_col     = find(strcmp([header],'patient.weight'));
comment_col    = find(strcmp([header],'patient.comment'));
% loop over rows of Excel table to build patient structure
% patients are not included if caseExcel, IPP, initials, gender, birth_date are not
% defined
[rowN, ~] = size(rawExcel);
patient_id = 0;
for row_idx = 2:rowN
    row = rawExcel(row_idx, :); % get the entire row
    
    % check that there is a sCase for this row
    sCase_id  = row{sCase_id_col}; % sCase_id from Excel
    if ~isempty(sCase_id)
        
        % check validity of data
        IPP        = row{IPP_col};
        initials   = row{initials_col};
        gender        = row{gender_col};
        anonym_birth_date = row{anonym_birth_date_col};
        anonym_birth_date = datetime(anonym_birth_date,'InputFormat','dd.MM.yyyy','ConvertFrom','excel');
        anonym_birth_date.Format = 'yyyy-MM-dd';
        birth_date = row{birth_date_col};
        birth_date = datetime(birth_date,'InputFormat','dd.MM.yyyy','ConvertFrom','excel');
        birth_date.Format = 'yyyy-MM-dd';
        % get muscle comment
        comment = row{comment_col};
        if isnan(comment)
            comment = '';
        end
        
        if ~isnan(IPP) & ...
                ~isnan(initials) & ...
                ~isnan(gender) & ...
                ~isnat(birth_date) & ...
                ~isnat(anonym_birth_date)
            
            % Check that patient in row sCase is not already in structure
            % We asume here that IPP uniquely identifies a patient
            sameIPP = find([patient.IPP] == IPP);
            if ~isempty(sameIPP)
                % IPP is allready in patient array, extra-checks to avoid false same patient
                if birth_date ~= patient(sameIPP).birth_date % not same birthdate
                    fprintf('\nSame IPP (%i) but different birth date in case %s !', IPP, sCase_id);
                end
                if gender ~= patient(sameIPP).gender % not same gender
                    fprintf('\nSame IPP (%i) but different gender in case %s !', IPP, sCase_id);
                end
                if ~strcmp(initials, patient(sameIPP).initials) % not same initials
                    fprintf('\nSame IPP (%i) but different initials in case %s !', IPP, sCase_id);
                end
            else % patient is not in patient structure array, so add it
                patient_id = patient_id + 1;
                patient_idx = patient_id;
                
                height = row{height_col};
                if height == ' ' % to avoid strange behavior of the excel import
                    height = NaN;
                end
                weight = row{weight_col};
                
                patient(patient_idx).patient_id = patient_id;
                patient(patient_idx).IPP        = IPP;
                patient(patient_idx).initials   = initials;
                patient(patient_idx).gender     = gender;
                patient(patient_idx).birth_date = birth_date;
                patient(patient_idx).height     = height;
                patient(patient_idx).weight     = weight;
                patient(patient_idx).anonym_birth_date = anonym_birth_date;
                patient(patient_idx).comment = comment;
            end
        end
    end
end

fprintf('Done\n');

end

