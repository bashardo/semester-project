function [ diagnosisList ] = diagnosisListFromExcel( rawExcel )
%DIAGNOSISLISTFROMEXCEL Summary of this function goes here
%   Detailed explanation goes here

fprintf('\nGet diagnosisList from Excel... ');

% define diagnosis structure array
diagnosisList = struct(...
    'diagnosisList_id' ,[], ...
    'name'             ,[], ...
    'description'      ,[] ...
    );

% set normal subnormaland diagnosis names
diagnosisList(1).diagnosisList_id = 1;
diagnosisList(1).name         = 'normal';
diagnosisList(1).description  = 'fully normal';

diagnosisList(2).diagnosisList_id = 2;
diagnosisList(2).name         = 'subnormal';
diagnosisList(2).description  = 'no OA but small abnormalities';

% get column of variables
header = rawExcel(1,:);
diagnosisName_col = find(strcmp([header],'diagnosis.name'));

% loop over rows of Excel table
[rowN, ~] = size(rawExcel);
diagnosisList_id = 2;
for row_idx = 2:rowN
    row = rawExcel(row_idx, :); % get the entire row
    diagnosisName = row(diagnosisName_col);
    diagnosisName = diagnosisName{1};
    if ~isempty(diagnosisName) & ~isnan(diagnosisName)
        sameDiagnosis = find(strcmp({diagnosisList.name}, diagnosisName)==1);
        % check if this diagnosis is already present in the array
        if isempty(sameDiagnosis)
            % add this diagnosis to the array
            diagnosisList_id = diagnosisList_id + 1;
            diagnosisList_idx = diagnosisList_id;
            diagnosisList(diagnosisList_idx).diagnosisList_id = diagnosisList_id;
            diagnosisList(diagnosisList_idx).name             = diagnosisName;
            diagnosisList(diagnosisList_idx).description      = '';
        end
    end
end

fprintf('Done\n');

end

