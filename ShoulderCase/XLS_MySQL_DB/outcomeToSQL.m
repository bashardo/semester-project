function outcomeToSQL( outcome, conn )
%OUTCOMETOSQL Summary of this function goes here
%   Detailed explanation goes here

fprintf('\noutcome to mySQL...');

% Empty outcome table and reset AUTO_INCREMENT
sqlquery = 'TRUNCATE outcome';
curs = exec(conn,sqlquery);
if ~isempty(curs.Message)
    fprintf('\nMessage: %s\n', curs.Message);
end

colnames = {'outcome_id','sCase_id','date','comment','loosening'};

% Loop on pCase structure array
for i=1:numel(outcome)
    
    outcome_id = outcome(i).outcome_id;
    sCase_id   = outcome(i).sCase_id;
    date       = outcome(i).date;
    comment    = outcome(i).comment;
    loosening  = outcome(i).loosening;
        
    exdata   = {outcome_id, sCase_id, date, comment, loosening};
    % insert data in SQL
    datainsert(conn,'outcome',colnames,exdata);
end

fprintf(' Done\n');

end
