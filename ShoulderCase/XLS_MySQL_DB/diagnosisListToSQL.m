function diagnosisListToSQL(diagnosisList, conn)
%diagnosisTOSQL Summary of this function goes here
%   Detailed explanation goes here

fprintf('\ndiagnosisList to mySQL...');

testquery= 'SET FOREIGN_KEY_CHECKS = 0';
exec(conn,testquery);

% Empty diagnosisList table and reset AUTO_INCREMENT
sqlquery = 'TRUNCATE diagnosisList';
curs = exec(conn,sqlquery);
if ~isempty(curs.Message)
    fprintf('\nMessage: %s\n', curs.Message);
end

testquery= 'SET FOREIGN_KEY_CHECKS = 1';
exec(conn,testquery);

colnames = {'diagnosisList_id','name','description'};

% Loop on pCase structure array
for i=1:numel(diagnosisList)
    diagnosisList_id = diagnosisList(i).diagnosisList_id;
    name             = diagnosisList(i).name;
    description      = diagnosisList(i).description;
    
    exdata   = {diagnosisList_id,name,description};
    % insert data in SQL
    datainsert(conn,'diagnosisList',colnames,exdata);
end

fprintf(' Done\n');

end

