function treatmentToSQL(treatment, conn)
%TREATMENTTOSQL Summary of this function goes here
%   Detailed explanation goes here

fprintf('\ntreatment to mySQL...');

% Empty treatment table and reset AUTO_INCREMENT
sqlquery = 'TRUNCATE treatment';
curs = exec(conn,sqlquery);
if ~isempty(curs.Message)
    fprintf('\nMessage: %s\n', curs.Message);
end

colnames = {'treatment_id','sCase_id','treatmentList_id','date','comment'};

% Loop on pCase structure array
for i=1:numel(treatment)
    treatment_id      = treatment(i).treatment_id;
    sCase_id          = treatment(i).sCase_id;
    treatmentList_id  = treatment(i).treatmentList_id;
    date              = treatment(i).date;
    comment           = treatment(i).comment;

    
    exdata   = {treatment_id,sCase_id,treatmentList_id,date,comment};
    % insert data in SQL
    datainsert(conn,'treatment',colnames,exdata);
end

fprintf(' Done\n');

end

