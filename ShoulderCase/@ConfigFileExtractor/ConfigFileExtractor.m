classdef ConfigFileExtractor < FileExtractor
% Extract variables from a 'config.txt' file in the current directory.



  properties (Constant)
    filename = 'config.txt';
  end


  methods (Static)

    function output = getVariable(variableName)
      output = FileExtractor.getVariableFromTextFile(variableName,ConfigFileExtractor.filename);
      assert(not(isempty(output)),'%s is not defined in the config file',variableName);
    end

  end


end
