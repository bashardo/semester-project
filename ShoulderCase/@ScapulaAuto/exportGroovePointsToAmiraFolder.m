function exportLandmarksToAmiraFolder(obj)
  scapulaLandmarks = LandmarksExporter;
  scapulaLandmarks.addLandmarks(obj.groove);

  if not(isfolder(obj.shoulder.SCase.dataAmiraPath))
    mkdir(obj.shoulder.SCase.dataAmiraPath);
  end

  filename = ['AutoScapulaGrooveLandmarks' obj.shoulder.SCase.id '.landmarkAscii'];
  scapulaLandmarks.exportAmiraFile(obj.shoulder.SCase.dataAmiraPath,filename);
end