classdef ScapulaAuto < Scapula
% To be used with ShoulderAuto data.
% The load() method requires a specific implementation.
% Instanciate a GlenoidAuto object.

  methods

    function createGlenoid(obj)
      obj.glenoid = GlenoidAuto(obj);
    end

    function outputArg = load(obj)
      % Load 2 files obtained by SSM auto segmentation
      % scapulaSurfaceAuto{L/R}.ply
      % scapulaLandmarksAuto{L/R}.mat

      outputArg = 1; % To be set to 1 of loading is OK and 0 otherwise

      SCase = obj.shoulder.SCase;
      SCaseId4C = SCase.id4C;
      matlabDir = SCase.dataMatlabPath;

      % Set side equal to manual side or 'R' if manual side is not available
      manualSide = obj.shoulder.SCase.shoulderManual.side;
      if isequal(manualSide,'L')
        side = manualSide;
        otherSide = 'R';
      elseif isequal(manualSide,'R')
        side = manualSide;
        otherSide = 'L';
      else
        side = 'R';
        otherSide = 'L';
      end

      % Try loading auto segmentation from matlab dir
      fileName = ['scapulaSurfaceAuto' side '.ply'];
      fileName = [matlabDir '/' fileName];

      if ~exist(fileName,'file')
        side = otherSide;
        fileName = ['scapulaSurfaceAuto' side '.ply'];
        fileName = [matlabDir '/' fileName];
      end


      %side = SCase.shoulder.side;

      if exist(fileName,'file') == 2
          try
              face_index_start = 1;
              [points,faces] = loadPly(fileName,face_index_start); % % Load ply file

              % ptCloud = pcread(fileName); % load the pointCloud object in the ply file
              % points = ptCloud.Location; % get the array of (x,y,z) points

              obj.surface.points = points;
              obj.surface.faces = faces;
              obj.segmentation = 'A'; % Auto segmentation from matlab
          catch
              obj.segmentation = 'E'; % Error on loading
              outputArg = 0;
          end
      else
          obj.segmentation = 'N'; % No segmentation file
          outputArg = 0;
      end

      % Try loading auto scapula landmarks from matlab dir
      filename = ['scapulaLandmarksAuto' side '.mat'];
      filename = [matlabDir '/' filename];
      if exist(filename,'file') == 2
          try
              % Load mat file with scapula landmarks
              load(filename, 'ScapulaLandmarks');
              % Set loaded landmarks to scapulaAuto (obj)
              obj.angulusInferior       = ScapulaLandmarks.angulusInferior;
              obj.trigonumSpinae        = ScapulaLandmarks.trigonumSpinae;
              obj.processusCoracoideus  = ScapulaLandmarks.processusCoracoideus;
              obj.acromioClavicular     = ScapulaLandmarks.acromioClavicular;
              obj.angulusAcromialis     = ScapulaLandmarks.angulusAcromialis;
              obj.spinoGlenoidNotch     = ScapulaLandmarks.spinoGlenoidNotch;
              obj.pillar                = ScapulaLandmarks.pillar;
              obj.groove                = ScapulaLandmarks.groove;
          catch
              disp(SCaseId4C); % For debug (2 cases)
              outputArg = 0;
          end
      else
          outputArg = 0;
      end
      try
        obj.measurePlane;
      catch
        outputArg = 0;
      end
    end

  end



end
