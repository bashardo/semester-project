function exportLandmarksToAmiraFolder(obj)
  scapulaLandmarks = LandmarksExporter;
  scapulaLandmarks.addLandmarks(obj.angulusInferior);
  scapulaLandmarks.addLandmarks(obj.trigonumSpinae);
  scapulaLandmarks.addLandmarks(obj.processusCoracoideus);
  scapulaLandmarks.addLandmarks(obj.acromioClavicular);
  scapulaLandmarks.addLandmarks(obj.angulusAcromialis);
  scapulaLandmarks.addLandmarks(obj.spinoGlenoidNotch);

  if not(isfolder(obj.shoulder.SCase.dataAmiraPath))
    mkdir(obj.shoulder.SCase.dataAmiraPath);
  end

  filename = ['AutoScapulaLandmarks' obj.shoulder.SCase.id '.landmarkAscii'];
  scapulaLandmarks.exportAmiraFile(obj.shoulder.SCase.dataAmiraPath,filename);
end