classdef MuscleMeasurer < handle
% Used to perfom PCSA and degeneration measurements of a Muscle object.
% Slices and mask images are expected to be found at some specific places
% and have specific names.
%
% This class is the results of extracting measurements methods from the
% project of Nathan Donini.  

  properties
    muscle
    segmentedArea
    segmentedImage

    rangeHU = double([-1000 1000]);
    muscleThreshold = 0;
    fatThreshold = 30;
    osteochondromaThreshold = 166;
  end



  methods

    function obj = MuscleMeasurer(muscle)
      obj.muscle = muscle;
      obj.segmentedArea = obj.getSegmentedArea;
      obj.segmentedImage = obj.getSegmentedImage;

      obj.normalizeThresholds;
    end

    function output = getSegmentedArea(obj)
      output = logical(imread(fullfile(obj.muscle.maskDataPath,[obj.muscle.segmentationSet '.png'])));
    end

    function output = getSegmentedImage(obj)
      load(fullfile(obj.muscle.container.slicesDataPath,[obj.muscle.sliceName '_ForMeasurements.mat']));
      normalizedImage = obj.getNormalizedImage(imageForMeasurements);

      segmentedImage = normalizedImage.*obj.segmentedArea;

      output = segmentedImage;
    end

    function output = getNormalizedImage(obj,image)
      image(image < obj.rangeHU(1)) = obj.rangeHU(1);
      image(image > obj.rangeHU(2)) = obj.rangeHU(2);
      output = (double(image)-obj.rangeHU(1))/(obj.rangeHU(2)-obj.rangeHU(1));
    end

    function normalizeThresholds(obj)
      obj.muscleThreshold = ((obj.muscleThreshold - 1)-obj.rangeHU(1))/(obj.rangeHU(2)-obj.rangeHU(1));
      obj.fatThreshold = ((obj.fatThreshold - 1)-obj.rangeHU(1))/(obj.rangeHU(2)-obj.rangeHU(1));
      obj.osteochondromaThreshold = ((obj.osteochondromaThreshold - 1)-obj.rangeHU(1))/(obj.rangeHU(2)-obj.rangeHU(1));
    end

    function output = getPCSA(obj)
      load(fullfile(obj.muscle.container.slicesDataPath,[obj.muscle.sliceName '_PixelSpacings.mat']));
      pixelSurface = (imagesPixelSpacings(1) * imagesPixelSpacings(2)) / 100; % cm^2
      output = pixelSurface * bwarea(obj.segmentedArea);
    end

    function output = getRatioAtrophy(obj)
      output = obj.getRatioAreas(obj.getAreaAtrophy,obj.segmentedArea);
    end

    function output = getRatioFat(obj)
      output = obj.getRatioAreas(obj.getAreaFat,obj.segmentedArea);
    end

    function output = getRatioOsteochondroma(obj)
      output = obj.getRatioAreas(obj.getAreaOsteochondroma,obj.segmentedArea);
    end

    function output = getRatioDegeneration(obj)
      output = obj.getRatioAtrophy + obj.getRatioFat + obj.getRatioOsteochondroma;
    end

    function output = getRatioAreas(obj,partialArea,totalArea)
      output = bwarea(partialArea)/bwarea(totalArea);
    end

    function output = getAreaMuscle(obj)
      % The area "Muscle" is the area of the segmented image that is not atrophied.
      % Looking for a better name.
      areaMuscle = imbinarize(obj.segmentedImage,obj.muscleThreshold);
      areaMuscle = imfill(areaMuscle,'holes');
      % Keep biggest island only
      islands = bwconncomp(areaMuscle);
      islandsSizes = cellfun(@numel,islands.PixelIdxList);
      [~,biggestIslandIndex] = max(islandsSizes);
      areaMuscle = false(size(areaMuscle));
      areaMuscle(islands.PixelIdxList{biggestIslandIndex}) = true;

      output = areaMuscle;
    end

    function output = getAreaAtrophy(obj)
      output = obj.segmentedArea & not(obj.getAreaMuscle);
    end

    function output = getAreaFat(obj)
      muscleImage = obj.segmentedImage.*obj.getAreaMuscle;
      areaFat = obj.getAreaMuscle & not(imbinarize(muscleImage,obj.fatThreshold));

      output = areaFat;
    end

    function output = getAreaOsteochondroma(obj)
      muscleImage = obj.segmentedImage.*obj.getAreaMuscle;
      areaOsteochondroma = imbinarize(muscleImage,obj.osteochondromaThreshold);
      areaOsteochondroma = imfill(areaOsteochondroma,'holes');

      output = areaOsteochondroma;
    end

  end

end
