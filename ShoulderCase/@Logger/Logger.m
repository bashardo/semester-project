classdef Logger < handle
% Class to create and manage log files.
%
% Log files are created automatically and include starting and ending timstamps.
% Methods are available to create section and delimitations in the 
% handled log file.
% 
% The log file name is chosen based on the logFileCategory argument
% given to the contructor. This argument is also used to delete
% the oldest log files according to the maxNumberOfLogFiles (default = 5)
% used with a Logger(logFileCategory,maxNumberOfLogFiles).

  properties (Access = private)
    startTime
    logFileCategory
    maxNumberOfLogFiles
    logFid
    prefix
    suffix
    horizontalDelimiter
  end


  methods

    function obj = Logger(logFileCategory,varargin)
      obj.startTime = datetime('now');
      obj.logFileCategory = logFileCategory;
      if nargin == 2
        obj.maxNumberOfLogFiles = varargin{1};
      else
        obj.maxNumberOfLogFiles = 5;
      end
      obj.prefix = '';
      obj.suffix = '';
      obj.horizontalDelimiter = '_______________________________________';
      obj.createNewLog;
    end

    function log(obj,textToLog,varargin)
      % to be used like sprintf().
      % write arguments in current log file.
      textToLog = obj.replaceSpecialCharacter(textToLog);
      textToLog = sprintf(textToLog,varargin{:});
      fprintf(obj.logFid,[textToLog obj.suffix]);
    end

    function logn(obj,textToLog,varargin)
      obj.suffix = [obj.suffix '\n'];
      try
        obj.log(textToLog,varargin{:});
      catch ME
        warning(ME.message);
      end
      obj.suffix = obj.suffix(1:end-2);
      fprintf(obj.logFid,obj.prefix);
    end

    function newSection(obj,title)
      if isempty(obj.prefix)
        obj.newBlock;
      end
      obj.logn('');
      obj.prefix = [obj.prefix '  '];
      obj.logn(title);
    end


    function newDelimitedSection(obj,title)
      if not(isempty(obj.prefix))
        prefixSave = obj.prefix;
        obj.prefix = obj.prefix(1);
        obj.logn('');
        obj.logn(obj.horizontalDelimiter);
        obj.prefix = prefixSave;
        obj.logn('');
        obj.prefix = [obj.prefix '  '];
        obj.log(title);
      else
        obj.newSection(title);
      end
    end

    function closeSection(obj)
      if (length(obj.prefix) > 3)
        obj.prefix = obj.prefix(1:end-2);
        obj.logn('');
      elseif (length(obj.prefix) > 0)
        obj.closeBlock;
      end
    end

    function closeBlock(obj)
      if (length(obj.prefix) > 0)
        obj.prefix = obj.prefix(1);
        obj.logn('');
        obj.log(obj.horizontalDelimiter);
        obj.prefix = '';
        obj.logn(obj.horizontalDelimiter);
        obj.logn('');
        obj.logn('');
      end
    end

    function closeLog(obj)
      obj.delete;
    end

    function deleteExtraLogFiles(obj)
      existingLogFiles = dir(['log\' obj.logFileCategory '_*.log']);
      numberOfExtraFiles = (length(existingLogFiles) - obj.maxNumberOfLogFiles);
      if (numberOfExtraFiles > 0)
        for i = 1:numberOfExtraFiles
          delete(fullfile(existingLogFiles(i).folder,existingLogFiles(i).name));
        end
      end
    end

  end



  methods (Access = private, Hidden = true)

    function createNewLog(obj)
      if isempty(dir('log'))
        mkdir('log');
      end
      obj.createLogFile;
      obj.deleteExtraLogFiles;
      obj.logHeader;
    end

    function createLogFile(obj)
      obj.startTime.Format = '_yyyy_MM_dd_HHmmss';
      obj.logFid = fopen(fullfile('log',[obj.logFileCategory char(obj.startTime) '.log']),'w');
    end

    function logHeader(obj)
      obj.logn([obj.logFileCategory '.log']);
      obj.startTime.Format = 'default';
      obj.logn('Date: %s',char(obj.startTime));
      obj.logn('');
    end

    function str = replaceSpecialCharacter(obj,str)
      str = strrep(str,'\','/');
    end

    function newBlock(obj)
      obj.logn('');
      obj.logn('');
      obj.log(obj.horizontalDelimiter);
      obj.prefix = ['|  '];
      obj.logn(obj.horizontalDelimiter);
      obj.logn('');
    end

    function delete(obj)
      obj.closeBlock;
      obj.logn('');
      obj.logn('');
      obj.startTime.Format = 'default';
      obj.logn('Date: %s',char(obj.startTime));
      obj.log('End of log file.');
      fclose(obj.logFid);
      obj.logFid = -1;
    end

  end



end
