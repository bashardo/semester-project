function [box2Points,normal,meanX,residuals,rmse,R2] = box2Plane(SCase)

j=0;
sPoints = SCase.shoulderAuto.scapula.coordSys.express(SCase.shoulderAuto.scapula.surface.points);
Origin = SCase.shoulderAuto.scapula.coordSys.express(SCase.shoulderAuto.scapula.coordSys.origin);
TS = SCase.shoulderAuto.scapula.coordSys.express(SCase.shoulderManual.scapula.trigonumSpinae);
PC = SCase.shoulderAuto.scapula.coordSys.express(SCase.shoulderAuto.scapula.processusCoracoideus);


    for i=1:length(sPoints)
        if(sPoints(i,2)>= Origin(1,2) && sPoints(i,1)<= PC(1,1) ...
                && sPoints(i,1)>= Origin(1,1)&& sPoints(i,3)>=(TS(1,3)) && sPoints(i,3)<=(Origin(1,3)))
        j=j+1;
        box2Points(j,:)=sPoints(i,:);
        end
    end
    
[normal,meanX,residuals,rmse,R2]=fitPlane(box2Points);   
    
end
