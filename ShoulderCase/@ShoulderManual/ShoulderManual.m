classdef ShoulderManual < Shoulder
% Shoulder using manually measured data.
% Instanciate a ScapulaManual object.

  methods (Access = protected)

    function createScapula(obj)
      obj.scapula = ScapulaManual(obj);
    end

  end



  methods

    function propagateDataPath(obj)
      % Update current dataPath
      % Propagate the path to objects in properties
        
      assert(not(isempty(obj.SCase.dataMatlabPath)),'SCase.dataMatlabPath is empty')
      obj.dataPath = fullfile(obj.SCase.dataMatlabPath,'shoulder_manual');

      if not(isfolder(obj.dataPath))
        mkdir(obj.dataPath);
      end

      obj.muscles.propagateDataPath;
    end

  end

end
