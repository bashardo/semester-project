function [P0] = project2Plane(P,N,Q,m)

%  Let P be the m x 3 array of the 3D points to be projected, let Q be the
%  1 x 3 vector of the given point on the plane, let N be the 1 x 3 vector 
%  of the normal direction to the plane, and let P0 be the m x 3 array of 
%  points orthogonally projected from P onto the plane. Then do this:
% 
N = N/norm(N);
N2 = N.'*N;
P0 = P*(eye(3)-N2)+repmat(Q*N2,m,1);

% AT: Why do we need to input m? m=size(p,1)

% AT: we might use X = transformPointsForward(tform,U), tform = affine3d(A), A
% being the 4x4 matrix of the thanformation

% AT: check the code with 
% p=rand(10,3);
% pp=project2Plane(p,[1 2 3],[1 2 3],size(p,1));
% scatter3(p(:, 1),p(:, 2),p(:, 3),'blue');hold on;
% scatter3(pp(:, 1),pp(:,2),pp(:, 3),'red');hold off;

end