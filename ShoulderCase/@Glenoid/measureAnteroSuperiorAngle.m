function measureAnteroSuperiorAngle(obj)
   % Glenoid inclinaison angle between yAxis of scapula and axis
   % of height of glenoid

    % Projection of height axis on IS and PA axis
    NewPCAAxis = obj.getNewPCAAxis;

    XYPlane = Plane();
    XYPlane.fit([0 0 0;obj.scapula.coordSys.xAxis;obj.scapula.coordSys.yAxis]);
    projectedPCAAxis = XYPlane.projectOnPlane(NewPCAAxis(:,1)');
    anteroSuperiorRotation = vrrotvec(projectedPCAAxis,obj.scapula.coordSys.yAxis);
    obj.anteroSuperiorAngle = rad2deg(anteroSuperiorRotation(4));
end
