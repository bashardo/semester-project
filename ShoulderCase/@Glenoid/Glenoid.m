classdef (Abstract) Glenoid < handle
    %GLENOID Summary of this class goes here
    %   Detailed explanation goes here
    % Need to load surface points from Amira (or auto-segment)
    % Need to set anatomical calculation, and use the scapular coord.Syst

    % TODO
    %  loadMatlab()

    properties
        surface % points of the glenoid surface load glenoid surface
        center % Glenoid center in CT coordinate system
        centerLocal % Glenoid center in scapula coordinate system
        radius
        centerLine
        depth
        width
        height
        anteroSuperiorAngle
        versionAmplitude
        versionOrientation
        version
        inclination
        density
        comment
        fittedSphere
        scapula
        walch
    end



    methods (Abstract)
      load(obj);
    end



    methods (Access = protected)  % Only Scapula is allowed to construct a Glenoid

        function obj = Glenoid(scapula)
            %GLENOID Construct an instance of this class
            %   Detailed explanation goes here
            obj.surface         = [];
            obj.center          = [];
            obj.centerLocal.x   = [];
            obj.centerLocal.y   = [];
            obj.centerLocal.z   = [];
            obj.radius          = [];
            obj.depth           = [];
            obj.width           = [];
            obj.height          = [];
            obj.anteroSuperiorAngle = [];
            obj.versionAmplitude     = [];
            obj.versionOrientation   = [];
            obj.version         = [];
            obj.inclination     = [];
            obj.density         = [];
            obj.comment         = [];
            obj.fittedSphere    = Sphere();
            obj.scapula         = scapula;
            obj.walch = [];
        end

    end



    methods

        function outputArg = morphology(obj)
            % Calculate morphological properties of the glenoid from its surface:
            % center, center line, radius, depth, width, height,
            % versionAmpl, versionOrient, version, inclination,
            % antero-superior angle
            outputArg = 1;

            try
              obj.fittedSphere.fitTo(obj.surface.points);
              obj.measureCenter;
              obj.measureCenterLocal;
              obj.measureCenterLine;
              obj.measureRadius;
              obj.measureDepth;
              obj.measureWidthAndHeight;
              obj.measureVersionAndInclination;
              obj.measureAnteroSuperiorAngle;
            catch
              outputArg = 0;
            end
        end

    end



end
