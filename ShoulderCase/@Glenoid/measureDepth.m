function measureDepth(obj)
  surfacePointsFromGlenoidCenter = obj.surface.points - obj.center;
  surfacePointsProjectedOnCenterLine= projectVectorArrayOnVector(surfacePointsFromGlenoidCenter,obj.centerLine);
  deepestPointIndex = findLongest3DVector(surfacePointsProjectedOnCenterLine);
  obj.depth = norm(surfacePointsProjectedOnCenterLine(deepestPointIndex,:));
end
