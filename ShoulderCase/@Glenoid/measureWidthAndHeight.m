function measureWidthAndHeight(obj)
  NewPCAAxis = obj.getNewPCAAxis;
  PCAGlenSurf = (NewPCAAxis^-1 * obj.surface.points')';

  obj.height = max(PCAGlenSurf(:,1))-min(PCAGlenSurf(:,1));
  obj.width = max(PCAGlenSurf(:,2))-min(PCAGlenSurf(:,2));
end
