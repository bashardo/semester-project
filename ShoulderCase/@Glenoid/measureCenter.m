function measureCenter(obj)
  % Glenoid center is the closet point between the mean of the extracted
  % surface and all the points of the extracted surface.
  surfacePointsFromMeanPoint = obj.surface.points - obj.surface.meanPoint;
  centerIndex = findShortest3DVector(surfacePointsFromMeanPoint);
  obj.center = obj.surface.points(centerIndex,:);
end
