function measureCenterLine(obj)
  obj.centerLine = obj.fittedSphere.center - obj.center;
  obj.centerLine = orientVectorToward(obj.centerLine,obj.scapula.coordSys.zAxis);
end
