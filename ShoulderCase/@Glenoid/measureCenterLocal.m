function measureCenterLocal(obj)
  centerLocal = obj.scapula.coordSys.express(obj.center);
  obj.centerLocal.x = centerLocal(1);
  obj.centerLocal.y = centerLocal(2);
  obj.centerLocal.z = centerLocal(3);
end
