function output = plot(obj)
  % plot glenoid surface
  points = obj.surface.points;
  faces = obj.surface.faces;
  x = points(:,1);
  y = points(:,2);
  z = points(:,3);
  
  plotHandle(1) = trisurf(faces,x,y,z,...
    'Facecolor','none',...
    'EdgeColor','magenta',...
    'EdgeAlpha',0.1,...
    'FaceLighting','none');

  % plot glenoid centerline
  pt1 = obj.center;
  pt2 = pt1 + obj.centerLine;
  x = [pt1(1), pt2(1)];
  y = [pt1(2), pt2(2)];
  z = [pt1(3), pt2(3)];

  plotHandle(2) = plot3(x,y,z,...
    'Color','magenta',...
    'LineWidth', 5);

  output = plotHandle;
end