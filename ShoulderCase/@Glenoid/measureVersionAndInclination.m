function measureVersionAndInclination(obj)
  % Glenoid version 3D, defined by the amplitude and orientation of the angle
  % between the glenoid centerline and the scapular transverse axis (z axis).
  % The orientation is the angle between the -x axis and the glenoid
  % centerline projected in the xy plane. Zero orientation correspond to -x
  % axis (posterior side), 90 to superior, 180 to anterior orientaion, and
  % -90 to inferior. Glenoid version is also reported as version2D (>0 for
  % retroversion) and inclination (>0 for superior tilt).

  absoluteCenterLine = obj.centerLine + obj.scapula.coordSys.origin;
  localCenterLine = obj.scapula.coordSys.express(absoluteCenterLine);

  X = [1 0 0];
  Y = [0 1 0];
  Z = [0 0 1];

  versionAmplitudeRotation = vrrotvec(Z,localCenterLine); % angle between centerLine and scapular axis
  obj.versionAmplitude = rad2deg(versionAmplitudeRotation(4));

  XYProjectedCenterLine = obj.scapula.coordSys.projectOnXYPlane(absoluteCenterLine);
  localXYProjectedCenterLine = obj.scapula.coordSys.express(XYProjectedCenterLine);
  versionOrientationRotation = vrrotvec(-X,localXYProjectedCenterLine);
  obj.versionOrientation = sign(localXYProjectedCenterLine*Y') * rad2deg(versionOrientationRotation(4));

  ZXProjectedCenterLine = obj.scapula.coordSys.projectOnZXPlane(absoluteCenterLine);
  localZXProjectedCenterLine = obj.scapula.coordSys.express(ZXProjectedCenterLine);
  versionRotation = vrrotvec(Z,localZXProjectedCenterLine);
  obj.version = sign(localZXProjectedCenterLine*X') * rad2deg(versionRotation(4));

  YZProjectedCenterLine = obj.scapula.coordSys.projectOnYZPlane(absoluteCenterLine);
  localYZProjectedCenterLine = obj.scapula.coordSys.express(YZProjectedCenterLine);
  inclinationRotation = vrrotvec(Z,localYZProjectedCenterLine);
  obj.inclination = sign(localYZProjectedCenterLine*Y') * rad2deg(inclinationRotation(4));
end
