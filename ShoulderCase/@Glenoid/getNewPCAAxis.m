function output = getNewPCAAxis(obj)
  pcaCoeff = pca(obj.surface.points);
  scapulaAxes = obj.scapula.coordSys.getRotationMatrix;
  heightWidthDepthScapulaAxes = scapulaAxes(:,[2 1 3]);
  pcaToScapulaCorrelation = abs(pcaCoeff'*heightWidthDepthScapulaAxes);

  newOrder = [];
  while not(isempty( find(not(isnan(pcaToScapulaCorrelation)))))
    [row,col] = find(pcaToScapulaCorrelation == max(pcaToScapulaCorrelation,[],'all'),1);
    pcaToScapulaCorrelation(row,:) = nan(1,size(pcaToScapulaCorrelation,2));
    pcaToScapulaCorrelation(:,col) = nan(size(pcaToScapulaCorrelation,1),1);
    newOrder(col) = row;
  end

  output = pcaCoeff(:,newOrder);
end
