function SCase = plotSCase(SCaseId,varargin)
% Call a ShoulderCase.plot function
%
% Inputs: id char of shoulder case (e.g. 'P315')
%         (optional) 'manual' or 'auto' (default is 'manual'). See ShouldeCase.plot
%         docstring for further explanation
%
% Output: Corresponding ShoulderCase object
%
% Example: SCase = plotSCase('P315');
%
% Author: Alexandre Terrier, EPFL-LBO
% Creation date: 2018-07-01
% Revision date: 2018-12-30
%
% TODO
%  Chech that the SCase exist and can be ploted

%% Open log file
logFileID = openLogFile('plotSCase.log');

%% Set the data directory from the configuration file config.txt
dataDir = ConfigFileExtractor.getVariable('dataDir');

%% Add path of ShoulderCase classes
addpath('ShoulderCase');

%% Instance of a ShoulderCase object
if (exist('SCase','var') > 0)
    clear SCase; % Check for delete method
end
SCase = ShoulderCase(SCaseId); % Instanciate a ShoulderCase object


% Load the SCase
% This should be a method of ShoulderCase class

% The validity of the format should be checked Pnnn or Nnnn.
if (numel(regexp(SCaseId,'^[PN]\d{1,3}$')) == 0)
    error('Invalid format of SCaseId argument. SCaseID must start with "P" or "N" and be followed by 1 to 3 digits.');
end

% Directory of SCaseId
SCaseDirLevel0 = SCaseId(1); % Either 'P' or 'N'
strLengthSCaseId = strlength(SCaseId(2:end));
if (strLengthSCaseId < 2)
    SCaseDirLevel1 = '0';        % Hunderets
    SCaseDirLevel2 = '0';        % Dozent
elseif (strLengthSCaseId < 3)
    SCaseDirLevel1 = '0';        % Hunderets
    SCaseDirLevel2 = SCaseId(2); % Dozent
else
    SCaseDirLevel1 = SCaseId(2); % Hunderets
    SCaseDirLevel2 = SCaseId(3); % Dozent
end
% Check if a (!unique! to be done) directory exists for this SCaseId
FindSCaseIdFolder = dir([dataDir '/' SCaseDirLevel0 '/' SCaseDirLevel1 '/' SCaseDirLevel2 '/' SCaseId '*']);
if (isempty(FindSCaseIdFolder)) % No directory for this SCaseId
    error(['Missing directory for SCaseId: ' SCaseId]);
end
SCaseDirLevel3 = FindSCaseIdFolder.name;
SCaseDir = [dataDir '/' SCaseDirLevel0 '/' SCaseDirLevel1 '/' SCaseDirLevel2 '/' SCaseDirLevel3];

% Check if this SCaseId has a CT directory with '-1' postfix (preoperative
FindCTFolder = dir([SCaseDir '/' 'CT*-1']);
if (isempty(FindCTFolder)) % No CT directory for this SCaseId
    error('Missing CT directory for SCaseId');
    % ! should exist script here !
end
CTDir = [SCaseDir '/' FindCTFolder.name];

matlabDir  = [CTDir '/matlab']; % we should check for existance
matlabFile = [matlabDir '/SCase.mat'];
if exist(matlabFile, 'file')
    %      SCaseL = load(matlabFile); % Load SCase dat file
    %      SCase.id        = SCaseL.sCase.id;
    %      SCase.shoulder  = SCaseL.sCase.shoulder;
    load(matlabFile);
    %SCase=sCase;
    SCase.plot(varargin{:}); % Plot SCase
    fprintf(logFileID, ['\n' SCaseId]);
else
    error('Not matlab file for this SCase');
end
fclose(logFileID); % Close log file
end
