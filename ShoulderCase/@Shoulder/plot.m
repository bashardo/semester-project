function plot(obj,colors,varargin)
  if contains('wireframe',varargin)
    obj.scapula.plotLandmarksWireframe(colors{1},colors{2});
    hold on
  end
  if contains('scapulaSurface',varargin)
    obj.scapula.plotSurface(colors{1},colors{2});
    hold on
  end
  if contains('glenoidSurface',varargin)
    obj.scapula.glenoid.plot();
    hold on
  end
  if contains('coordinateSystem',varargin)
    obj.scapula.coordSys.plot(repmat(colors(1),1,3),false);
    hold on
  end
  if contains('centeredCoordinateSystem',varargin)
    obj.scapula.coordSys.plot(repmat(colors(1),1,3),true);
    hold on
  end
end