function initializeLayout(obj)
  obj.axesHandle('scapula') = subplot(2,4,[1 2 5 6]);
  obj.axesHandle('centered coordinate system') = subplot(2,4,[3 4]);
  obj.axesHandle('manual muscles') = subplot(2,4,7);
  obj.axesHandle('auto muscles') = subplot(2,4,8);
end