function plot(obj)
  % set method colors
  autoColors = {'b','#D9FFFF'};
  manualColors = {'#D95319','#FFFF00'};



  % plot scapula data
  axes(obj.axesHandle('scapula'));  
  title('Auto is blue, manual is red');
  hold on;
  grid on
  axis vis3d
  rotate3d on
  
  auto = containers.Map;
  try
    auto('wireframe') = obj.SCase.shoulderAuto.scapula.plotLandmarksWireframe(autoColors{1},autoColors{2});
  end
  try
    auto('scapula surface') = obj.SCase.shoulderAuto.scapula.plotSurface(autoColors{1},autoColors{2});
  end
  try
    auto('glenoid') = obj.SCase.shoulderAuto.scapula.glenoid.plot();
  end
  try
    auto('coordinate system') = obj.SCase.shoulderAuto.scapula.coordSys.plot(autoColors{1},false);
  end
  
  manual = containers.Map;
  try
    manual('wireframe') = obj.SCase.shoulderManual.scapula.plotLandmarksWireframe(manualColors{1},manualColors{2});
  end
  try
    manual('scapula surface') = obj.SCase.shoulderManual.scapula.plotSurface(manualColors{1},manualColors{2});
  end
  try
    manual('glenoid') = obj.SCase.shoulderManual.scapula.glenoid.plot();
  end
  try
    manual('coordinate system') = obj.SCase.shoulderManual.scapula.coordSys.plot(manualColors{1},false);
  end
  
  outliersNormLimit = 10;
  difference = containers.Map;
  try
    difference('difference') = obj.SCase.plotManualAutoDifferences(outliersNormLimit);
  end
  


  % plot coordinateSystem data
  axes(obj.axesHandle('centered coordinate system'));    
  title('Auto is blue, manual is red');
  hold on;
  grid on
  axis vis3d
  rotate3d on
  
  try
    auto('centered coordinate system') = obj.SCase.shoulderAuto.scapula.coordSys.plot(autoColors{1},true);
  end
  try
    manual('centered coordinate system') = obj.SCase.shoulderManual.scapula.coordSys.plot(manualColors{1},true);
  end
  

  
  % plot muscles segmentation
  axes(obj.axesHandle('auto muscles'));
  title('Auto');
  hold on;
  
  try
    auto('rotator cuff') = obj.SCase.shoulderAuto.muscles.plot();
  end
  
  axes(obj.axesHandle('manual muscles'));
  title('Manual');
  hold on;
  
  try
    manual('rotator cuff') = obj.SCase.shoulderManual.muscles.plot();
  end


  % store the data handles
  obj.plotHandle.auto = auto;
  obj.plotHandle.manual = manual;
  obj.plotHandle.difference = difference;
end
