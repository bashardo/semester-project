function initializeButton(obj)
  autoPanel = uipanel(obj.fig,...
    'Title','Auto',...
    'units','pixels',...
    'Position',[10 320 180 150 ]);
  for i = 1:length(obj.options)-1
    try
      obj.buttonHandle.auto(i) = uicontrol(autoPanel,...
        'Style','checkbox',...
        'Value',1,...
        'String',obj.options{i},...
        'position',[10,130-i*20,150,20],...
        'Callback',{@setDataVisibility,obj.plotHandle.auto(obj.options{i})});
    end
  end

  manualPanel = uipanel(obj.fig,...
    'Title','Manual',...
    'units','pixels',...
    'Position',[10 160 180 150 ]);
  for i = 1:length(obj.options)-1
    try
      obj.buttonHandle.manual(i) = uicontrol(manualPanel,...
        'Style','checkbox',...
        'Value',1,...
        'String',obj.options{i},...
        'position',[10,130-i*20,150,20],...
        'Callback',{@setDataVisibility,obj.plotHandle.manual(obj.options{i})});
    end
  end
  
  try
  obj.buttonHandle.difference = uicontrol(obj.fig,...
    'Style','checkbox',...
    'Value',1,...
    'String','auto/manual difference',...
    'position',[20,130,150,20],...
    'Callback',{@setDataVisibility,obj.plotHandle.difference('difference')});
  end
end

function setDataVisibility(button,event,dataHandle)
  
  for i = 1:length(dataHandle)
    set(dataHandle(i),'Visible',logical(button.Value));
  end
  
end