classdef ShoulderCasePlotter < handle
% Create a figure with ShoulderCase's plots 
% and buttons to toggle the data visualisation.
%
% Used by the ShoulderCase.plot() method.

  properties (Access = private)
    SCase
    fig
    axesHandle
    plotHandle
    buttonHandle
    options = {'wireframe',...
      'scapula surface',...
      'glenoid',...
      'coordinate system',...
      'centered coordinate system',...
      'rotator cuff',...
      'difference'};
  end



  methods (Access = ?ShoulderCase)

    function obj = ShoulderCasePlotter(SCase)
      obj.SCase = SCase;
      obj.fig = figure('Name',SCase.id,...
        'NumberTitle','off',...
        'units','normalized',...
        'outerposition',[0 0 1 1]);
      obj.axesHandle = containers.Map;
      obj.plotHandle = {};
      obj.buttonHandle = {};

      obj.initializeLayout();
      obj.plot();
      obj.initializeButton();
    end

  end



  methods (Access = private)

    initializeButton(obj);
    initializeLayout(obj);
    plot(obj);

  end

end