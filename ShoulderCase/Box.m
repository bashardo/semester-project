classdef Box < handle



  properties
    plane
  end



  methods

    function obj = Box()
     obj.plane          = Plane();
     
    end

    function output= getBoxPoints(obj,points,minXYZ,maxXYZ)
    % Returns the points from the given points that are in the given boundaries.
    
    constraintMin = repmat(minXYZ,size(points,1),1);
    constraintMax = repmat(maxXYZ,size(points,1),1);
    
    validCoordinates = and(points>=constraintMin, points<=constraintMax);
    validPoints = all(validCoordinates,2);
    
    output = points(validPoints,:);
   
    end
    
%     function [newCoordSysPoints]=transforMatrix(SCase,oldPoints)    
%         rotation=inv([SCase.shoulderAuto.scapula.coordSys.xAxis',SCase.shoulderAuto.scapula.coordSys.yAxis',SCase.shoulderAuto.scapula.coordSys.zAxis'])*eye(3,3);
%         transition=SCase.shoulderAuto.scapula.coordSys.origin;
%         newCoordSysPoints=oldPoints*rotation+transition.*ones(length(oldPoints),3);
% 
%     end
%      
%     function [] = plotSCasepoint(SCase,points)
%         plot(SCase.shoulderAuto  );
%         hold on
%         scatter3(points(:,1),points(:,2),points(:,3),'k','*');
%     end


  end



end
