%clc,clear,

%% load section
load N29.mat;

Origin = SCase.shoulderAuto.scapula.coordSys.express(SCase.shoulderAuto.scapula.coordSys.origin);
AI=SCase.shoulderAuto.scapula.coordSys.express(SCase.shoulderManual.scapula.angulusInferior);
AG=SCase.shoulderAuto.scapula.coordSys.express(SCase.shoulderManual.scapula.spinoGlenoidNotch);
TS = SCase.shoulderAuto.scapula.coordSys.express(SCase.shoulderManual.scapula.trigonumSpinae);
PC = SCase.shoulderAuto.scapula.coordSys.express(SCase.shoulderAuto.scapula.processusCoracoideus);
AA = SCase.shoulderAuto.scapula.coordSys.express(SCase.shoulderAuto.scapula.angulusAcromialis);
AC = SCase.shoulderAuto.scapula.coordSys.express(SCase.shoulderAuto.scapula.acromioClavicular);
points = SCase.shoulderAuto.scapula.coordSys.express(SCase.shoulderAuto.scapula.surface.points);

% %% constrient 
% minXYZ1=[TS(1,1)-5,TS(1,2)-80,TS(1,3)];
% maxXYZ1=[(TS(1,1)+5),TS(1,2),AG(1,3)];
% minXYZ2=[Origin(1,1),Origin(1,2),TS(1,3)];
% maxXYZ2=[PC(1,1), max(points(:,2)),Origin(1,3)];
% minXYZ3=[((AA(1,1)+AC(1,1))/2),Origin(1,2),TS(1,3)];
% maxXYZ3=[Origin(1,1), max(points(:,2)),Origin(1,3)];



% %% Boxes & palnes
% SCase.shoulderAuto.scapula.creatBox;
% 
% SCase.shoulderAuto.scapula.InferoSuperior.plane.fit...
%     (transformMatrix(SCase,(SCase.shoulderAuto.scapula.InferoSuperior.getBoxPoints(points,minXYZ1,maxXYZ1))));
% SCase.shoulderAuto.scapula.AntoriorSuperior.plane.fit...
%     (transformMatrix(SCase,(SCase.shoulderAuto.scapula.AntoriorSuperior.getBoxPoints(points,minXYZ2,maxXYZ2))));
% SCase.shoulderAuto.scapula.PosteriorSuperior.plane.fit...
%     (transformMatrix(SCase,(SCase.shoulderAuto.scapula.PosteriorSuperior.getBoxPoints(points,minXYZ3,maxXYZ3))));
% 
% SCase.shoulderAuto.plot
% hold on
% SCase.shoulderAuto.scapula.PosteriorSuperior.plane.plot(80)
% 
% hold on

%% constrient 
minXYZ1=[TS(1,1)-10,TS(1,2)-80,TS(1,3)];
maxXYZ1=[(TS(1,1)+10),TS(1,2),AG(1,3)];
minXYZ2=[Origin(1,1),Origin(1,2),TS(1,3)];
maxXYZ2=[PC(1,1), max(points(:,2)),Origin(1,3)];
minXYZ3=[((AA(1,1)+AC(1,1))/2)-10,Origin(1,2),TS(1,3)];
maxXYZ3=[((AA(1,1)+AC(1,1))/2)+10, max(points(:,2)),Origin(1,3)];



%% Boxes & palnes
SCase.shoulderAuto.scapula.creatBox;

SCase.shoulderAuto.scapula.InferoSuperior.plane.fit...
    (transformMatrix(SCase,(SCase.shoulderAuto.scapula.InferoSuperior.getBoxPoints(points,minXYZ1,maxXYZ1))));
SCase.shoulderAuto.scapula.AntoriorSuperior.plane.fit...
    (transformMatrix(SCase,(SCase.shoulderAuto.scapula.AntoriorSuperior.getBoxPoints(points,minXYZ2,maxXYZ2))));
SCase.shoulderAuto.scapula.PosteriorSuperior.plane.fit...
    (transformMatrix(SCase,(SCase.shoulderAuto.scapula.PosteriorSuperior.getBoxPoints(points,minXYZ3,maxXYZ3))));
SCase.shoulderAuto.plot;
hold on
SCase.shoulderAuto.scapula.PosteriorSuperior.plane.plot(100)
hold on 
SCase.shoulderAuto.scapula.AntoriorSuperior.plane.plot(100)
SCase.shoulderAuto.scapula.InferoSuperior.plane.plot(100)
