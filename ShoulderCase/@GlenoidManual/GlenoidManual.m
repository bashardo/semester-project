classdef GlenoidManual < Glenoid
% To be used with ShoulderManual data.
% The load() method requires a specific implementation.

  methods

    function outputArg = load(obj)
      % LOAD Summary of this method goes here
      %   Load the points of the glenoid surface from the amira
      %   directoty.

      SCase = obj.scapula.shoulder.SCase;
      SCaseId4C = SCase.id4C;

      amiraDir = SCase.dataAmiraPath;

      % Import glenoid surface points
      fileName = ['ExtractedSurface' SCaseId4C '.stl'];
      fileName = [amiraDir '/' fileName];
      if exist(fileName,'file') == 2
        [points,faces,~]=loadStl(fileName,1);
        obj.surface.points = points;
        obj.surface.meanPoint = mean(points);
        obj.surface.faces = faces;
      else
        warning(['Could not find file: ' fileName]);
        outputArg = 0;
        return;
       %  error(['Could not find file: ' fileName])
      end

      outputArg = 1; % Should report on loading result
    end

  end



end
