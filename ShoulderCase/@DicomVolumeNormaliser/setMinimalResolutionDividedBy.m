function setMinimalResolutionDividedBy(obj,divisionFactor)
  zSpacing = obj.spatial.PatientPositions(2:end,3)-obj.spatial.PatientPositions(1:end-1,3);
  maxZ = max(zSpacing(zSpacing ~= 0),[],'all');
  minXY = min(obj.spatial.PixelSpacings(obj.spatial.PixelSpacings ~= 0),[],'all');
  obj.resolution = min([minXY maxZ],[],'all')/divisionFactor;
end
