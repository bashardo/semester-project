function normaliseVolume(obj)
  normalisedSizeZ = round(abs(obj.spatial.PatientPositions(1,3) - obj.spatial.PatientPositions(end,3))/obj.resolution);
  normalisedSizeX = round(max(obj.spatial.PixelSpacings(:,1),[],'all')*size(obj.volume,1)/obj.resolution);
  normalisedSizeY = round(max(obj.spatial.PixelSpacings(:,2),[],'all')*size(obj.volume,2)/obj.resolution);
  try
    obj.volume = imresize3(obj.volume,[normalisedSizeX normalisedSizeY normalisedSizeZ]);
  catch
    % Retry with casting volume data type from 'double' to 'single' in case of "out of memory" error.
    obj.volume = imresize3(single(obj.volume),[normalisedSizeX normalisedSizeY normalisedSizeZ]);
  end
  newZPositions = (obj.spatial.PatientPositions(1,3):obj.resolution:obj.spatial.PatientPositions(end,3))';
  obj.spatial.PatientPositions = [repmat(obj.spatial.PatientPositions(1,1:2),length(newZPositions),1) newZPositions];
  obj.spatial.PixelSpacings = repmat(obj.resolution,size(obj.spatial.PatientPositions,1),2);
end
