classdef DicomVolumeNormaliser < handle & DicomVolume
% Used to normalise dicom volumes spacings.
%
% Dicom volume are not necessarily isotrope, often slices spacing
% is not equal to the slices' pixel spacing.
% This class normalise the spacings in the three directions
% based on the smallest spacing found divided by a chosen
% factor.
% The higher the factor, the more time will take the computation
% of the normalisation.
%
% Once volumic data are loaded with DicomeVolume methods, the
% volume normalisation is achieved in two steps:
%
%   divisionFactor = 2;   % 2 has been a good enough tradeoff
%                         % while testing this class
%   loadedDicomVolume.setMinimalResolutionDividedBy(divisionFactor);
%   loadedDicomVolume.normaliseVolume();

  properties
    resolution = [];
  end



  methods
    function obj = DicomVolumeNormaliser(varargin)
      if (nargin == 1)
        obj.loadDataFromFolder(varargin{1});
      end
    end
  end

end
