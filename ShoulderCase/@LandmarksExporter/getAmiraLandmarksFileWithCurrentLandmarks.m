function output = getAmiraLandmarksFileWithCurrentLandmarks(obj)
  rawText = obj.getAmiraLandmarksRawFile();
  fileText = rawText;
  for i = 1:size(obj.landmarks,1)
    fileText = [fileText getTextFromPoint(obj.landmarks(i,:))];
  end
  fileText = sprintf(fileText,size(obj.landmarks,1));
  output = fileText;
end

function output = getTextFromPoint(landmark)
  output = replace([num2str(landmark(1),'%10.15e') ' ' num2str(landmark(2),'%10.15e') ' ' num2str(landmark(3),'%10.15e') '\n'],'e+','e+0');
end