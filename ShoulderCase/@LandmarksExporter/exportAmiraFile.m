function exportAmiraFile(obj,filepath,filename)
  fid = fopen(fullfile(filepath,filename),'w');
  fprintf(fid,obj.getAmiraLandmarksFileWithCurrentLandmarks());
  fclose(fid);
end