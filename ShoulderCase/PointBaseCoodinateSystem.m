classdef PointBaseCoodinateSystem < CoordinateSystemScapula


  properties

  end







  methods

    function obj = PointBaseCoodinateSystem() 
     
     
    end
    
 
    function setNewCoordSys(obj,scapula)
        
        scapula.planeGen;
        syms x y z
        
    equ1 = scapula.InferoSuperior.plane.normal(1,1)*(x-scapula.InferoSuperior.plane.point...
         (1,1))+scapula.InferoSuperior.plane.normal(1,2)*(y-scapula.InferoSuperior.plane.point...
         (1,2))+scapula.InferoSuperior.plane.normal(1,3)*(z-scapula.InferoSuperior.plane.point(1,3))==0;
    equ2 = scapula.AntoriorSuperior.plane.normal(1,1)...
        *(x-scapula.AntoriorSuperior.plane.point(1,1))+scapula.AntoriorSuperior.plane.normal(1,2)...
        *(y-scapula.AntoriorSuperior.plane.point(1,2))+scapula.AntoriorSuperior.plane.normal(1,3)...
        *(z-scapula.AntoriorSuperior.plane.point(1,3))==0;
    equ3 = scapula.PosteriorSuperior.plane.normal(1,1)*(x-scapula.PosteriorSuperior.plane.point(1,1))...
        +scapula.PosteriorSuperior.plane.normal(1,2)*(y-scapula.PosteriorSuperior.plane.point(1,2))...
        +scapula.PosteriorSuperior.plane.normal(1,3)*(z-scapula.PosteriorSuperior.plane.point(1,3))==0;


    intersection12 = solve([equ1,equ2]);
    intersection13 = solve([equ1,equ3]);
    intersection23 = solve([equ2,equ3]);

    xInter12 = sym2poly(intersection12.x);
    yInter12 = sym2poly(intersection12.y);
    p1l1 = [xInter12(2); yInter12(2) ;0];
    p2l1 = [xInter12(1)+xInter12(2); yInter12(1)+yInter12(2) ;1];
    dir1=(p2l1-p1l1)/norm(p2l1-p1l1);

    xInter13 = sym2poly(intersection13.x);
    yInter13 = sym2poly(intersection13.y);
    p1l2 = [xInter13(2); yInter13(2) ;0];
    p2l2 = [xInter13(1)+xInter13(2); yInter13(1)+yInter13(2) ;1];
    dir2=(p2l2-p1l2)/norm(p2l2-p1l2);

    xInter23 = sym2poly(intersection23.x);
    yInter23 = sym2poly(intersection23.y);
    p1l3 = [xInter23(2); yInter23(2) ;0];
    p2l3 = [xInter23(1)+xInter23(2); yInter23(1)+yInter23(2) ;1];
    dir3 = (p2l3-p1l3)/norm(p2l3-p1l3);
    pointOnIntersect12 = [p1l1';p1l2'];
    pointOnIntersect3 = [p1l3';p2l3'];

    
   
        
        SG = scapula.spinoGlenoidNotch;
    
        SGl1=p1l1 + dot((SG' - p1l1),dir1)*dir1;
        SGl2=p1l2 + dot((SG' - p1l2),dir2)*dir2;
        SGl3=p1l3 + dot((SG' - p1l3),dir3)*dir3;
    
        projected = project2Plane(pointOnIntersect3,scapula.InferoSuperior.plane.normal...
        ,scapula.InferoSuperior.plane.point,size(pointOnIntersect3,1));
        dir3New = (projected(2,:)-projected(1,:))/norm(projected(2,:)-projected(1,:));
        
        meanSGonZaxis = mean([SGl1';SGl2';SGl3']);
        
        
            lateral     = scapula.spinoGlenoidNotch;
            medial      = scapula.trigonumSpinae;
            superior    = scapula.spinoGlenoidNotch;
            inferior    = scapula.angulusInferior;
        %Direction of axes
        obj.zAxis = mean([dir1';dir2';dir3New])/norm(mean([dir1';dir2';dir3New]));
        obj.zAxis   = orientVectorToward(obj.zAxis,(lateral-medial));
        obj.xAxis = scapula.InferoSuperior.plane.normal;
        obj.yAxis = cross(obj.xAxis,obj.zAxis)/norm(cross(obj.xAxis,obj.zAxis));
        obj.yAxis   = orientVectorToward(obj.yAxis,(superior-inferior));
        obj.origin = meanSGonZaxis +dot((SG - meanSGonZaxis),obj.zAxis)*obj.zAxis;
        %obj.origin = project2line(SG',meanSGonZaxis',obj.zAxis);
        
    end
    
        function projectedPoint = project2line(pointOut,pointIn,dir)
        projectedPoint = pointOut + dot((pointOut-pointIn),dir)*dir;    
        end
    
%    function plot1(obj,scapula)
%                 
%         hold on
%         s=80;
%         scatter3(obj.origin(1,1),obj.origin(1,2),obj.origin(1,3),s,'MarkerEdgeColor','r','LineWidth',4);
%         
%         zAxisPoints = [obj.origin+60*obj.zAxis;obj.origin-110*obj.zAxis];
%         xAxisPoints = [obj.origin-50*obj.xAxis;obj.origin+50*obj.xAxis];
%         yAxisPoints = [obj.origin+50*obj.yAxis;obj.origin-50*obj.yAxis];
%         plot3(xAxisPoints(:,1),xAxisPoints(:,2),xAxisPoints(:,3),'b','LineWidth',5);
%         plot3(yAxisPoints(:,1),yAxisPoints(:,2),yAxisPoints(:,3),'g','LineWidth',5);
%         plot3(zAxisPoints(:,1),zAxisPoints(:,2),zAxisPoints(:,3),'k','LineWidth',5);
%         scapula.InferoSuperior.plane.plot(80);
%         
%    end
%     
%       function plot2(obj,scapula)
%                 
%         hold on
%         s=80;
%         scatter3(obj.origin(1,1),obj.origin(1,2),obj.origin(1,3),s,'MarkerEdgeColor','r','LineWidth',4);
%         
%         zAxisPoints = [obj.origin+60*obj.zAxis;obj.origin-110*obj.zAxis];
%         xAxisPoints = [obj.origin-50*obj.xAxis;obj.origin+50*obj.xAxis];
%         yAxisPoints = [obj.origin+50*obj.yAxis;obj.origin-50*obj.yAxis];
%         plot3(xAxisPoints(:,1),xAxisPoints(:,2),xAxisPoints(:,3),'--c','LineWidth',5);
%         plot3(yAxisPoints(:,1),yAxisPoints(:,2),yAxisPoints(:,3),'--y','LineWidth',5);
%         plot3(zAxisPoints(:,1),zAxisPoints(:,2),zAxisPoints(:,3),'--w','LineWidth',5);
%         scapula.InferoSuperior.plane.plot(80);
%         
%     end

    
    

 

 


    
  end



 



end
