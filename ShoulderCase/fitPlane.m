function [normal,meanX,residuals,rmse,R2] = fitPlane(X)

% 2 lines below are commented to replace princomp by pca
%[coeff,score,roots] = princomp(X);
%normal = coeff(:,3);
[coeff,score,roots] = pca(X);
normal = cross(coeff(:,1),coeff(:,2));
[Xn,Xm] = size(X);
meanX = mean(X,1);
Xfit = repmat(meanX,Xn,1) + score(:,1:2)*coeff(:,1:2)';
residuals = X - Xfit;
error =  diag(pdist2(residuals,zeros(Xn,Xm)));
sse = sum(error.^2);
rmse = norm(error)/sqrt(Xn);

for i=1:Xn;
    tot(i) = norm(meanX-X(i,:));
end
    
sst = sum(tot.^2); 

R2 = 1-(sse/sst); %http://en.wikipedia.org/wiki/Coefficient_of_determination
end