classdef Circle < handle
% Create an array of points on a circle in the 3D space.
% 
% Designed to plot planes of shoulder cases.



  properties
    points
    numberOfPoints
    radius
    center
    normal
  end



  methods
    function obj = Circle(center,radius,normal,numberOfPoints)
      obj.center = center;
      obj.radius = radius;
      obj.numberOfPoints = numberOfPoints;
      obj.normal = normal;

      obj.createPoints;
    end

    function createPoints(obj)
      obj.points = obj.radius*[cos(2*pi*(1:obj.numberOfPoints)/obj.numberOfPoints)',...
                               sin(2*pi*(1:obj.numberOfPoints)/obj.numberOfPoints)',...
                               zeros(obj.numberOfPoints,1)];
       obj.rotate(vrrotvec([0 0 1],obj.normal));
       obj.translate(obj.center);
    end

    function rotate(obj,rotationVector)
      obj.points = (vrrotvec2mat(rotationVector)*obj.points')';
    end

    function translate(obj,vector)
      obj.points = obj.points + vector;
    end
  end

end
