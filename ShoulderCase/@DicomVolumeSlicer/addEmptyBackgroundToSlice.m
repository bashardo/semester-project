function addEmptyBackgroundToSlice(obj,height,width)
  % The background is evenly added around the slice.
  % height and width must be given in pixels.

  sliceHeight = size(obj.sliced,1);
  sliceWidth = size(obj.sliced,2);
  height = max(height,sliceHeight);
  width = max(width,sliceWidth);

  top = 1+round(height/2-sliceHeight/2);
  left = 1+round(width/2-sliceWidth/2);
  bottom = top + sliceHeight-1;
  right = left + sliceWidth-1;

  newSlice = repmat(min(obj.sliced,[],'all'),[height width]);
  newSlice(top:bottom,left:right) = obj.sliced;
  obj.sliced = newSlice;

  newSliceX = -ones(height,width);
  newSliceX(top:bottom,left:right) = obj.slicedX;
  obj.slicedX = newSliceX;

  newSliceY = -ones(height,width);
  newSliceY(top:bottom,left:right) = obj.slicedY;
  obj.slicedY = newSliceY;

  newSliceZ = -ones(height,width);
  newSliceZ(top:bottom,left:right) = obj.slicedZ;
  obj.slicedZ = newSliceZ;

  newPointsInVolume = false(height,width);
  newPointsInVolume(top:bottom,left:right) = obj.pointsInVolume;
  obj.pointsInVolume = newPointsInVolume;
end
