function orientSliceUpwardVector(obj,point,vector)
  % Give a vector in the volume and rotate the slice such that
  % the projection of this vector onto the slice is pointing 
  % upward.
  
  pointIndices = obj.getPointIndexInSlice(point);
  vectorIndices = obj.getPointIndexInSlice(obj.slicedPlane.projectOnPlane(point + 10*vector/norm(vector))) - pointIndices;
  rotation = vrrotvec([vectorIndices 0],[-1 0 0]);
  obj.sliced = imrotate(obj.sliced,rotation(3)*180*rotation(4)/pi);
  obj.slicedX = imrotate(obj.slicedX,rotation(3)*180*rotation(4)/pi);
  obj.slicedY = imrotate(obj.slicedY,rotation(3)*180*rotation(4)/pi);
  obj.slicedZ = imrotate(obj.slicedZ,rotation(3)*180*rotation(4)/pi);
  obj.pointsInVolume = imrotate(obj.pointsInVolume,rotation(3)*180*rotation(4)/pi);
  obj.measureSlicedPixelSpacings;
end
