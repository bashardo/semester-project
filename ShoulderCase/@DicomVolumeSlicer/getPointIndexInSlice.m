function output = getPointIndexInSlice(obj,point)
  % The current implementation actually returns the point in
  % the slice that is the closest to the given point, even if
  % the latter is clearly not in the slice (!).
  %
  % The former implementation has been kept but commented out.
  % It was close to the solution I (Matthieu Boubat) proposed
  % here https://ch.mathworks.com/matlabcentral/answers/524181-how-can-i-retrieve-x-y-coordinates-of-a-sliced-plane-through-a-3d-volume-using-the-obliqueslice-func.
  % This solution has been discarded because it failed sometime
  % (did not find point indices).
  %
  % This method might be refactored at some point.
  
  pointIndices = obj.getPointIndexInVolume(point);

  coordinatesError = (abs(obj.slicedX-pointIndices(1)) + abs(obj.slicedY-pointIndices(2)) + abs(obj.slicedZ-pointIndices(3)));
  [i,j] = ind2sub(size(obj.sliced),...
                  find( coordinatesError == min(coordinatesError,[],'all') ));
  % [i,j] = ind2sub(size(obj.sliced),...
  %                 find( (fix(obj.slicedX) == pointIndices(1) | ceil(obj.slicedX) == pointIndices(1)) &...
  %                       (fix(obj.slicedY) == pointIndices(2) | ceil(obj.slicedY) == pointIndices(2)) &...
  %                       (fix(obj.slicedZ) == pointIndices(3) | ceil(obj.slicedZ) == pointIndices(3)) ));
  assert(not(isempty([i,j])),'Point has not been found in slice');
  output = [i(1),j(1)];
end
