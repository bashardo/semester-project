function slice(obj,point,normalVector)
  % Use the obliqueslice() MATLAB method

  % The Plane object is used to project points on the slice
  obj.slicedPlane = Plane();
  obj.slicedPlane.point = point;
  obj.slicedPlane.normal = normalVector;

  % obliqueslice() uses volumic indices and not absolute coordinates
  pointIndices = obj.getPointIndexInVolume(point);
  vectorIndices = obj.getPointIndexInVolume(point+50*normalVector/norm(normalVector)) - pointIndices;
  [obj.sliced,x,y,z] = obliqueslice(obj.volume,pointIndices,vectorIndices,...
                                    'FillValues',nan);

  % The following properties are used to manipulate the slice and 
  % must be modified the same was the slice is modified.
  obj.pointsInVolume = not(isnan(obj.sliced));
  obj.sliced = (obj.sliced * obj.dicomInfo.RescaleSlope) + obj.dicomInfo.RescaleIntercept;
  obj.slicedX = x;
  obj.slicedY = y;
  obj.slicedZ = z;
  
  obj.measureSlicedPixelSpacings
end
