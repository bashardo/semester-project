function rescaleSliceToInt16(obj)
  % Format used to run muscle measurements

  obj.sliced(not(obj.pointsInVolume)) = 0;
  obj.sliced = int16(obj.sliced);
end
