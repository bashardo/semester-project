function measureSlicedPixelSpacings(obj)
  % Find top, bottom, leftest, and rightest points in the slice for which
  % coordinates in the volume can be found. Then measure the top to bottom
  % and the leftest to rightest vectors' norms.

  % Find the row and the column in the slice that contains the most points
  % for which coordinates in the volume can be found.
  longestRowIndex = find(sum(obj.pointsInVolume,2) == max(sum(obj.pointsInVolume,2)));
  longestColumnIndex = find(sum(obj.pointsInVolume) == max(sum(obj.pointsInVolume)));

  longestRowElements = find(obj.pointsInVolume(longestRowIndex(1),:));
  longestColumnElements = find(obj.pointsInVolume(:,longestColumnIndex(1)));




  % Find the leftest point
  leftSliceIndex = [longestRowIndex(1),longestRowElements(1)];
  leftVolumeIndex = [round(obj.slicedX(leftSliceIndex(1),leftSliceIndex(2))),...
  round(obj.slicedY(leftSliceIndex(1),leftSliceIndex(2))),...
  round(obj.slicedZ(leftSliceIndex(1),leftSliceIndex(2)))];
  % Volume boundaries can be exceeded at this point but must be asserted.
  leftVolumeIndex = max(leftVolumeIndex,[1 1 1]);
  leftVolumeIndex = min(leftVolumeIndex,size(obj.volume));
  leftPoint = obj.getPointCoordinates(leftVolumeIndex);
  
  
  
  % Find the rightest point
  rightSliceIndex = [longestRowIndex(1),longestRowElements(end)];
  rightVolumeIndex = [round(obj.slicedX(rightSliceIndex(1),rightSliceIndex(2))),...
  round(obj.slicedY(rightSliceIndex(1),rightSliceIndex(2))),...
  round(obj.slicedZ(rightSliceIndex(1),rightSliceIndex(2)))];
  % Volume boundaries can be exceeded at this point but must be asserted.
  rightVolumeIndex = max(rightVolumeIndex,[1 1 1]);
  rightVolumeIndex = min(rightVolumeIndex,size(obj.volume));
  rightPoint = obj.getPointCoordinates(rightVolumeIndex);
  
  
  
  % Find the top point
  topSliceIndex = [longestColumnElements(1),longestColumnIndex(1)];
  topVolumeIndex = [round(obj.slicedX(topSliceIndex(1),topSliceIndex(2))),...
  round(obj.slicedY(topSliceIndex(1),topSliceIndex(2))),...
  round(obj.slicedZ(topSliceIndex(1),topSliceIndex(2)))];
  % Volume boundaries can be exceeded at this point but must be asserted.
  topVolumeIndex = max(topVolumeIndex,[1 1 1]);
  topVolumeIndex = min(topVolumeIndex,size(obj.volume));
  topPoint = obj.getPointCoordinates(topVolumeIndex);
  
  
  
  % Find the bottom point
  bottomSliceIndex = [longestColumnElements(end),longestColumnIndex(1)];
  bottomVolumeIndex = [round(obj.slicedX(bottomSliceIndex(1),bottomSliceIndex(2))),...
                       round(obj.slicedY(bottomSliceIndex(1),bottomSliceIndex(2))),...
                       round(obj.slicedZ(bottomSliceIndex(1),bottomSliceIndex(2)))];
  % Volume boundaries can be exceeded at this point but must be asserted.
  bottomVolumeIndex = max(bottomVolumeIndex,[1 1 1]);
  bottomVolumeIndex = min(bottomVolumeIndex,size(obj.volume));
  bottomPoint = obj.getPointCoordinates(bottomVolumeIndex);





  obj.slicedPixelSpacings = [norm(rightPoint - leftPoint)/(length(longestRowElements)-1),...
                             norm(bottomPoint - topPoint)/(length(longestColumnElements)-1)];
end
