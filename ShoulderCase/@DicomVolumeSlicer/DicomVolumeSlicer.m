classdef DicomVolumeSlicer < handle & DicomVolume & DicomVolumeNormaliser
% Use a normalised dicom volume to create slices.
% 
% The rescale methods have been extracted form the project
% of Nathan Donini to produce formatted slices data that
% can be used with the rotator cuff auto segmentation 
% system and with the MuscleMeasurer class.

  properties
    sliced
  end



  properties (Access = public)
    slicedPixelSpacings
    slicedPlane
    slicedX
    slicedY
    slicedZ
    pointsInVolume
  end



  methods
    function obj = DicomVolumeSlicer(varargin)
      if (nargin == 1)
        obj.loadDataFromFolder(varargin{1});
      end
    end
  end

end
