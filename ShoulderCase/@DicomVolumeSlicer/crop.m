function [top,bottom,left,right] = crop(obj,center,height,width)
  % center: point in volume
  % width, height: length in mm
  assert(width>0,'Width must be positive');
  assert(height>0,'Height must be positive');
  centerIndex = obj.getPointIndexInSlice(center);

  top = max(1,centerIndex(1)-round((height/2)/obj.slicedPixelSpacings(1)));
  bottom = min(size(obj.sliced,1),centerIndex(1)+round((height/2)/obj.slicedPixelSpacings(1)));
  left = max(1,centerIndex(2)-round((width/2)/obj.slicedPixelSpacings(2)));
  right = min(size(obj.sliced,2),centerIndex(2)+round((width/2)/obj.slicedPixelSpacings(2)));

  obj.sliced = obj.sliced(top:bottom,left:right);
  obj.slicedX = obj.slicedX(top:bottom,left:right);
  obj.slicedY = obj.slicedY(top:bottom,left:right);
  obj.slicedZ = obj.slicedZ(top:bottom,left:right);
  obj.pointsInVolume = obj.pointsInVolume(top:bottom,left:right);
end
