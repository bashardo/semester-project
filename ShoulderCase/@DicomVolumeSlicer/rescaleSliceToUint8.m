function rescaleSliceToUint8(obj)
  % Format used by the rotator cuff segmentation system

  HU_interval_png = [-100 160];
  lin_coef = [HU_interval_png(1) 1 ; HU_interval_png(2) 1]^(-1)*[0 255]';
  obj.sliced = lin_coef(1)*obj.sliced+lin_coef(2);
  obj.sliced = uint8(obj.sliced);
end
