function resize(obj,rowsAndColumns)
  % rowAndColumns should be a 1x2 array specifying the number of pixels wanted
  % in rows an columns. For further information check the documentation
  % of imresize().

  obj.sliced = imresize(obj.sliced,rowsAndColumns);
  obj.slicedX = imresize(obj.slicedX,rowsAndColumns);
  obj.slicedY = imresize(obj.slicedY,rowsAndColumns);
  obj.slicedZ = imresize(obj.slicedZ,rowsAndColumns);
  obj.pointsInVolume = imresize(obj.pointsInVolume,rowsAndColumns);
  obj.measureSlicedPixelSpacings;
end
