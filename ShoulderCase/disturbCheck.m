%clc,clear, close all
%% Load case

database = ShoulderCaseLoader;
Cases = database.getAllNormalCasesID;
%%
% CaseID = input('please enter the case ID = ');
% SCase = database.loadCase(Cases{1,CaseID});
%SCase = database.loadCase(Cases{1,16});
%load N29.mat

 range = 2.7;
for i=1:length(a)
   SCase = database.loadCase(Cases{1,a(i,1)});
   figure;

%% 
%SCase = database.loadCase(Cases{1,3});
%SCase.shoulderAuto.plot;
SCase.shoulderAuto.scapula.coordSys = CoordinateSystemScapula();
%SCase.shoulderAuto.scapula.coordSys.setSystemWithScapulaLandmarks(SCase.shoulderAuto.scapula);
SCase.shoulderAuto.scapula.newCoordSys = PointBaseCoodinateSystem();
SCase.shoulderAuto.scapula.newCoordSysDeviate = PointBaseCoodinateSystem();
%SCase.shoulderAuto.scapula.oldCoordSysDeviate = CoordinateSystem();
SCase.shoulderAuto.scapula.newCoordSys.setNewCoordSys(SCase.shoulderAuto.scapula);
%SCase.shoulderAuto.scapula.newCoordSys.plot1(SCase.shoulderAuto.scapula);

%% Adding the disturbance to the landmarks-range+2*range*rand(1,3))
SCase.shoulderAuto.scapula.angulusInferior = SCase.shoulderAuto.scapula.angulusInferior+(-range+2*range*rand(1,3));
SCase.shoulderAuto.scapula.spinoGlenoidNotch = SCase.shoulderAuto.scapula.spinoGlenoidNotch+(-range+2*range*rand(1,3));
SCase.shoulderAuto.scapula.trigonumSpinae = SCase.shoulderAuto.scapula.trigonumSpinae+(-range+2*range*rand(1,3));
SCase.shoulderAuto.scapula.processusCoracoideus = SCase.shoulderAuto.scapula.processusCoracoideus+(-range+2*range*rand(1,3));
SCase.shoulderAuto.scapula.angulusAcromialis = SCase.shoulderAuto.scapula.angulusAcromialis+(-range+2*range*rand(1,3));
SCase.shoulderAuto.scapula.acromioClavicular = SCase.shoulderAuto.scapula.acromioClavicular+(-range+2*range*rand(1,3));
SCase.shoulderAuto.scapula.spinoGlenoidNotch = SCase.shoulderAuto.scapula.spinoGlenoidNotch +(-range+2*range*rand(1,3));
SCase.shoulderAuto.scapula.groove = SCase.shoulderAuto.scapula.groove+(-range+2*range*rand(length(SCase.shoulderAuto.scapula.groove),3)); 

%% Constructing the deviated coordinate systems
SCase.shoulderAuto.scapula.newCoordSysDeviate.setNewCoordSys(SCase.shoulderAuto.scapula);
SCase.shoulderAuto.scapula.setCoordinateSystemWithLandmarksDeviate();

%% plot
%SCase.shoulderAuto.scapula.coordSys.plot('b',false);
SCase.shoulderAuto.scapula.newCoordSys.plot('b',true);
%SCase.shoulderAuto.scapula.oldCoordSysDeviate.plot('c',false);
SCase.shoulderAuto.scapula.newCoordSysDeviate.plot('r',true);
 end