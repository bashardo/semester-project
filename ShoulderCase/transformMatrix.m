function [newCoordSysPoints]=transformMatrix(SCase,oldPoints)    
rotation=inv([SCase.shoulderAuto.scapula.coordSys.xAxis',SCase.shoulderAuto.scapula.coordSys.yAxis',SCase.shoulderAuto.scapula.coordSys.zAxis'])*eye(3,3);
transition=SCase.shoulderAuto.scapula.coordSys.origin;
newCoordSysPoints=oldPoints*rotation+transition.*ones(length(oldPoints),3);

end