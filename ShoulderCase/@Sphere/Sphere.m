classdef Sphere < handle
% Simple sphere object.
% Used to be fitted to glenoid points.
% 
% This class could be used elsewhere and the constructor
% access might be widen.

  properties
    center
    radius
    residuals
    R2
    RMSE
  end

  methods (Access = ?Glenoid)
    function obj = Sphere()
      obj.center            = [];
      obj.radius            = [];
      obj.residuals         = [];
      obj.R2                = [];
      obj.RMSE              = [];
    end

    function fitTo(obj,points)
      [center,radius,residuals,R2] = fitSphere(points);
      obj.center            = center';
      obj.radius            = radius;
      obj.residuals         = residuals;
      obj.R2                = R2;
      obj.RMSE              = norm(residuals)/sqrt(length(residuals));
    end
  end

end
