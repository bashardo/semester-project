%% loading the database

clc,clear,close all
database = ShoulderCaseLoader;
Cases = database.getAllNormalCasesID;
%SCase=database.loadCase(Cases{1,2});
%load N29.mat
range = 2.7;
%% Inilization
for i=1:length(Cases)
SCase=database.loadCase(Cases{1,i+211});
i+211
SCase.shoulderAuto.scapula.coordSys =  CoordinateSystemScapula();
SCase.shoulderAuto.scapula.setCoordinateSystemWithLandmarks();
%SCase.shoulderAuto.scapula.coordSys.setSystemWithScapulaLandmarks(SCase.shoulderAuto.scapula);
SCase.shoulderAuto.scapula.newCoordSys = PointBaseCoodinateSystem();
SCase.shoulderAuto.scapula.newCoordSysDeviate = PointBaseCoodinateSystem();
SCase.shoulderAuto.scapula.oldCoordSysDeviate =  CoordinateSystemScapula();
SCase.shoulderAuto.scapula.newCoordSys.setNewCoordSys(SCase.shoulderAuto.scapula);

% Adding the disturbance to the landmarks-range+2*range*rand(1,3))
SCase.shoulderAuto.scapula.angulusInferior = SCase.shoulderAuto.scapula.angulusInferior+(-range+2*range*rand(1,3));
SCase.shoulderAuto.scapula.spinoGlenoidNotch = SCase.shoulderAuto.scapula.spinoGlenoidNotch+(-range+2*range*rand(1,3));
SCase.shoulderAuto.scapula.trigonumSpinae = SCase.shoulderAuto.scapula.trigonumSpinae+(-range+2*range*rand(1,3));
SCase.shoulderAuto.scapula.processusCoracoideus = SCase.shoulderAuto.scapula.processusCoracoideus+(-range+2*range*rand(1,3));
SCase.shoulderAuto.scapula.angulusAcromialis = SCase.shoulderAuto.scapula.angulusAcromialis+(-range+2*range*rand(1,3));
SCase.shoulderAuto.scapula.acromioClavicular = SCase.shoulderAuto.scapula.acromioClavicular+(-range+2*range*rand(1,3));
SCase.shoulderAuto.scapula.spinoGlenoidNotch = SCase.shoulderAuto.scapula.spinoGlenoidNotch +(-range+2*range*rand(1,3));
SCase.shoulderAuto.scapula.groove = SCase.shoulderAuto.scapula.groove+(-range+2*range*rand(length(SCase.shoulderAuto.scapula.groove),3)); 

% Constructing the deviated coordinate systems
SCase.shoulderAuto.scapula.newCoordSysDeviate.setNewCoordSys(SCase.shoulderAuto.scapula);
%SCase.shoulderAuto.scapula.oldCoordSysDeviate.setCoordinateSystemWithLandmarks();
SCase.shoulderAuto.scapula.setCoordinateSystemWithLandmarksDeviate();
%
rotationMatrixNew2Ref = SCase.shoulderAuto.scapula.newCoordSys.getRotationMatrix;
rotationMatrixOld2Ref = SCase.shoulderAuto.scapula.coordSys.getRotationMatrix;
rotationMatrixRef2Newdeviate = inv(SCase.shoulderAuto.scapula.newCoordSysDeviate.getRotationMatrix);
rotationMatrixRef2Olddeviate = inv(SCase.shoulderAuto.scapula.oldCoordSysDeviate.getRotationMatrix);
rotationMatrixNew2Newdeviate = rotationMatrixNew2Ref*rotationMatrixRef2Newdeviate;
rotationMatrixOld2Olddeviate = rotationMatrixOld2Ref*rotationMatrixRef2Olddeviate;
rotationAngleNew(i,:) = vrrotmat2vec(rotationMatrixNew2Newdeviate);
rotationAngleOld(i,:) = vrrotmat2vec(rotationMatrixOld2Olddeviate);
%
originNewDeviate(i,:) = norm(SCase.shoulderAuto.scapula.newCoordSysDeviate.origin-SCase.shoulderAuto.scapula.newCoordSys.origin);
originOldDeviate(i,:) = norm(SCase.shoulderAuto.scapula.oldCoordSysDeviate.origin-SCase.shoulderAuto.scapula.coordSys.origin);


end
      
      