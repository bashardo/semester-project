function output = projectVectorArrayOnVector(vectorArray,vector)
  output = (vectorArray*vector') .* vector / (norm(vector)^2);
end
