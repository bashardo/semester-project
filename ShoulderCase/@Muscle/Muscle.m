classdef Muscle < handle
% The Muscle class is linked to a segmented muscle file (a mask)
% and to the slice it has been segmented out of.
%
% Then, this class can measured values linked to the PCSA and the
% muscle's degeneration.

  properties
    name = '';
    segmentationSet = '';
    sliceName = '';
    
    PCSA = [];
    atrophy = [];
    fat = [];
    osteochondroma = [];
    degeneration = [];
  end



  properties (Hidden = true)
    dataPath = '';
    maskDataPath = '';
    contourDataPath = '';
    container = [];
  end


  methods

    function obj = Muscle(musclesContainer,muscleName,slicesWithMuscleName)
      obj.container = musclesContainer;
      obj.name = muscleName;
      obj.sliceName = slicesWithMuscleName;
    end

    function propagateDataPath(obj)
      % Update current dataPath
      % Propagate the path to objects in properties

      obj.dataPath = fullfile(obj.container.dataPath,obj.name);
      obj.maskDataPath = fullfile(obj.dataPath,'mask');
      obj.contourDataPath = fullfile(obj.dataPath,'contour');

      if not(isfolder(obj.dataPath))
        mkdir(obj.dataPath);
      end
      if not(isfolder(obj.maskDataPath))
        mkdir(obj.maskDataPath);
      end
      if not(isfolder(obj.contourDataPath))
        mkdir(obj.contourDataPath);
      end
    end


    function output = getValues(obj)
      output = cellfun(@(property) obj.(property), properties(obj),...
                       'UniformOutput',false);
    end

    function measure(obj,segmentationSet)
      obj.segmentationSet = segmentationSet;
      measurer = MuscleMeasurer(obj);

      obj.PCSA = measurer.getPCSA;
      obj.atrophy = measurer.getRatioAtrophy;
      obj.fat = measurer.getRatioFat;
      obj.osteochondroma = measurer.getRatioOsteochondroma;
      obj.degeneration = measurer.getRatioDegeneration;
    end

  end

end
