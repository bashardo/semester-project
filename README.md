# Shoulder Database handling and measurements

## Introduction
This repository contains Matlab codes to import anonymised clinical cases in a shoulder database (shoulderDB), and update this database with complementary clinical, radiological, morphological, or biomechanical data. This project is a collaboration between Laboratory of Biomechanical Orthopedics of the Ecole Polytechnique Federal de Lausanne (EPFL-LBO), the ARTORG center of the University of Bern, the Orthopedic Service of the University Hospital of Lausanne (CHUV-OTR), and the Radiological Department of the University Hospital of Lausanne (CHUV-RAD).

## Setup

### Download

Clone or download [this repository](https://c4science.ch/diffusion/8218/).
Open MATLAB, set the working directory to be the downloaded repository and add its folders and subfolders to the MATLAB path. To do the latter part, once the working directory has been set you can run the following command:

    addapth(genpath(pwd));

### Write the config file

The base folder must contain a "config.txt" with the following fields:

* maxSCaseIDDigits: This is the maximum allowed number of digits in the SCase IDs. 

        With maxSCaseIDDigits = 3, 'P123' is a valid ID, 'P1234' is not valid.

* SCaseIDValidTypes: These are the allowed letters used as a prefix in the SCase IDs.                   

        With SCaseIDValidTypes = 'NP', 'P123' is a valid ID, 'Z1234' is not valid.

* pythonDir: This is the path to the rotator cuff segmentation system.              

* dataDir: This is the root path to the data used i.e. /shoulder/dataDev or /shoulder/data on lbovenus.epfl.ch

A full config.txt example for a system working on lbovenus.epfl.ch contains the four following lines:

    maxSCaseIDDigits = 3
    SCaseIDValidTypes = 'NP'
    pythonDir = '/shoulder/methods/python/rcseg'
    dataDir = '/shoulder/dataDev'


## How to use

There are four main features within this system.

#### Access data

Data are restricted to authorised users, in a dedicated EPFL server.
The data related to a specific case can be accessed thanks to the ShoulderCaseLoader class. For example:

    database = ShoulderCaseLoader();
    SCase = database.loadCase('P527');

Will load the data related to the case 'P527' into the variable SCase.
The ShoulderCaseLoader class features many other methods to access the data. To get the list of all the available cases run:

    database = ShoulderCaseLoader();
    SCaseList = database.getAllCasesIDs();

To load all the available cases in an cell array run:

    database = ShoulderCaseLoader();
    AllTheSCases = database.loadAllCases();

#### Visualize data

All the measured data are directly available in a loaded ShoulderCase's properties.

To observe and interact with a cases's data run:

    database = ShoulderCaseLoader();
    SCase = database.loadCase('P527');
    SCase.plot();

or

    plotSCase('P527');

#### Update data

To update the measured values of the shoulder cases use the function measureSCase(). Refer to the function's documentation for further informations.

#### Import new data

To import new cases (basically add a CT-files folder at the right place) use the importSCase GUI. The importSCase code is located in the sub-directory importSCase. Its description is detailed there. It currently only work for Windows.

## Documentation

Should be added here:
* datastructure design
* class diagram

## Files/Folders

### Folders

- **./anatomy** contains scripts used by Amira manual measurements and what looks like deprecated scripts that are now part of the Scapula class.
- **./ShoulderCase** contains the class and scripts used to update and access to the data. See here [how are organized class files and folders](https://ch.mathworks.com/help/matlab/matlab_oop/class-files-and-folders.html).
- **./ImportSCase** contains the GUI used to import new data into the database.
- **./Generated_Amira_TCL_Scripts** used to store TCL scripts. Not tracked by git.
- **./log** contains log files.
- **./XLS_MySQL_DB** contains scripts used to read data in .xls files and write in SQL database.
- **./upsert** contains a script to update and insert data in a database. Looks like it's an external not LBO-made feature.

### Files

- *config.txt* is mandatory.
- *csv2xlxSCase.m* Matlab script to transform CSV files into XLS files
- *dicominfoSCase.m* Matlab script used after new SCase importations. Check utility in ImportSCase SOP
- *listSCase.m* Matlab script that create a list of SCases. Deprecated, should be replaced by ShoulderCaseLoader methods everywhere.
- *loadDicomCTscan* Looks like a worksheet. Probably removable.
- *measureSCase.m* Matlab Script used to update data. (check SOP for more details)
- *openConfigFile.m* Deprecated, should be replaced by the ConfigFileExtractor methods everywhere.
- *openLogFile.m* Deprecated, should be replaced by the Logger methods everywhere.
- *plotSCase.m* Deprecated, should be replaced by ShoulderCase.plot() method everywhere.
- *statSCase.m* Statistical analysis on all the cases. Probably deprecated.

## Version

Current version is 1.0.5

## Authors

* Alexandre Terrier
* Julien Ston
* Antoine Dewarrat
* Raphael Obrist
* Killian Cosendey
* Valérie Malfroy Camine
* Jorge Solana-Muñoz
* Bharath Narayanan
* Paul Cimadomo
* Matthieu Boubat


## License

EPFL CC BY-NC-SA 4.0 Int.