# README.md of directory ImportScase
This document describes the content of the directory where it is located.

## Directory reason to exists
- This directory contain the files needed to execute a dedicated Matlab interface (GUI) developed by Jorge Solana Muñoz at LBO.
- This interface aims to simplify the task of importing and anonymizing new CT scans to the existant Shoulder Database.

## Files
- *config.txt*: This text file might contain a preselected path to the main directory of CT data. When the file is void or the content is useless, the software should ask the operator to indicate the correct path with the help of an interactive navigator window.
- *importSCase.m*: This is the main script of this software application
- *importSCase.fig*: This Matlab figure Script contains the graphical description of the graphical interface.
- *README.md*: This is this current file.
- *sortStructByField*: This is a dedicated function used by the main script (*importSCase.m*) to sort the CT's slices depending on the specified DICOM field. This function has been separated to allow it use by other scripts.
- *tempInfo.dat*: This file contains the variables used during the execution of *importSCase.m*.  Temporal file created to allow deeper understanding of the function.
