function sortedST = sortStructByField(ST,field)
try
    STfields = fieldnames(ST);
    STcell = struct2cell(ST);
    sz = size(STcell);      % Notice that this is a 3 dimensional array.
                            % For MxN structure array with P fields, the size
                            % of the converted cell array is PxMxN

    % Once it's a cell array, you can sort using sortrows:

    % Convert to a matrix
    STcell = reshape(STcell, sz(1), []);      % Px(MxN)

    % Make each field a column
    STcell = STcell';                         % (MxN)xP

    column=0
    % Sort by field 
    for k=1:length(STfields)
        if isequal(STfields(k),field)
            column = k;
        end
    end
    % fieldName=char(STfields(1))
    % field=char(field)
    % fieldColum = find(fieldName(1:length(field))==field)
    if column == 0
        ['Warning: field ' char(field) ' not found in dicom metadata']
        sortedST = ST;
        return
    else
        fieldColumn = column;
        STcell = sortrows(STcell, fieldColumn);

        %And convert it back to a structure array:

        % Put back into original cell array format
       STcell = reshape(STcell', sz);

       % Convert to Struct
       STsorted = cell2struct(STcell, STfields, 1);
       sortedST = STsorted;
    end
catch except
    except.identifier
    except.message
    ['exception arised during dicom sorting by field']
end
