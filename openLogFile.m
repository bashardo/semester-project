function logFileID = openLogFile(filename)
%SETLOGFILE Creates a log file using filename in a log directory, within
%current working directory.
%   The directory log is created if not present.

% Check if log dir exists in working directory
logDir = 'log';
if ~exist('log', 'dir') % If not create dir log
       mkdir('log')
end
logFile = [logDir '/' filename];
logFileID = fopen(logFile, 'w');
fprintf(logFileID, filename); % Write name of file
fprintf(logFileID, '\nDate: '); % Write date
fprintf(logFileID, datestr(datetime)); % Write time

output = 1;
message = 'OK';
end

